package com.kyo.test;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Test {

	public static void main(String[] args) {
		String str = "1417359600" + "000";
		Date date = new Date(Long.parseLong(str));
		SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd", Locale.ENGLISH);

		System.out.println(df.format(date));

	}
}
