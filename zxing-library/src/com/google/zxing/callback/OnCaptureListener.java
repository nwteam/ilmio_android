package com.google.zxing.callback;

import android.graphics.Bitmap;

import com.google.zxing.Result;

public interface OnCaptureListener {

	public void onCapture(Result result, Bitmap barcode, Bitmap background);
}
