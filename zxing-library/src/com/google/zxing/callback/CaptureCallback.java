package com.google.zxing.callback;

import android.graphics.Bitmap;
import android.os.Handler;

import com.google.zxing.Result;

public interface CaptureCallback {

	void handleDecode(Result result, Bitmap barcode);

	Handler getHandler();

}
