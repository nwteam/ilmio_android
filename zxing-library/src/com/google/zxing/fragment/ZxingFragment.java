package com.google.zxing.fragment;

import java.io.IOException;
import java.util.Vector;

import android.graphics.Bitmap;
import android.graphics.SurfaceTexture;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.R;
import com.google.zxing.Result;
import com.google.zxing.callback.CaptureCallback;
import com.google.zxing.callback.OnCaptureListener;
import com.google.zxing.camera.CameraManager;
import com.google.zxing.decoding.CaptureHandler;
import com.google.zxing.decoding.DecodeFormatManager;
import com.google.zxing.decoding.InactivityTimer;

public class ZxingFragment extends Fragment implements CaptureCallback,
		TextureView.SurfaceTextureListener {
	// ===========================================================
	// Constants
	// ===========================================================
	private static final int STATE_CAPTURE_ON = 0;
	private static final int STATE_CAPTURE_OFF = 1;
	// ===========================================================
	// Fields
	// ===========================================================
	private CaptureHandler mHandler;
	private boolean mHasSurface;
	private Vector<BarcodeFormat> mDecodeFormats;
	private String mCharacterSet;
	private InactivityTimer mInactivityTimer;
	private OnCaptureListener mOnCaptureListener;
	private int mState = STATE_CAPTURE_ON;
	private TextureView mPreviewView;
	private View mResultView;

	// ===========================================================
	// Life cycle
	// ===========================================================
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mDecodeFormats = new Vector<BarcodeFormat>();
		mDecodeFormats.addAll(DecodeFormatManager.QR_CODE_FORMATS);
		mCharacterSet = null;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_zxing, container, false);

		mPreviewView = (TextureView) view.findViewById(R.id.preview_view);
		mResultView = view.findViewById(R.id.result_view);

		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		CameraManager.init(getActivity().getApplicationContext());
		mHasSurface = false;
		mInactivityTimer = new InactivityTimer(getActivity());
	}

	@Override
	public void onResume() {
		super.onResume();
		if (mState == STATE_CAPTURE_ON) {
			if (mHasSurface) {
				initCamera(mPreviewView.getSurfaceTexture());
			} else {
				mPreviewView.setSurfaceTextureListener(this);
			}
		}
	}

	@Override
	public void onPause() {
		super.onPause();
		if (mState == STATE_CAPTURE_ON) {
			if (mHandler != null) {
				mHandler.quitSynchronously();
				mHandler = null;
			}
			CameraManager.get().closeDriver();
		}
	}

	@Override
	public void onDestroy() {
		mInactivityTimer.shutdown();
		super.onDestroy();
	}

	// ===========================================================
	// Getter & Setter
	// ===========================================================
	public CameraManager getCameraManager() {
		return CameraManager.get();
	}

	// ===========================================================
	// Methods for/from SuperClass/Interfaces
	// ===========================================================
	@Override
	public void onSurfaceTextureAvailable(SurfaceTexture surface, int width,
			int height) {
		if (!mHasSurface) {
			mHasSurface = true;
			if (mState == STATE_CAPTURE_ON) {
				initCamera(surface);
			}
		}
	}

	@Override
	public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width,
			int height) {
		// Ignored, Camera does all the work for us
	}

	@Override
	public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
		mHasSurface = false;
		return true;
	}

	@Override
	public void onSurfaceTextureUpdated(SurfaceTexture surface) {
		// Invoked every time there's a new Camera preview frame
	}

	@Override
	public Handler getHandler() {
		return mHandler;
	}

	// ===========================================================
	// Methods
	// ===========================================================
	private void initCamera(SurfaceTexture surface) {
		try {
			CameraManager.get().openDriver(surface);
		} catch (IOException ioe) {
			return;
		} catch (RuntimeException e) {
			return;
		}
		if (mHandler == null) {
			mHandler = new CaptureHandler(this, mDecodeFormats, mCharacterSet);
		}
	}

	@SuppressWarnings("deprecation")
	@Override
	public void handleDecode(Result result, Bitmap barcode) {
		mInactivityTimer.onActivity();
		Bitmap background = null;
		if (mPreviewView != null) {
			background = mPreviewView.getBitmap();
			mResultView.setBackgroundDrawable(new BitmapDrawable(
					getResources(), mPreviewView.getBitmap()));
		}
		finishCapture();
		onHandleDecode(result, barcode, background);
		mOnCaptureListener.onCapture(result, barcode, background);
	}

	public void setOnCaptureListener(OnCaptureListener listener) {
		mOnCaptureListener = listener;
	}

	protected void onHandleDecode(Result result, Bitmap barcode,
			Bitmap background) {

	}

	public void finishCapture() {
		Log.i("TAG", "finishCapture");
		mState = STATE_CAPTURE_OFF;

		if (mHandler != null) {
			mHandler.quitSynchronously();
			mHandler = null;
		}
		CameraManager.get().closeDriver();
	}
	// ===========================================================
	// Inner and Anonymous Classes
	// ===========================================================

}
