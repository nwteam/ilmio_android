package jp.hottea.mitsuidesigntec.fragment.coupon;

import java.util.ArrayList;
import java.util.List;

import jp.hottea.mitsuidesigntec.android.R;
import jp.hottea.mitsuidesigntec.entity.Brand;
import jp.hottea.mitsuidesigntec.entity.Shop;
import jp.hottea.mitsuidesigntec.entity.ShopListResponse;
import jp.hottea.mitsuidesigntec.fragment.BaseFragment;
import jp.hottea.mitsuidesigntec.log.KLog;
import jp.hottea.mitsuidesigntec.util.ConfigInfo;
import jp.hottea.mitsuidesigntec.volley.UrlBuilder;
import jp.hottea.mitsuidesigntec.volley.VolleyRequest;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.GsonRequest;

public class ShopListFragment extends BaseFragment {

	// ===========================================================
	// Constants
	// ===========================================================
	// ===========================================================
	// Fields
	// ===========================================================
	private ConfigInfo mInfo;
	private VolleyRequest mVolleyRequest;
	private Brand mBrand;
	private List<Shop> mShops;
	private List<Shop> mDatas;
	private ListView mListView;
	private MyAdapter mAdapter;
	private EditText mSearch;
	private String mSearchText;
	private TextView mSearchResult;
	private TextView mTitle;

	// ===========================================================
	// Life cycle
	// ===========================================================
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setLoadingEnable(true);
		mInfo = getConfigInfo();
		mVolleyRequest = getVolleyRequest();

		Bundle args = getArguments();
		if (args != null) {
			mBrand = (Brand) args.getSerializable("brand");
		}

		if (!isLocalDebug()) {
			requestShopList();
		}

		// setup data
		mDatas = new ArrayList<Shop>();
		mSearchText = "";
	}

	@Override
	public View onCreateContent(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.fragment_shop_list, container,
				false);
		// text
		mSearch = (EditText) view.findViewById(R.id.search);
		mSearch.setText(mSearchText);
		mSearch.setOnEditorActionListener(mOnEditorActionListener);
		mSearchResult = (TextView) view.findViewById(R.id.search_result);
		String result = getResources().getString(R.string.search_result);

		// list
		mAdapter = new MyAdapter();
		mListView = (ListView) view.findViewById(R.id.list);
		mListView.setOnItemClickListener(mOnItemClickListener);
		mListView.setAdapter(mAdapter);

		if (mInfo.isLocalDebug()) {
			Shop shop = null;
			for (int i = 0; i < 10; i++) {
				shop = new Shop();
				shop.setShop_name("xxxxx");
				shop.setAddress("xxxxxxxxxx");
				mDatas.add(shop);
			}
			result = String.format(result, "xxxx", 16);
			mSearchResult.setText(result);
		} else {
			if (mBrand != null && mDatas != null) {
				notifyDataSetChanged();
			}
		}

		return view;
	}

	@Override
	protected View onCreateHeader(LayoutInflater inflater, ViewGroup container) {
		View view = inflater.inflate(R.layout.header_back_text_2, container,
				false);
		View back = view.findViewById(R.id.back);
		back.setOnClickListener(mOnClickListener);
		View temp = view.findViewById(R.id.title_temp);
		ViewGroup.LayoutParams params = temp.getLayoutParams();
		back.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
		params.width = back.getMeasuredWidth();
		// temp.setLayoutParams(params);
		mTitle = (TextView) view.findViewById(R.id.title);
		String shoplist = getResources().getString(R.string.x_shop_list);
		if (isLocalDebug()) {
			mTitle.setText(String.format(shoplist, "XXXXXX"));
		} else {
			if (mBrand != null) {
				mTitle.setText(String.format(shoplist, mBrand.getBrand_name()));
			}
		}
		return view;
	}

	// ===========================================================
	// Methods
	// ===========================================================
	private void requestShopList() {
		if (mBrand != null) {
			UrlBuilder builder = new UrlBuilder();
			builder.put("c", "brand");
			builder.put("a", "brand_shop_list");
			builder.put("brand_id", mBrand.getBrand_id());
			String url = builder.build();
			KLog.i("url:" + url);

			GsonRequest<ShopListResponse> request = new GsonRequest<ShopListResponse>(
					url, ShopListResponse.class,
					new Listener<ShopListResponse>() {
						@Override
						public void onResponse(ShopListResponse response) {
							try {
								if ("0".equals(response.getResult().getError())) {
									mShops = response.getResult().getList();
									mDatas = new ArrayList<Shop>();
									for (Shop item : mShops) {
										mDatas.add(item);
									}
									notifyDataSetChanged();
									finishLoading();
								} else {
									finishLoading(false);
									Toast.makeText(getActivity(),
											R.string.request_error,
											Toast.LENGTH_LONG).show();
								}
							} catch (Exception e) {
								finishLoading(false);
								e.printStackTrace();
								Toast.makeText(getActivity(),
										R.string.request_error,
										Toast.LENGTH_LONG).show();
							}
						}
					}, new ErrorListener() {
						@Override
						public void onErrorResponse(VolleyError error) {
							finishLoading(false);
							Toast.makeText(getActivity(),
									R.string.request_failed, Toast.LENGTH_LONG)
									.show();
						}
					});
			request.setTag("shop_list");
			addRequestTag("shop_list");
			mVolleyRequest.getRequestQueue().cancelAll("shop_list");
			mVolleyRequest.addToRequestQueue(request);
		}
	}

	@Override
	public void notifyDataSetChanged() {
		super.notifyDataSetChanged();
		String result = getString(R.string.search_result);
		if (TextUtils.isEmpty(mSearchText)) {
			mSearchResult.setText(String.format(result, mBrand.getBrand_name(),
					mDatas.size()));
		} else {
			mSearchResult.setText(String.format(result, mSearchText,
					mDatas.size()));
		}
		mAdapter.notifyDataSetChanged();
	}

	// ===========================================================
	// Listener
	// ===========================================================
	private OnClickListener mOnClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.back:
				getFragmentManager().popBackStack();
				break;
			default:
				break;
			}
		}
	};

	private OnItemClickListener mOnItemClickListener = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			FragmentTransaction ft = getFragmentManager().beginTransaction();
			Fragment f = new ShopDetailFragment();
			Bundle args = new Bundle();
			args.putSerializable("shop", mAdapter.getItem(position));
			f.setArguments(args);
			ft.add(R.id.container, f);
			ft.addToBackStack(null);
			ft.commit();
		}
	};

	private OnEditorActionListener mOnEditorActionListener = new OnEditorActionListener() {

		@Override
		public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
			if (actionId == EditorInfo.IME_ACTION_SEARCH) {
				InputMethodManager imm = (InputMethodManager) getActivity()
						.getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(mSearch.getWindowToken(), 0);

				String text = mSearch.getText().toString();
				if (TextUtils.isEmpty(text)) {
					mSearchText = ""; // all item
					mDatas.clear();
					for (Shop item : mShops) {
						mDatas.add(item);
					}
					notifyDataSetChanged();
				} else {
					mSearchText = text;
					mDatas.clear();
					for (Shop item : mShops) {
						if (item.getShop_name().contains(text)
								|| item.getAddress().contains(text)) {
							mDatas.add(item);
						}
					}
					notifyDataSetChanged();
				}
				return true;
			}
			return false;
		}
	};

	// ===========================================================
	// Getter & Setter
	// ===========================================================
	// ===========================================================
	// Methods for/from SuperClass/Interfaces
	// ===========================================================
	// ===========================================================
	// Inner and Anonymous Classes
	// ===========================================================
	private class MyAdapter extends BaseAdapter {
		private LayoutInflater mInflater;

		public MyAdapter() {
			super();
			mInflater = LayoutInflater.from(getActivity());
		}

		@Override
		public int getCount() {
			return mDatas.size();
		}

		@Override
		public Shop getItem(int position) {
			return mDatas.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View view = null;
			ViewHolder holder = null;

			if (null == convertView) {
				view = mInflater.inflate(R.layout.item_normal_next_1, parent,
						false);
				holder = new ViewHolder();
				holder.name = (TextView) view.findViewById(R.id.item_title);
				holder.address = (TextView) view
						.findViewById(R.id.item_subtitle);
				view.setTag(holder);
			} else {
				view = convertView;
				holder = (ViewHolder) view.getTag();
			}

			holder.name.setText(getItem(position).getShop_name());
			holder.address.setText(getItem(position).getAddress());

			return view;
		}
	}

	private class ViewHolder {
		private TextView name;
		private TextView address;
	}
}
