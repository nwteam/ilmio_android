package jp.hottea.mitsuidesigntec.fragment.coupon;

import java.util.ArrayList;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.util.Log;
import android.view.KeyEvent;
import android.widget.*;
import jp.hottea.mitsuidesigntec.activity.CategoryActivity;
import jp.hottea.mitsuidesigntec.android.R;
import jp.hottea.mitsuidesigntec.constants.ActivityConstants;
import jp.hottea.mitsuidesigntec.constants.NetworkConstants;
import jp.hottea.mitsuidesigntec.entity.Brand;
import jp.hottea.mitsuidesigntec.entity.BrandListResponse;
import jp.hottea.mitsuidesigntec.entity.Category;
import jp.hottea.mitsuidesigntec.entity.CategoryListResponse;
import jp.hottea.mitsuidesigntec.fragment.BaseFragment;
import jp.hottea.mitsuidesigntec.log.KLog;
import jp.hottea.mitsuidesigntec.util.ConfigInfo;
import jp.hottea.mitsuidesigntec.volley.UrlBuilder;
import jp.hottea.mitsuidesigntec.volley.VolleyRequest;
import jp.hottea.mitsuidesigntec.widget.CustomDialog;
import jp.hottea.mitsuidesigntec.widget.HeaderGridView;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView.OnItemClickListener;

import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.GsonRequest;
import com.android.volley.toolbox.NetworkImageView;

public class CouponTopFragment extends BaseFragment {
	// ===========================================================
	// Constants
	// ===========================================================
	// ===========================================================
	// Fields
	// ===========================================================
	private VolleyRequest mVolleyRequest;
	private ArrayList<Brand> mBrands;
	private ArrayList<Category> mCategorys;
	private Brand mFirstBrand;
	// --
	private NetworkImageView mFirstBrandImage;
	private TextView mFirstBrandName;
	private TextView mFirstBrandCategory;
	// --
	private HeaderGridView mGridView;
	private MyAdapter mAdapter;
	// --
	private LayoutInflater mInflater;

	// ===========================================================
	// Life cycle
	// ===========================================================
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setLoadingEnable(true);
		mVolleyRequest = getVolleyRequest();
		mBrands = new ArrayList<Brand>();
		if (!isLocalDebug()) {
			requestBrand();
			requestCategory();
		}
	}

	@Override
	public View onCreateContent(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		System.out.println("onCreateView");
		View view = inflater.inflate(R.layout.fragment_coupon_top, container,
				false);

		View gridHeaderView = inflater
				.inflate(R.layout.header_gridview_1, null);
		// category
		gridHeaderView.findViewById(R.id.category_layout).setOnClickListener(
				mOnClickListener);

		mFirstBrandImage = (NetworkImageView) gridHeaderView
				.findViewById(R.id.brand_image);
		mFirstBrandImage.setOnClickListener(mOnClickListener);
		mFirstBrandImage.setDefaultImageResId(R.drawable.empty_photo);
		mFirstBrandImage.setErrorImageResId(R.drawable.empty_photo);
		mFirstBrandName = (TextView) gridHeaderView
				.findViewById(R.id.brand_name);
		mFirstBrandCategory = (TextView) gridHeaderView
				.findViewById(R.id.category_name);

		mInflater = inflater;
		mAdapter = new MyAdapter(getActivity().getApplicationContext());
		mGridView = (HeaderGridView) view.findViewById(R.id.pull_refresh_grid);
		mGridView.addHeaderView(gridHeaderView, null, false);
		// mGridView.addHeaderView(gridHeaderView);
		mGridView.setAdapter(mAdapter);
		// mPullRefreshGridView.setMode(Mode.PULL_FROM_END);
		mGridView.setOnItemClickListener(mOnItemClickListener);
		mGridView.setOnScrollListener(mOnScrollListener);
		int spacing = getResources().getDimensionPixelSize(
				R.dimen.top_grid_spacing);
		mGridView.setPadding(spacing, 0, spacing, 0);

		if (isLocalDebug()) {
			mFirstBrand = new Brand();
			mFirstBrand.setBrand_name("XXXXXXXX");
			mFirstBrand.setCat_name("XXX");
			mFirstBrand.setResourceId(R.drawable.furniture);
			for (int i = 0; i < 8; i++) {
				Brand brand = new Brand();
				brand.setBrand_name("XXXXXXXX");
				brand.setCat_name("XXX");
				brand.setResourceId(R.drawable.g1 + i);
				mBrands.add(brand);
			}
			mFirstBrandImage.setImageResource(mFirstBrand.getResourceId());
			mFirstBrandName.setText(mFirstBrand.getBrand_name());
			mFirstBrandCategory.setText(mFirstBrand.getCat_name());
		} else {
			if (mFirstBrand != null && mBrands != null) {
				notifyDataSetChanged();
			}
		}

		return view;
	}


	/*void googleVersion(){
		String query = "jp.hottea.mitsuidesigntec.android";
		Market.AppsRequest appsRequest = Market.AppsRequest.newBuilder()
				.setQuery(query)
				.setStartIndex(0).setEntriesCount(10)
				.setWithExtendedInfo(true)
				.build();
		MarketSession session = new MarketSession();
		session.append(appsRequest, new MarketSession.Callback<Market.AppsResponse>() {
			@Override
			public void onResult(Market.ResponseContext context, Market.AppsResponse response) {
				// Your code here
				// response.getApp(0).getCreator() ...
				// see AppsResponse class definition for more infos
				int versionCode = response.getApp(0).getVersionCode();

				Log.e("googleVersion", "versionCode=" + versionCode);
				int currentVc = getAppVersion(getActivity());
				if(currentVc > versionCode){
					showDialog();
				}

			}
		});

	}*/

	void showDialog(){
		final CustomDialog dialog = new CustomDialog(getActivity(), R.style.DialogStyle);
		View contentView = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_layout,null);

		dialog.setContentView(contentView);

		dialog.setCanceledOnTouchOutside(false);

		dialog.show();

		Button googleBtn = (Button) contentView.findViewById(R.id.google_btn);
		googleBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Uri uri = Uri.parse("https://play.google.com/store/apps/details?id=jp.hottea.mitsuidesigntec.android&hl=ja");
				Intent it = new Intent(Intent.ACTION_VIEW, uri);
				startActivity(it);
			}
		});

	}

	/**
	 * @return Application's version code from the {@code PackageManager}.
	 */
	private static int getAppVersion(Context context) {
		try {
			PackageInfo packageInfo = context.getPackageManager()
					.getPackageInfo(context.getPackageName(), 0);
			return packageInfo.versionCode;
		} catch (PackageManager.NameNotFoundException e) {
			// should never happen
			throw new RuntimeException("Could not get package name: " + e);
		}
	}

	protected View onCreateHeader(LayoutInflater inflater, ViewGroup container) {
		View view = inflater.inflate(R.layout.header_image, container, false);
		return view;
	}

	// ===========================================================
	// Listener
	// ===========================================================

	private OnClickListener mOnClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.brand_image: {
				if (mFirstBrand != null) {
					Bundle args = new Bundle();
					args.putSerializable("brand", mFirstBrand);

					FragmentTransaction ft = getFragmentManager()
							.beginTransaction();
					BrandDetailFragment f = new BrandDetailFragment();
					f.setArguments(args);
					ft.add(R.id.container, f);
					ft.addToBackStack(null);
					ft.commit();
				}
				break;
			}
			case R.id.category_layout: {
				Intent intent = new Intent(getActivity(),
						CategoryActivity.class);
				if (mCategorys != null) {
					intent.putExtra("categorys", mCategorys);
				}
				getActivity().startActivityForResult(intent,
						ActivityConstants.REQUEST_CATEGORY);
				break;
			}
			default:
				break;
			}
		}
	};

	private OnItemClickListener mOnItemClickListener = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			Brand brand = (Brand) parent.getAdapter().getItem(position);
			Bundle args = new Bundle();
			args.putSerializable("brand", brand);

			FragmentTransaction ft = getFragmentManager().beginTransaction();
			BrandDetailFragment f = new BrandDetailFragment();
			f.setArguments(args);
			ft.add(R.id.container, f);
			ft.addToBackStack(null);
			ft.commit();
		}
	};

	// OnRefreshListener<GridView> mOnRefreshListener = new
	// OnRefreshListener<GridView>() {
	//
	// @Override
	// public void onRefresh(PullToRefreshBase<GridView> refreshView) {
	// String label = DateUtils.formatDateTime(getActivity()
	// .getApplicationContext(), System.currentTimeMillis(),
	// DateUtils.FORMAT_SHOW_TIME | DateUtils.FORMAT_SHOW_DATE
	// | DateUtils.FORMAT_ABBREV_ALL);
	//
	// // Update the LastUpdatedLabel
	// refreshView.getLoadingLayoutProxy().setLastUpdatedLabel(label);
	//
	// // Do work to refresh the list here.
	// new GetDataTask().execute();
	// }
	// };

	private OnScrollListener mOnScrollListener = new OnScrollListener() {

		@Override
		public void onScrollStateChanged(AbsListView view, int scrollState) {
			switch (scrollState) {
			case OnScrollListener.SCROLL_STATE_FLING:
				// mVolleyRequest.getRequestQueue().stop();
				break;
			case OnScrollListener.SCROLL_STATE_IDLE:
				// mVolleyRequest.getRequestQueue().start();
				break;
			default:
				break;
			}
		}

		@Override
		public void onScroll(AbsListView view, int firstVisibleItem,
				int visibleItemCount, int totalItemCount) {
		}
	};

	// ===========================================================
	// Methods for/from SuperClass/Interfaces
	// ===========================================================
	public void onResult(int requestCode, int resultCode, Intent data) {
		KLog.i("onResult");

		if (data == null) {
			return;
		}

		String category = data.getStringExtra("category");
		KLog.i("onResult, category: " + category);
		startLoading();
		requestBrandByCategory(category);
	}

	// ===========================================================
	// Methods
	// ===========================================================
	private void requestBrand() {
		UrlBuilder builder = new UrlBuilder();
		builder.put("c", "brand");
		builder.put("a", "brand_list_index");
		builder.put("type", "recommend");
		builder.put("rank_id", getConfigInfo().getString(ConfigInfo.KEY_ID));
		builder.put("version_chk_flg","2");
		builder.put("ver",""+getAppVersion(getActivity()));
		String url = builder.build();
		KLog.e("url:" + url);
		GsonRequest<BrandListResponse> response = new GsonRequest<BrandListResponse>(
				url, BrandListResponse.class, null,
				new Listener<BrandListResponse>() {
					@Override
					public void onResponse(BrandListResponse response) {
						try {
							KLog.e("response.getResult().getError()="+response.getResult().getError());
							if ("0".equals(response.getResult().getError())) {
								mBrands = response.getResult().getList();
								if (mBrands.size() > 0) {
									mFirstBrand = mBrands.remove(0);
								}
								notifyDataSetChanged();
								finishLoading();
							} else if("1".equals(response.getResult().getError())){
								showDialog();
							}
							else {
								finishLoading(false);
								Toast.makeText(getActivity(),
										R.string.request_error,
										Toast.LENGTH_LONG).show();
							}

						} catch (Exception e) {
							finishLoading(false);
							e.printStackTrace();
							Toast.makeText(getActivity(),
									R.string.request_error, Toast.LENGTH_LONG)
									.show();
						}
					}
				}, new ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						KLog.e("error:" + error.getMessage());
						finishLoading(false);
						Toast.makeText(getActivity(), R.string.request_failed,
								Toast.LENGTH_LONG).show();
					}
				});
		response.setTag("brand_list_response");
		addRequestTag("brand_list_response");
		mVolleyRequest.getRequestQueue().cancelAll("brand_list_response");
		mVolleyRequest.addToRequestQueue(response);
	}

	private void requestBrandByCategory(String categoryId) {
		UrlBuilder builder = new UrlBuilder();
		builder.put("c", "brand");
		builder.put("a", "brand_list");
		builder.put("cat_id", categoryId);
		builder.put("rank_id", getConfigInfo().getString(ConfigInfo.KEY_ID));
		String url = builder.build();
		KLog.i("url:" + url);
		GsonRequest<BrandListResponse> request = new GsonRequest<BrandListResponse>(
				url, BrandListResponse.class, null,
				new Listener<BrandListResponse>() {
					@Override
					public void onResponse(BrandListResponse response) {
						try {
							if ("0".equals(response.getResult().getError())) {
								mBrands = response.getResult().getList();
								if (mBrands.size() > 0) {
									mFirstBrand = mBrands.remove(0);
								}
								notifyDataSetChanged();
								finishLoading();
							} else {
								Toast.makeText(getActivity(),
										R.string.request_error,
										Toast.LENGTH_SHORT).show();
								finishLoading(false);
							}

						} catch (Exception e) {
							e.printStackTrace();
							Toast.makeText(getActivity(),
									R.string.request_error, Toast.LENGTH_SHORT)
									.show();
							finishLoading(false);
						}
					}
				}, new ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						KLog.e("error:" + error.getMessage());
						Toast.makeText(getActivity(), R.string.request_failed,
								Toast.LENGTH_SHORT).show();
						finishLoading(false);
					}
				});
		request.setTag("brand_list_response");
		addRequestTag("brand_list_response");
		mVolleyRequest.getRequestQueue().cancelAll("brand_list_response");
		mVolleyRequest.addToRequestQueue(request);
	}

	private void requestCategory() {
		UrlBuilder builder = new UrlBuilder();
		builder.put("c", "category");
		builder.put("a", "cat_list");
		String url = builder.build();
		KLog.i("url:" + url);

		GsonRequest<CategoryListResponse> request = new GsonRequest<CategoryListResponse>(
				url, CategoryListResponse.class, null,
				new Listener<CategoryListResponse>() {

					@Override
					public void onResponse(CategoryListResponse response) {
						try {
							if ("0".equals(response.getResult().getError())) {
								mCategorys = response.getResult().getList();
							} else {
								KLog.e(getString(R.string.request_error));
							}
						} catch (Exception e) {
							e.printStackTrace();
							KLog.e(getString(R.string.request_error));
						}
					}
				}, new ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						KLog.e(getString(R.string.request_failed));
					}
				});
		request.setTag("category_list");
		addRequestTag("category_list");
		mVolleyRequest.getRequestQueue().cancelAll("category_list");
		mVolleyRequest.addToRequestQueue(request);
	}

	@Override
	public void notifyDataSetChanged() {
		if (null != mFirstBrand) {
			mFirstBrandImage.setImageUrl(NetworkConstants.URL_HOST
					+ mFirstBrand.getBrand_image(),
					mVolleyRequest.getImageLoader());
			mFirstBrandName.setText(mFirstBrand.getBrand_name());
			mFirstBrandCategory.setText(mFirstBrand.getCat_name());
		}
		mAdapter.notifyDataSetChanged();
	}

	// ===========================================================
	// Inner and Anonymous Classes
	// ===========================================================

	private class MyAdapter extends BaseAdapter {

		// AbsListView.LayoutParams normalParams;

		public MyAdapter(Context context) {

		}

		@Override
		public int getCount() {
			return mBrands.size();
		}

		@Override
		public Brand getItem(int position) {
			return mBrands.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View view = convertView;
			ViewHolder holder = null;
			if (null == view) {
				view = mInflater.inflate(R.layout.brand, parent, false);
				holder = new ViewHolder();
				holder.image = (NetworkImageView) view
						.findViewById(R.id.brand_image);
				holder.image.setDefaultImageResId(R.drawable.empty_photo);
				holder.image.setErrorImageResId(R.drawable.empty_photo);
				holder.name = (TextView) view.findViewById(R.id.brand_name);
				holder.category = (TextView) view
						.findViewById(R.id.category_name);
				view.setTag(holder);
			} else {
				holder = (ViewHolder) view.getTag();
			}
			if (isLocalDebug()) {
				holder.image
						.setImageResource(getItem(position).getResourceId());
			} else {
				holder.image.setImageUrl(
						NetworkConstants.URL_HOST
								+ getItem(position).getBrand_image(),
						mVolleyRequest.getImageLoader());
			}
			holder.name.setText(getItem(position).getBrand_name());
			holder.category.setText(getItem(position).getCat_name());

			return view;
		}

		class ViewHolder {
			NetworkImageView image;
			TextView name;
			TextView category;
		}
	}

	private class GetDataTask extends AsyncTask<Void, Void, ArrayList<Brand>> {

		@Override
		protected ArrayList<Brand> doInBackground(Void... result) {
			// try {
			// Thread.sleep(3000);
			// } catch (InterruptedException e) {
			// e.printStackTrace();
			// }
			// for (int i = 0; i < 10; i++) {
			// mBrands.add(null);
			// }
			return null;
		}

		@Override
		protected void onPostExecute(ArrayList<Brand> result) {

			mAdapter.notifyDataSetChanged();
			// Call onRefreshComplete when the list has been refreshed.
			// mGridView.onRefreshComplete();
		}
	}
}
