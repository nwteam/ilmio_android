package jp.hottea.mitsuidesigntec.fragment.coupon;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import jp.hottea.mitsuidesigntec.activity.MainActivity;
import jp.hottea.mitsuidesigntec.android.R;
import jp.hottea.mitsuidesigntec.constants.NetworkConstants;
import jp.hottea.mitsuidesigntec.entity.Brand;
import jp.hottea.mitsuidesigntec.entity.BrandDetailResponse;
import jp.hottea.mitsuidesigntec.entity.LoginIn;
import jp.hottea.mitsuidesigntec.entity.LoginInResponse;
import jp.hottea.mitsuidesigntec.fragment.BaseFragment;
import jp.hottea.mitsuidesigntec.fragment.news.NewsTopFragment;
import jp.hottea.mitsuidesigntec.log.KLog;
import jp.hottea.mitsuidesigntec.util.ConfigInfo;
import jp.hottea.mitsuidesigntec.volley.UrlBuilder;
import jp.hottea.mitsuidesigntec.volley.VolleyRequest;
import jp.hottea.mitsuidesigntec.widget.LoadingDialog;
import android.app.AlertDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.GsonRequest;
import com.android.volley.toolbox.NetworkImageView;

public class BrandDetailFragment extends BaseFragment {
	// ===========================================================
	// Constants
	// ===========================================================
	// ===========================================================
	// Fields
	// ===========================================================
	private ConfigInfo mInfo;
	private VolleyRequest mVolleyRequest;
	private Brand mBrand;
	// --
	private ViewPager mPager;
	private MyAdapter mAdapter;
	private RadioGroup mRadioGroup;
	private TextView mCategoryName;
	private TextView mDiscountRate;
	private NetworkImageView mBrandLogo;
	private TextView mBrandDesc;
	private Button mShopeList;
	private Button mQR;
	private TextView mDiscountOutContents;

	private TextView mTitle;
//	private View mQrLayout;
	private View mBiccamera;
	private View mWebshop;
	private TextView mKpBlCode;
	private TextView mNewContent;
	private TextView mNewsTitle;
	
	private LinearLayout new_layout;

	// ===========================================================
	// Life cycle
	// ===========================================================
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setLoadingEnable(true);
		mInfo = getConfigInfo();
		mVolleyRequest = getVolleyRequest();
		Bundle args = getArguments();
		if (!isLocalDebug() && args != null) {
			mBrand = (Brand) args.getSerializable("brand");
			if (null != mBrand) {
				requestBrandDetail();
			}
		}
	}

	@Override
	public View onCreateContent(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		final View view = inflater.inflate(R.layout.fragment_brand_detail, container,
				false);

		/** pager height */
		FrameLayout layout = (FrameLayout) view.findViewById(R.id.pager_layout);
		layout.getLayoutParams().height = getResources().getDisplayMetrics().widthPixels / 2;
		mAdapter = new MyAdapter(getChildFragmentManager());
		mPager = (ViewPager) view.findViewById(R.id.pager);
		mPager.setAdapter(mAdapter);
		mPager.setOnPageChangeListener(mOnPageChangeListener);
		mRadioGroup = (RadioGroup) view.findViewById(R.id.radio_group);

		mCategoryName = (TextView) view.findViewById(R.id.category_name);
		mDiscountRate = (TextView) view.findViewById(R.id.discount_rate);

		mBrandLogo = (NetworkImageView) view.findViewById(R.id.brand_logo);
		mBrandLogo.setDefaultImageResId(R.drawable.empty_photo);
		mBrandLogo.setErrorImageResId(R.drawable.empty_photo);
		mBrandDesc = (TextView) view.findViewById(R.id.brand_desc);
		mShopeList = (Button) view.findViewById(R.id.shopelist);
		mShopeList.setOnClickListener(mOnClickListener);
		mQR = (Button) view.findViewById(R.id.qr);
		mQR.setOnClickListener(mOnClickListener);
		mDiscountOutContents = (TextView) view.findViewById(R.id.discount_out_contents);
//		mQrLayout = view.findViewById(R.id.qr_layout);
		mBiccamera = view.findViewById(R.id.biccamera);
		mWebshop = view.findViewById(R.id.webshop);
		mWebshop.setOnClickListener(mOnClickListener);
		mKpBlCode = (TextView) view.findViewById(R.id.kp_bl_code);
		mKpBlCode.setOnLongClickListener(new OnLongClickListener() {
			
			@Override
			public boolean onLongClick(View view) {
				String kp_code = mBrand.getKp_code();
				if(!TextUtils.isEmpty(kp_code)){
					ClipboardManager clipboardManager = (ClipboardManager)view.getContext().getSystemService(Context.CLIPBOARD_SERVICE);  
					clipboardManager.setPrimaryClip(ClipData.newPlainText(null, kp_code));  
					if (clipboardManager.hasPrimaryClip()){  
						String s = "クーポンコードコピーしました！";  
						Toast.makeText(view.getContext(), s, Toast.LENGTH_SHORT).show();
					}
				}
				
				return false;
			}
		});

		mNewContent = (TextView) view.findViewById(R.id.new_best);
		mNewsTitle = (TextView) view.findViewById(R.id.new_Title);
		new_layout = (LinearLayout) view.findViewById(R.id.new_layout);
		
		if (mInfo.isLocalDebug()) {
			mCategoryName.setText("XXXXX");
			mDiscountRate.setText("XX" + getString(R.string.x_discount_rate_1));
			mBrandLogo.setImageResource(R.drawable.brand_logo);
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < 10; i++) {
				sb.append("XXXXXXXXXXXXXX");
			}
			mBrandDesc.setText(sb.toString());
			mDiscountOutContents.setText("XXXXXXXXXXX");
		} else {
			if (mBrand != null) {
				notifyDataSetChanged();
			}
		}
		
		view.findViewById(R.id.catch_all).setOnClickListener(mOnClickListener);
		view.findViewById(R.id.next).setOnClickListener(mOnClickListener);

		return view;
	}

	@Override
	protected View onCreateHeader(LayoutInflater inflater, ViewGroup container) {
		View view = inflater.inflate(R.layout.header_text, container, false);
		mTitle = (TextView) view.findViewById(R.id.title);
		// if (isLocalDebug()) {
		// mTitle.setText("XXXX");
		// } else {
		// if (mBrand != null) {
		// mTitle.setText(mBrand.getBrand_name());
		// }
		// }
		mTitle.setText("iLMiO"/*R.string.coupon*/);

		return view;
	}

	// ===========================================================
	// Listener
	// ===========================================================
	private OnClickListener mOnClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.shopelist:
				FragmentTransaction ft = getFragmentManager()
						.beginTransaction();
				ShopListFragment f = new ShopListFragment();
				if (mBrand != null) {
					Bundle args = new Bundle();
					args.putSerializable("brand", mBrand);
					f.setArguments(args);
				}
				ft.add(R.id.container, f);
				ft.addToBackStack(null);
				ft.commit();
				break;
			case R.id.qr:
				tryBarcode();
				break;
			case R.id.webshop:
				if (mBrand == null || TextUtils.isEmpty(mBrand.getEc_url())) {
					return;
				}
				Intent intent = new Intent();
				intent.setAction(Intent.ACTION_VIEW);
				Uri content_url = Uri.parse(mBrand.getEc_url());
				intent.setData(content_url);
				startActivity(intent);
				break;
			case R.id.next:
			case R.id.catch_all:
				FragmentTransaction ft1 = getFragmentManager()
				.beginTransaction();
				NewsTopFragment f1 = new NewsTopFragment();
				ft1.add(R.id.container, f1);
				ft1.addToBackStack(null);
				ft1.commit();
				break;
			default:
				break;
			}
		}
	};

	private OnPageChangeListener mOnPageChangeListener = new OnPageChangeListener() {

		@Override
		public void onPageScrollStateChanged(int state) {

		}

		@Override
		public void onPageScrolled(int position, float positionOffset,
				int positionOffsetPixels) {

		}

		@Override
		public void onPageSelected(int position) {
			mRadioGroup.check(R.id.radio1 + position);
		}
	};

	// ===========================================================
	// Methods
	// ===========================================================

	private void tryBarcode() {
		String dateFrom = mInfo.getString(ConfigInfo.KEY_EXPIRED_DATE_FROM);
		String dateTo = mInfo.getString(ConfigInfo.KEY_EXPIRED_DATE_TO);

		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
		String dateCur = df.format(new Date());
		if (dateTo.compareTo(dateCur) >= 0 && dateCur.compareTo(dateFrom) >= 0) {
			// ok
			requestBarcode();
		} else {
			// need update info
			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
			builder.setMessage(R.string.msg_update);
			builder.setPositiveButton(R.string.yes_update,
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							requestUpdateInfo();
						}
					});
			builder.setNegativeButton(R.string.no,
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {

						}
					});
			AlertDialog dialog = builder.create();
			dialog.show();
		}
	}

	private void requestUpdateInfo() {
		final LoadingDialog dialog = new LoadingDialog(getActivity());
		dialog.show();
		UrlBuilder builder = new UrlBuilder();
		// http://153.121.37.86/index.php?c=users&a=get_login_code&id=2&sign=5d5f3cb3f4b679c2e86ada37397def74
		builder.put("c", "users");
		builder.put("a", "get_login_code");
		builder.put("id", mInfo.getString(ConfigInfo.KEY_ID));
		String url = builder.build();
		KLog.i("url:" + url);
		GsonRequest<LoginInResponse> request = new GsonRequest<LoginInResponse>(
				url, LoginInResponse.class, new Listener<LoginInResponse>() {
					@Override
					public void onResponse(LoginInResponse response) {
						if (null != dialog && dialog.isShowing()) {
							dialog.dismiss();
						}
						KLog.i("error:" + response.getResult().getError());
						try {
							if ("0".equals(response.getResult().getError())) {
								LoginIn info = response.getResult().getList();
								mInfo.putBoolean(ConfigInfo.KEY_LOGIN, true);
								mInfo.putString(ConfigInfo.KEY_ID, info.getId());
								mInfo.putString(ConfigInfo.KEY_RANK_ID,
										info.getRank_id()); // login code
								mInfo.putString(ConfigInfo.KEY_RANK_NAME,
										info.getRank_name());
								mInfo.putString(
										ConfigInfo.KEY_EXPIRED_DATE_FROM,
										info.getExpired_date_from());
								mInfo.putString(ConfigInfo.KEY_EXPIRED_DATE_TO,
										info.getExpired_date_to());
								requestBarcode();
							} else {
								Toast.makeText(getActivity(),
										R.string.request_error,
										Toast.LENGTH_LONG).show();
							}
						} catch (Exception e) {
							if (null != dialog && dialog.isShowing()) {
								dialog.dismiss();
							}
							e.printStackTrace();
							Toast.makeText(getActivity(),
									R.string.request_error, Toast.LENGTH_LONG)
									.show();
						}
					}
				}, new ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						KLog.e("error:" + error.getMessage());
						Toast.makeText(getActivity(), R.string.request_failed,
								Toast.LENGTH_LONG).show();
						finishLoading(false);
					}
				});
		request.setTag("request_update");
		addRequestTag("request_update");
		mVolleyRequest.getRequestQueue().cancelAll("request_update");
		mVolleyRequest.addToRequestQueue(request);
	}

	private void requestBarcode() {
		MainActivity activity = (MainActivity) getActivity();
		activity.requestQrcode();
	}

	private void requestBrandDetail() {
		UrlBuilder builder = new UrlBuilder();
		builder.put("c", "brand");
		builder.put("a", "brand_info");
		builder.put("rank_id", getConfigInfo().getString(ConfigInfo.KEY_RANK_ID));
		builder.put("brand_id", mBrand.getBrand_id());
		String url = builder.build();
		KLog.i("url:" + url);
		GsonRequest<BrandDetailResponse> request = new GsonRequest<BrandDetailResponse>(
				url, BrandDetailResponse.class,
				new Listener<BrandDetailResponse>() {
					@Override
					public void onResponse(BrandDetailResponse response) {
						try {
							if ("0".equals(response.getResult().getError())) {
								mBrand = response.getResult().getList();
								notifyDataSetChanged();
								finishLoading();
							} else {
								Toast.makeText(getActivity(),
										R.string.request_error,
										Toast.LENGTH_LONG).show();
								finishLoading(false);
							}
						} catch (Exception e) {
							e.printStackTrace();
							Toast.makeText(getActivity(),
									R.string.request_error, Toast.LENGTH_LONG)
									.show();
							finishLoading(false);
						}
					}
				}, new ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						KLog.e("error:" + error.getMessage());
						Toast.makeText(getActivity(), R.string.request_failed,
								Toast.LENGTH_LONG).show();
						finishLoading(false);
					}
				});
		request.setTag("brand_detail");
		addRequestTag("brand_detail");
		mVolleyRequest.getRequestQueue().cancelAll("brand_detail");
		mVolleyRequest.addToRequestQueue(request);
	}

	@Override
	public void notifyDataSetChanged() {
		// mTitle.setText(mBrand.getBrand_name());
//		mTitle.setText(R.string.coupon);
		mCategoryName.setText(mBrand.getCat_name());
		mDiscountRate.setText(mBrand.getDiscount_rate() + "%");
		mBrandLogo.setImageUrl(
				NetworkConstants.URL_HOST + mBrand.getBrand_logo(),
				mVolleyRequest.getImageLoader());
		mBrandDesc.setText(mBrand.getBrand_desc());
		mDiscountOutContents.setText(mBrand.getDiscount_out_contents());
		
		if(mBrand.getTitle() == null || mBrand.getTitle().equals(""))
		{
			new_layout.setVisibility(View.GONE);
		}else{
			new_layout.setVisibility(View.VISIBLE);
			mNewsTitle.setText(mBrand.getTitle());
			mNewContent.setText(mBrand.getContents());
		}

		if (mBrand.getQr_flg() == 1) {
			mQR.setVisibility(View.GONE);
			mShopeList.setVisibility(View.GONE);
			mBiccamera.setVisibility(View.GONE);
			mWebshop.setVisibility(View.VISIBLE);
		} else if ("BIC CAMERA".equals(mBrand.getBrand_name())) {
			mQR.setVisibility(View.GONE);
			mShopeList.setVisibility(View.VISIBLE);
			mBiccamera.setVisibility(View.VISIBLE);
			mWebshop.setVisibility(View.GONE);
		} else if ("大塚家具".equals(mBrand.getBrand_name())) {
			mQR.setVisibility(View.GONE);
			mShopeList.setVisibility(View.VISIBLE);
			mBiccamera.setVisibility(View.GONE);
			mWebshop.setVisibility(View.GONE);
		} else {
			mQR.setVisibility(View.VISIBLE);
			mShopeList.setVisibility(View.VISIBLE);
			mBiccamera.setVisibility(View.GONE);
			mWebshop.setVisibility(View.GONE);
		}

		String kp_bl_code = "";
		String kp_code = mBrand.getKp_code();
		String rank_name = getConfigInfo().getString(ConfigInfo.KEY_RANK_NAME);
		if (!TextUtils.isEmpty(kp_code)&& mBrand.getQr_flg() == 1) {
			kp_bl_code = getString(R.string.coupon_code) + "：" + kp_code;
		}

		if (!TextUtils.isEmpty(kp_bl_code)) {
			kp_bl_code = kp_bl_code + "\n" + rank_name;
		} else {
			kp_bl_code = rank_name;
		}

		String temp = mBrand.getBl_code();
		if(temp!=null){
			if (!"".equals(temp)) {
				if("0".equals(mBrand.getBl_code())){
					return;
				}
				kp_bl_code = kp_bl_code + "：" + "\n" +"(" + temp + ")";
			}
			
		}

		if (!TextUtils.isEmpty(kp_bl_code)) {
			mKpBlCode.setVisibility(View.VISIBLE);
			mKpBlCode.setText(kp_bl_code);
		} else {
			mKpBlCode.setVisibility(View.GONE);
		}

		mAdapter.notifyDataSetChanged();
	}

	// ===========================================================
	// Getter & Setter
	// ===========================================================
	// ===========================================================
	// Methods for/from SuperClass/Interfaces
	// ===========================================================
	// ===========================================================
	// Inner and Anonymous Classes
	// ===========================================================
	private class MyAdapter extends FragmentStatePagerAdapter {

		public MyAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int position) {
			ImageFragment f = new ImageFragment();
			Bundle args = new Bundle();
			args.putInt("position", position);
			f.setArguments(args);
			return f;
		}

		@Override
		public int getItemPosition(Object object) {
			return POSITION_NONE;
		}

		@Override
		public int getCount() {
			return 3;
		}
	}

	private class ImageFragment extends Fragment {

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			NetworkImageView view = (NetworkImageView) inflater.inflate(
					R.layout.image, container, false);
			view.setDefaultImageResId(R.drawable.empty_photo);
			view.setErrorImageResId(R.drawable.empty_photo);
			Bundle args = getArguments();
			if (!isLocalDebug() && mBrand != null && args != null) {
				int position = args.getInt("position");
				switch (position) {
				case 0:
					view.setImageUrl(
							NetworkConstants.URL_HOST + mBrand.getBrand_image(),
							mVolleyRequest.getImageLoader());
					break;
				case 1:
					view.setImageUrl(
							NetworkConstants.URL_HOST
									+ mBrand.getBrand_image2(),
							mVolleyRequest.getImageLoader());
					break;
				case 2:
					view.setImageUrl(
							NetworkConstants.URL_HOST
									+ mBrand.getBrand_image3(),
							mVolleyRequest.getImageLoader());
					break;
				default:
					break;
				}
			}
			return view;
		}
	}
}
