package jp.hottea.mitsuidesigntec.fragment.news;

import java.util.ArrayList;
import java.util.List;

import jp.hottea.mitsuidesigntec.android.R;
import jp.hottea.mitsuidesigntec.entity.News;
import jp.hottea.mitsuidesigntec.entity.NewsListResponse;
import jp.hottea.mitsuidesigntec.fragment.BaseFragment;
import jp.hottea.mitsuidesigntec.log.KLog;
import jp.hottea.mitsuidesigntec.util.ConfigInfo;
import jp.hottea.mitsuidesigntec.volley.UrlBuilder;
import jp.hottea.mitsuidesigntec.volley.VolleyRequest;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.GsonRequest;

public class NewsTopFragment extends BaseFragment {
	// ===========================================================
	// Constants
	// ===========================================================
	// ===========================================================
	// Fields
	// ===========================================================
	private ConfigInfo mInfo;
	private VolleyRequest mVolleyRequest;
	// ---
	private ArrayList<News> mNewsList;
	private LayoutInflater mInflater;
	private List<Section> mSections;
	private ListView mListView;
	private MyAdapter mAdapter;

	// ===========================================================
	// Life cycle
	// ===========================================================

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setLoadingEnable(true);
		mInfo = ConfigInfo.getInstance(getActivity());
		mVolleyRequest = VolleyRequest.getInstance(getActivity());
		mNewsList = new ArrayList<News>();
		mSections = new ArrayList<Section>();

		if (mInfo.isLocalDebug()) {
			News news = null;
			for (int i = 0; i < 5; i++) {
				news = new News();
				news.setTitle("XXXX");
				news.setContents("XXXXXXXXXXXXXXXXXXXXXXX");
				news.setInsert_time("XXXXXX");
				news.setSender("AAAAA");
				news.setSupply_id("XXXXXX");
				news.setSupply_time_from("XXXXXX");
				mNewsList.add(news);
			}
			for (int i = 0; i < 5; i++) {
				news = new News();
				news.setTitle("XXXX");
				news.setContents("XXXXXXXXXXXXXXXXXXXXXXX");
				news.setInsert_time("XXXXXX");
				news.setSender("BBBB");
				news.setSupply_id("XXXXXX");
				news.setSupply_time_from("XXXXXX");
				mNewsList.add(news);
			}
			for (int i = 0; i < 5; i++) {
				news = new News();
				news.setTitle("XXXX");
				news.setContents("XXXXXXXXXXXXXXXXXXXXXXX");
				news.setInsert_time("XXXXXX");
				news.setSender("CCCC");
				news.setSupply_id("XXXXXX");
				news.setSupply_time_from("XXXXXX");
				mNewsList.add(news);
			}
		} else {
			requestNewsList();
		}
	}

	@Override
	public View onCreateContent(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_news_top, container,
				false);
		mAdapter = new MyAdapter();
		mListView = (ListView) view.findViewById(R.id.list);
		mListView.setOnItemClickListener(mOnItemClickListener);
		mListView.setAdapter(mAdapter);
		mInflater = LayoutInflater.from(getActivity());

		if (mNewsList != null) {
			notifyDataSetChanged();
		}

		return view;
	}

	@Override
	protected View onCreateHeader(LayoutInflater inflater, ViewGroup container) {
		View view = inflater.inflate(R.layout.header_image, container, false);
		return view;
	}

	// ===========================================================
	// Methods
	// ===========================================================
	private void buildSection() {
		mSections = new ArrayList<Section>();
		Section section = new Section();
		for (News item : mNewsList) {
			if (item.getSender().equals(section.name)) {
				section.add(item);
				item.setFirstItem(false);
			} else {
				section = new Section();
				section.name = item.getSender();
				section.add(item);
				item.setFirstItem(true);
				mSections.add(section);
			}
		}
	}

	private void requestNewsList() {
		UrlBuilder builder = new UrlBuilder();
		builder.put("c", "news");
		builder.put("a", "news_list");
		builder.put("page", "1");
		String url = builder.build();
		KLog.i("url:" + url);

		GsonRequest<NewsListResponse> request = new GsonRequest<NewsListResponse>(
				url, NewsListResponse.class, new Listener<NewsListResponse>() {

					@Override
					public void onResponse(NewsListResponse response) {
						try {
							if ("0".equals(response.getResult().getError())) {
								mNewsList = response.getResult().getList()
										.getNews_list();
								System.out.println("newList size="+mNewsList.size());
								notifyDataSetChanged();
								finishLoading();
							} else {
								Toast.makeText(getActivity(),
										R.string.request_failed,
										Toast.LENGTH_LONG).show();
								finishLoading(false);
							}
						} catch (Exception e) {
							e.printStackTrace();
							Toast.makeText(getActivity(),
									R.string.request_failed, Toast.LENGTH_LONG)
									.show();
							finishLoading(false);
						}
					}
				}, new ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						Toast.makeText(getActivity(), R.string.request_failed,
								Toast.LENGTH_LONG).show();
						finishLoading(false);
					}
				});
		request.setTag("news_list");
		mVolleyRequest.getRequestQueue().cancelAll("news_list");
		mVolleyRequest.addToRequestQueue(request);
	}

	@Override
	public void notifyDataSetChanged() {
		super.notifyDataSetChanged();
		buildSection();
		mAdapter.notifyDataSetChanged();
	}

	// ===========================================================
	// Listener
	// ===========================================================
	private OnItemClickListener mOnItemClickListener = new OnItemClickListener() {

/*		@Override
		public void onItemClick(AdapterView<?> adapterView, View view,
				int section, int position, long id) {
			News news = mAdapter.getItem(section, position);
			FragmentTransaction ft = getFragmentManager().beginTransaction();
			NewsDetailFragment f = new NewsDetailFragment();
			Bundle args = new Bundle();
			args.putSerializable("news", news);
			f.setArguments(args);
			ft.add(R.id.container, f);
			ft.addToBackStack(null);
			ft.commit();
		}

		@Override
		public void onSectionClick(AdapterView<?> adapterView, View view,
				int section, long id) {

		}
*/
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			// TODO Auto-generated method stub
			News news = (News) mAdapter.getItem(position);
			FragmentTransaction ft = getFragmentManager().beginTransaction();
			NewsDetailFragment f = new NewsDetailFragment();
			Bundle args = new Bundle();
			args.putSerializable("news", news);
			f.setArguments(args);
			ft.add(R.id.container, f);
			ft.addToBackStack(null);
			ft.commit();
		}

	};

	// ===========================================================
	// Methods for/from SuperClass/Interfaces
	// ===========================================================
	// ===========================================================
	// Inner and Anonymous Classes
	// ===========================================================
	private class Section {
		public String name;
		public List<News> items = new ArrayList<News>();

		public void add(News item) {
			items.add(item);
		}
	}

	private class ViewHolder {
		View line;
		TextView title;
		TextView subtitle;
		TextView date;
	}
	
	class MyAdapter extends BaseAdapter{

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			if(mNewsList == null){
				return 0;
			}
			return mNewsList.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			if(mNewsList == null || mNewsList.size() <= position){
				return null;
			}
			return mNewsList.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			View view = null;
			ViewHolder holder = null;
			if (convertView == null) {
				view = mInflater.inflate(R.layout.item_normal_next_2, parent,
						false);
				holder = new ViewHolder();
				holder.title = (TextView) view.findViewById(R.id.item_title);
				holder.subtitle = (TextView) view
						.findViewById(R.id.item_subtitle);
				holder.date = (TextView) view.findViewById(R.id.item_date);
				holder.line = view.findViewById(R.id.item_line);
				view.setTag(holder);
			} else {
				view = convertView;
				holder = (ViewHolder) view.getTag();
			}

			News news = (News) getItem(position);

			if (news.isFirstItem()) {
				holder.line.setVisibility(View.GONE);
			} else {
				holder.line.setVisibility(View.VISIBLE);
			}

			holder.title.setText(news.getTitle());
			holder.subtitle.setText(news.getContents());
			holder.date.setText(news.getInsert_time());
			return view;
		}
		
	}

/*	class MyAdapter extends SectionedBaseAdapter {

		public String getSection(int section) {
			return mSections.get(section).name;
		}

		@Override
		public News getItem(int section, int position) {
			return mSections.get(section).items.get(position);
		}

		@Override
		public long getItemId(int section, int position) {
			int id = position;
			while (section > 0) {
				section--;
				id = id + getCountForSection(section) + 1;
			}
			return id;
		}

		public int getSectionPosition(int section) {
			return (int) getItemId(section, 0);
		}

		@Override
		public int getSectionCount() {
			return mSections.size();
		}

		@Override
		public int getCountForSection(int section) {
			return mSections.get(section).items.size();
		}

		@Override
		public View getItemView(int section, int position, View convertView,
				ViewGroup parent) {
			View view = null;
			ViewHolder holder = null;
			if (convertView == null) {
				view = mInflater.inflate(R.layout.item_normal_next_2, parent,
						false);
				holder = new ViewHolder();
				holder.title = (TextView) view.findViewById(R.id.item_title);
				holder.subtitle = (TextView) view
						.findViewById(R.id.item_subtitle);
				holder.date = (TextView) view.findViewById(R.id.item_date);
				holder.line = view.findViewById(R.id.item_line);
				view.setTag(holder);
			} else {
				view = convertView;
				holder = (ViewHolder) view.getTag();
			}

			News news = getItem(section, position);

			if (news.isFirstItem()) {
				holder.line.setVisibility(View.GONE);
			} else {
				holder.line.setVisibility(View.VISIBLE);
			}

			holder.title.setText(news.getTitle());
			holder.subtitle.setText(news.getContents());
			holder.date.setText(news.getInsert_time());
			return view;
		}

		@Override
		public View getSectionHeaderView(int section, View convertView,
				ViewGroup parent) {
			TextView view = null;

			if (convertView == null) {
				view = (TextView) mInflater.inflate(R.layout.item_section_1,
						parent, false);
			} else {
				view = (TextView) convertView;
			}
			view.setText(getSection(section));
			view.setVisibility(View.GONE);
			return view;
		}
		
	}*/

}
