package jp.hottea.mitsuidesigntec.fragment.news;

import jp.hottea.mitsuidesigntec.android.R;
import jp.hottea.mitsuidesigntec.constants.NetworkConstants;
import jp.hottea.mitsuidesigntec.entity.News;
import jp.hottea.mitsuidesigntec.entity.NewsDetailResponse;
import jp.hottea.mitsuidesigntec.fragment.BaseFragment;
import jp.hottea.mitsuidesigntec.log.KLog;
import jp.hottea.mitsuidesigntec.util.ConfigInfo;
import jp.hottea.mitsuidesigntec.volley.UrlBuilder;
import jp.hottea.mitsuidesigntec.volley.VolleyRequest;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.GsonRequest;
import com.android.volley.toolbox.NetworkImageView;

public class NewsDetailFragment extends BaseFragment {
	// ===========================================================
	// Constants
	// ===========================================================
	// ===========================================================
	// Fields
	// ===========================================================
	private ConfigInfo mInfo;
	private VolleyRequest mVolleyRequest;
	private News mNews;
	private String mSenderText;
	// --
	private ViewPager mPager;
	private MyAdapter mAdapter;
	private RadioGroup mRadioGroup;
	private TextView mDate;
	private TextView mTitle;
	private TextView mContentTitle;
	private TextView mContent;

	// ===========================================================
	// Life cycle
	// ===========================================================
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setLoadingEnable(true);
		mInfo = getConfigInfo();
		mVolleyRequest = getVolleyRequest();

		Bundle args = getArguments();

		if (!isLocalDebug() && args != null) {
			News news = (News) args.getSerializable("news");
			mSenderText = news.getSender();
			requestNewDetail(news.getId());
		}
	}

	@Override
	public View onCreateContent(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_news_detail, container,
				false);
		/** pager height */
		FrameLayout layout = (FrameLayout) view.findViewById(R.id.pager_layout);
		layout.getLayoutParams().height = getResources().getDisplayMetrics().widthPixels / 2;

		mAdapter = new MyAdapter(getChildFragmentManager());
		mPager = (ViewPager) view.findViewById(R.id.pager);
		mPager.setAdapter(mAdapter);
		mPager.setOnPageChangeListener(mOnPageChangeListener);

		mRadioGroup = (RadioGroup) view.findViewById(R.id.radio_group);
		mDate = (TextView) view.findViewById(R.id.news_date);
		mContentTitle = (TextView) view.findViewById(R.id.news_title);
		mContent = (TextView) view.findViewById(R.id.news_content);
		mContent.setAutoLinkMask(Linkify.ALL); 
		mContent.setMovementMethod(LinkMovementMethod.getInstance()); 

		if (mInfo.isLocalDebug()) {
			mRadioGroup.check(R.id.radio1);
			mDate.setText("XXXXXXXXX");
			mContentTitle.setText("XXXXXXXXXXXXXXXXX");
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < 5; i++) {
				sb.append("XXXXXXXXXXXXXXXXXXXX");
			}
			mContent.setText(sb.toString());
		} else {
			if (mNews != null) {
				notifyDataSetChanged();
			}
		}

		return view;
	}

	@Override
	protected View onCreateHeader(LayoutInflater inflater, ViewGroup container) {
		View view = inflater.inflate(R.layout.header_text, container, false);
		mTitle = (TextView) view.findViewById(R.id.title);
		if (mInfo.isLocalDebug()) {
			mTitle.setText("XXXXXXXXXXX");
		} else {
			mTitle.setText(mSenderText);
		}
		return view;
	}

	// ===========================================================
	// Methods
	// ===========================================================
	private void requestNewDetail(String news_id) {

		if (TextUtils.isEmpty(news_id)) {
			KLog.e("id:" + news_id);
			return;
		}

		UrlBuilder builder = new UrlBuilder();
		builder.put("c", "news");
		builder.put("a", "news_info");
		builder.put("id", news_id);
		String url = builder.build();
		KLog.i("url:" + url);

		GsonRequest<NewsDetailResponse> request = new GsonRequest<NewsDetailResponse>(
				url, NewsDetailResponse.class,
				new Listener<NewsDetailResponse>() {

					@Override
					public void onResponse(NewsDetailResponse response) {
						try {
							if ("0".equals(response.getResult().getError())) {
								mNews = response.getResult().getList();
								notifyDataSetChanged();
								finishLoading();
							} else {
								finishLoading(false);
							}

						} catch (Exception e) {
							e.printStackTrace();
							finishLoading(false);
						}
					}
				}, new ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						Toast.makeText(getActivity(), R.string.request_failed,
								Toast.LENGTH_LONG).show();
						finishLoading(false);
					}
				});
		request.setTag("news_detail");
		addRequestTag("news_detail");
		mVolleyRequest.getRequestQueue().cancelAll("news_detail");
		mVolleyRequest.addToRequestQueue(request);
	}

	@Override
	public void notifyDataSetChanged() {
		super.notifyDataSetChanged();

		mDate.setText(mNews.getSupply_time_from());
		mContentTitle.setText(mNews.getTitle());
		mContent.setText(mNews.getContents());
		mAdapter.notifyDataSetChanged();
	}

	// ===========================================================
	// Listener
	// ===========================================================
	private OnPageChangeListener mOnPageChangeListener = new OnPageChangeListener() {

		@Override
		public void onPageScrollStateChanged(int state) {

		}

		@Override
		public void onPageScrolled(int position, float positionOffset,
				int positionOffsetPixels) {

		}

		@Override
		public void onPageSelected(int position) {
			mRadioGroup.check(R.id.radio1 + position);
		}
	};

	// ===========================================================
	// Getter & Setter
	// ===========================================================
	// ===========================================================
	// Methods for/from SuperClass/Interfaces
	// ===========================================================
	// ===========================================================
	// Inner and Anonymous Classes
	// ===========================================================
	private class MyAdapter extends FragmentStatePagerAdapter {

		public MyAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int position) {
			Fragment f = new ImageFragment();
			Bundle args = new Bundle();
			args.putInt("position", position);
			f.setArguments(args);
			return f;
		}

		@Override
		public int getItemPosition(Object object) {
			return POSITION_NONE;
		}

		@Override
		public int getCount() {
			return 3;
		}
	}

	private class ImageFragment extends Fragment {

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			NetworkImageView view = (NetworkImageView) inflater.inflate(
					R.layout.image, container, false);
			view.setDefaultImageResId(R.drawable.empty_photo);
			view.setErrorImageResId(R.drawable.empty_photo);
			Bundle args = getArguments();
			if (!isLocalDebug() && mNews != null && args != null) {
				int position = args.getInt("position");
				switch (position) {
				case 0:
					view.setImageUrl(
							NetworkConstants.URL_HOST + mNews.getImg_path1(),
							mVolleyRequest.getImageLoader());
					break;
				case 1:
					view.setImageUrl(
							NetworkConstants.URL_HOST + mNews.getImg_path2(),
							mVolleyRequest.getImageLoader());
					break;
				case 2:
					view.setImageUrl(
							NetworkConstants.URL_HOST + mNews.getImg_path3(),
							mVolleyRequest.getImageLoader());
					break;
				default:
					break;
				}
			}
			return view;
		}
	}
}
