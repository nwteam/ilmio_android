package jp.hottea.mitsuidesigntec.fragment.qr;

import jp.hottea.mitsuidesigntec.android.R;
import jp.hottea.mitsuidesigntec.constants.NetworkConstants;
import jp.hottea.mitsuidesigntec.entity.Coupon;
import jp.hottea.mitsuidesigntec.log.KLog;
import jp.hottea.mitsuidesigntec.util.ConfigInfo;
import jp.hottea.mitsuidesigntec.volley.VolleyRequest;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.Writer;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.oned.ITFWriter;
import com.pkmmte.view.CircularImageView;

public class BarcodeResultFragment extends Fragment {

	private ConfigInfo mInfo;
	private VolleyRequest mVolleyRequest;
	private Button mBackIcon;
	private CircularImageView mBrandIcon;
	private NetworkImageView mBrandLogo;
	private TextView mShopAddress;
	private TextView mDiscountRate;
	private ImageView mBarcodeImage;
	private TextView mBarcode;
	private Button mBackTop;
	private TextView mRankName;
	private TextView mRankId;
	private Coupon mCoupon;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_barcode_result,
				container, false);
		mInfo = ConfigInfo.getInstance(getActivity());
		mVolleyRequest = VolleyRequest.getInstance(getActivity());
		mBackIcon = (Button) view.findViewById(R.id.back);
		mBackIcon.setOnClickListener(mOnClickListener);
		mBrandIcon = (CircularImageView) view.findViewById(R.id.brand_icon);
		mBrandLogo = (NetworkImageView) view.findViewById(R.id.brand_logo);
		mShopAddress = (TextView) view.findViewById(R.id.shop_address);
		mDiscountRate = (TextView) view.findViewById(R.id.x_discount_rate_2);
		mBarcode = (TextView) view.findViewById(R.id.code_num);
		mBarcodeImage = (ImageView) view.findViewById(R.id.code_image);
		mBackTop = (Button) view.findViewById(R.id.back_top_page);
		mBackTop.setOnClickListener(mOnClickListener);
		mRankName = (TextView) view.findViewById(R.id.rank_name);
		mRankId = (TextView) view.findViewById(R.id.rank_id);

		if (mInfo.isLocalDebug()) {
			mBrandIcon.setImageResource(R.drawable.g1);
			mBrandLogo.setImageResource(R.drawable.brand_logo);
			mShopAddress.setText("XXXXXXX");
			mDiscountRate.setText("XXXXXXXXXXXXX");
			mRankName.setText("XXXXXXX");
			mRankId.setText("XXXXXXX");
		}

		if (mCoupon != null) {
			setUpData(mCoupon);
		}

		return view;
	}

	private OnClickListener mOnClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			getActivity().finish();
		}
	};

	public void setUpData(Coupon coupon) {
		mCoupon = coupon;
		ImageLoader loader = mVolleyRequest.getImageLoader();
		loader.get(NetworkConstants.URL_HOST + coupon.getBrand_image(),
				ImageLoader.getImageListener(mBrandIcon,
						R.drawable.empty_photo, R.drawable.empty_photo));
		mBrandLogo.setImageUrl(
				NetworkConstants.URL_HOST + coupon.getBrand_logo(), loader);
		mShopAddress.setText(coupon.getAddress());
		mDiscountRate.setText(String.format(
				getString(R.string.x_discount_rate_2),
				coupon.getDiscount_rate()));

		Bitmap bitmap = getBarcode(coupon.getBarcode());
		if (null != bitmap) {
			mBarcodeImage.setImageBitmap(bitmap);
			mBarcode.setText(coupon.getBarcode());
		}

		mRankName.setText(mInfo.getString(ConfigInfo.KEY_RANK_NAME));
		mRankId.setText(mInfo.getString(ConfigInfo.KEY_RANK_ID));
	}

	private Bitmap getBarcode(String barcode) {
		try {
			if (TextUtils.isEmpty(barcode)) {
				Toast.makeText(getActivity(), R.string.request_barcode_failed,
						Toast.LENGTH_LONG).show();
				return null;
			}
			Writer writer = new ITFWriter();
			String finaldata = Uri.encode(barcode, "utf-8");
			BitMatrix bm = writer
					.encode(finaldata, BarcodeFormat.ITF, 150, 150);
			Bitmap bitmap = Bitmap.createBitmap(150, 150, Config.ARGB_8888);
			for (int i = 0; i < 150; i++) {// width
				for (int j = 0; j < 150; j++) {// height
					bitmap.setPixel(i, j, bm.get(i, j) ? Color.BLACK
							: Color.WHITE);
				}
			}
			return bitmap;
		} catch (WriterException e) {
			Toast.makeText(getActivity(), R.string.request_barcode_failed,
					Toast.LENGTH_LONG).show();
			KLog.e("getBarcode failed");
			e.printStackTrace();
		}
		return null;
	}
}
