package jp.hottea.mitsuidesigntec.fragment.qr;

import jp.hottea.mitsuidesigntec.widget.BarcodeFrameView;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.google.zxing.camera.CameraManager;
import com.google.zxing.fragment.ZxingFragment;

public class BarcodeCaptureFragment extends ZxingFragment {

	private View mScreenshot;
	private BarcodeFrameView mFrameView;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		FrameLayout view = (FrameLayout) super.onCreateView(inflater,
				container, savedInstanceState);

		mScreenshot = new View(getActivity());
		view.addView(mScreenshot, ViewGroup.LayoutParams.MATCH_PARENT,
				ViewGroup.LayoutParams.MATCH_PARENT);
		// view.addView(mScreenshot, 300, 300);

		mFrameView = new BarcodeFrameView(getActivity());
		view.addView(mFrameView, ViewGroup.LayoutParams.MATCH_PARENT,
				ViewGroup.LayoutParams.MATCH_PARENT);
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		mFrameView.setCameraManager(CameraManager.get());
	}
}
