package jp.hottea.mitsuidesigntec.fragment.qr;

import jp.hottea.mitsuidesigntec.android.R;
import jp.hottea.mitsuidesigntec.constants.NetworkConstants;
import jp.hottea.mitsuidesigntec.fragment.BaseFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.TextView;

public class BarcodeTopFragment extends BaseFragment {

	WebView webView;
	
	@Override
	protected View onCreateContent(LayoutInflater inflater,
			ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.fragment_barcode_top, container,
				false);
		webView = (WebView) view.findViewById(R.id.wv_oauth);
		webView.getSettings().setJavaScriptEnabled(true);
		
		WebSettings s = webView.getSettings();     
		s.setBuiltInZoomControls(true);     
		s.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NARROW_COLUMNS);     
		s.setUseWideViewPort(true);     
		s.setLoadWithOverviewMode(true);     
		s.setSavePassword(true);     
		s.setSaveFormData(true);     
		s.setJavaScriptEnabled(true);     
		// enable navigator.geolocation     
		s.setGeolocationEnabled(true);     
		s.setGeolocationDatabasePath("/data/data/org.itri.html5webview/databases/");     
		// enable Web Storage: localStorage, sessionStorage     
		s.setDomStorageEnabled(true); 
		
		webView.requestFocus();

		webView.setScrollBarStyle(0);
		
		webView.loadUrl(NetworkConstants.URL_QA);
		return view;
	}
	@Override
	protected View onCreateHeader(LayoutInflater inflater, ViewGroup container) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.header_text, container, false);

		TextView title = (TextView) view.findViewById(R.id.title);
		title.setText(R.string.qa_title);
		return view;
	}
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}
	
	
	
	
}
