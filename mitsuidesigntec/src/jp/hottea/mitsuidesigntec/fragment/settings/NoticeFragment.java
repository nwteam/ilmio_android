package jp.hottea.mitsuidesigntec.fragment.settings;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.GsonRequest;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import jp.hottea.mitsuidesigntec.android.R;
import jp.hottea.mitsuidesigntec.entity.LoginIn;
import jp.hottea.mitsuidesigntec.entity.LoginInResponse;
import jp.hottea.mitsuidesigntec.fragment.BaseFragment;
import jp.hottea.mitsuidesigntec.log.KLog;
import jp.hottea.mitsuidesigntec.util.ConfigInfo;
import jp.hottea.mitsuidesigntec.volley.UrlBuilder;
import jp.hottea.mitsuidesigntec.volley.VolleyRequest;
import jp.hottea.mitsuidesigntec.widget.LoadingDialog;

public class NoticeFragment extends BaseFragment{

	private ConfigInfo mInfo;
	private VolleyRequest mVolleyRequest;
	
	private Switch mPush;
	private Button mSave;
	
	
	@Override
	public View onCreateContent(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.settings_notice, container,
				false);
		view.setOnClickListener(mOnClickListener);
		mPush = (Switch) view.findViewById(R.id.push);
		mPush.setSwitchTextAppearance(getActivity(),
				R.style.SwitchTextAppearance);
		mSave = (Button) view.findViewById(R.id.save);
		mSave.setOnClickListener(mOnClickListener);
		mPush.setChecked("0".equals(mInfo
				.getString(ConfigInfo.KEY_PUSH_FLAG)));
		return view;
	}

	@Override
	protected View onCreateHeader(LayoutInflater inflater, ViewGroup container) {
		View view = inflater.inflate(R.layout.header_text, container, false);
		TextView title = (TextView) view.findViewById(R.id.title);
		title.setText(R.string.notice);

		return view;
	}


	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		mInfo = getConfigInfo();
		mVolleyRequest = getVolleyRequest();
	}

	private OnClickListener mOnClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.save:
				requestSaveUserInfo();
				break;
			default:
				break;
			}
		}
	};
	
	private void requestSaveUserInfo() {
		final LoadingDialog dialog = new LoadingDialog(getActivity(),
				getResources().getString(R.string.save_loading));
		dialog.show();

		UrlBuilder builder = new UrlBuilder();
		builder.put("c", "users");
		builder.put("a", "update_user_info");
		builder.put("user_id", mInfo.getString(ConfigInfo.KEY_USER_ID));
		builder.put("rank_id", mInfo.getString(ConfigInfo.KEY_RANK_ID));
		builder.put("push_flag", mPush.isChecked() ? "0" : "1");
		builder.put("up_flg", "3");
		mInfo.putString(ConfigInfo.KEY_PUSH_FLAG, mPush.isChecked() ? "0" : "1");

		String url = builder.build();
		KLog.i("url:" + url);

		GsonRequest<LoginInResponse> request = new GsonRequest<LoginInResponse>(
				url, LoginInResponse.class, new Listener<LoginInResponse>() {

					@Override
					public void onResponse(LoginInResponse response) {
						try {
							if (null != dialog && dialog.isShowing()) {
								dialog.dismiss();
							}

							KLog.i("error:" + response.getResult().getError());
							if ("0".equals(response.getResult().getError())) {
								LoginIn info = response.getResult().getList();
								mInfo.putBoolean(ConfigInfo.KEY_LOGIN, true);
								mInfo.putString(ConfigInfo.KEY_ID, info.getId());
								mInfo.putString(ConfigInfo.KEY_RANK_ID,
										info.getRank_id()); // login code
								mInfo.putString(ConfigInfo.KEY_RANK_NAME,
										info.getRank_name());
								SimpleDateFormat df = new SimpleDateFormat(
										"yyyy/MM/dd", Locale.ENGLISH);

								String expired_date_from = info
										.getExpired_date_from();
								if (!TextUtils.isEmpty(expired_date_from)) {
									expired_date_from = expired_date_from
											+ "000";
									Date date = new Date(Long
											.parseLong(expired_date_from));
									expired_date_from = df.format(date);
								}

								String expired_date_to = info
										.getExpired_date_to();
								if (!TextUtils.isEmpty(expired_date_to)) {
									expired_date_to = expired_date_to + "000";
									Date date = new Date(Long
											.parseLong(expired_date_to));
									expired_date_to = df.format(date);
								}

								mInfo.putString(
										ConfigInfo.KEY_EXPIRED_DATE_FROM,
										expired_date_from);
								mInfo.putString(ConfigInfo.KEY_EXPIRED_DATE_TO,
										expired_date_to);
								notifyDataSetChanged();
								Toast.makeText(getActivity(),
										R.string.request_succeed,
										Toast.LENGTH_LONG).show();
							} else {
								Toast.makeText(getActivity(),
										R.string.request_error,
										Toast.LENGTH_LONG).show();
							}
						} catch (Exception e) {
							e.printStackTrace();
							Toast.makeText(getActivity(),
									R.string.request_failed, Toast.LENGTH_LONG)
									.show();
						}
					}
				}, new ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						if (null != dialog && dialog.isShowing()) {
							dialog.dismiss();
						}
						Toast.makeText(getActivity(), R.string.request_failed,
								Toast.LENGTH_LONG).show();
					}
				});
		request.setTag("save_info");
		addRequestTag("save_info");
		mVolleyRequest.getRequestQueue().cancelAll("save_info");
		mVolleyRequest.addToRequestQueue(request);
	}
	
}
