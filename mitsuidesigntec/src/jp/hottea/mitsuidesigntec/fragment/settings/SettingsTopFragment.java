package jp.hottea.mitsuidesigntec.fragment.settings;

import jp.hottea.mitsuidesigntec.activity.TextActivity;
import jp.hottea.mitsuidesigntec.activity.TutorialActivity;
import jp.hottea.mitsuidesigntec.android.R;
import jp.hottea.mitsuidesigntec.fragment.BaseFragment;
import jp.hottea.mitsuidesigntec.fragment.qr.BarcodeTopFragment;
import jp.hottea.mitsuidesigntec.fragment.shopsearch.GoogleMapFragment;
import jp.hottea.mitsuidesigntec.util.ConfigInfo;
import jp.hottea.mitsuidesigntec.volley.VolleyRequest;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class SettingsTopFragment extends BaseFragment {
	// ===========================================================
	// Constants
	// ===========================================================
	// ===========================================================
	// Fields
	// ===========================================================
	private ConfigInfo mInfo;
	private VolleyRequest mVolleyRequest;
	// --
	/*private Switch mPush;
	private EditText mBirthDate;
	private RadioGroup mSex;
	private EditText mAddress;
	private EditText mBuildingType;
	private TextView mMembershipCategory;
	private EditText mLoginCode;
	private TextView mValidTerm;
	private Button mSave;
	// birth day
	private int mYear;
	private int mMonth;
	private int mDay;*/
	
	private ListView listView;
	private LayoutInflater mInflater;
	private String[] items = {"会員種別","ユーザ情報","通知","利用規約","プライバシーポリシー","ヘルプ"};

	// ===========================================================
	// Life cycle
	// ===========================================================
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mInfo = getConfigInfo();
		mVolleyRequest = getVolleyRequest();
	}

	@Override
	public View onCreateContent(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_settings_top, container,
				false);
		
		mInflater = LayoutInflater.from(getActivity());
		MyAdapter adapter = new MyAdapter();
		listView = (ListView) view.findViewById(R.id.listView);
		listView.setAdapter(adapter);
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				Intent intent;
				switch (arg2) {
				case 0:
					getFragmentManager().beginTransaction()
					.add(R.id.container, new MembershipFragment())
					.addToBackStack(null).commit();
					break;
				case 1:
					getFragmentManager().beginTransaction()
					.add(R.id.container, new UserInfoFragment())
					.addToBackStack(null).commit();
					break;
				case 2:
					getFragmentManager().beginTransaction()
					.add(R.id.container, new NoticeFragment())
					.addToBackStack(null).commit();
					break;
				case 3:
					intent = new Intent(getActivity(), TextActivity.class);
					intent.putExtra("type", TextActivity.TYPE_PROTOCOL);
					startActivity(intent);
					break;
				case 4:
					intent = new Intent(getActivity(), TextActivity.class);
					intent.putExtra("type", TextActivity.TYPE_PRIVACY_POLICY);
					startActivity(intent);
					break;
				case 5:
//					intent = new Intent(getActivity(),
//							TutorialActivity.class);
//					intent.putExtra("from", "top");
//					startActivity(intent);

					getFragmentManager().beginTransaction().add(R.id.container,new BarcodeTopFragment()).addToBackStack(null).commit();

					break;
				default:
					break;
				}
			}
		});
		/*mPush = (Switch) view.findViewById(R.id.push);
		mPush.setSwitchTextAppearance(getActivity(),
				R.style.SwitchTextAppearance);
		mBirthDate = (EditText) view.findViewById(R.id.birth_date);
		mBirthDate.setOnClickListener(mOnClickListener);
		mSex = (RadioGroup) view.findViewById(R.id.sex);
		mAddress = (EditText) view.findViewById(R.id.address);
		mAddress.setOnClickListener(mOnClickListener);
		mBuildingType = (EditText) view.findViewById(R.id.building_type);
		mBuildingType.setOnClickListener(mOnClickListener);
		mMembershipCategory = (TextView) view
				.findViewById(R.id.membership_category);
		mLoginCode = (EditText) view.findViewById(R.id.login_code);
		mValidTerm = (TextView) view.findViewById(R.id.valid_term);
		mSave = (Button) view.findViewById(R.id.save);
		mSave.setOnClickListener(mOnClickListener);
		view.findViewById(R.id.protocol).setOnClickListener(mOnClickListener);
		view.findViewById(R.id.privacy_policy).setOnClickListener(
				mOnClickListener);
		view.findViewById(R.id.top).setOnClickListener(mOnClickListener);

		if (mInfo.isLocalDebug()) {
			mBirthDate.setText("XXXXXXXXX");
			mAddress.setText("XXXXXXX");
			mBuildingType.setText("XXXXXXXXXX");
			mMembershipCategory.setText("XXXXX");
			mLoginCode.setText("XXXXXXXXXX");
			mValidTerm.setText("XXXXXXXXX-XXXXXXXXX");
			// date
			final Calendar c = Calendar.getInstance();
			mYear = c.get(Calendar.YEAR);
			mMonth = c.get(Calendar.MONTH);
			mDay = c.get(Calendar.DAY_OF_MONTH);
		} else {
			mPush.setChecked("0".equals(mInfo
					.getString(ConfigInfo.KEY_PUSH_FLAG)));
			mBirthDate.setText(mInfo.getString(ConfigInfo.KEY_BIRTHDAY));
			mSex.check("1".equals(mInfo.getString(ConfigInfo.KEY_SEX)) ? R.id.male
					: R.id.female);
			mAddress.setText(mInfo.getString(ConfigInfo.KEY_ADDRESS));
			mBuildingType.setText(mInfo.getString(ConfigInfo.KEY_BUIDING_TYPE));
			mMembershipCategory.setText(mInfo
					.getString(ConfigInfo.KEY_RANK_NAME));
			mLoginCode.setText(mInfo.getString(ConfigInfo.KEY_RANK_ID));
			mValidTerm.setText(mInfo
					.getString(ConfigInfo.KEY_EXPIRED_DATE_FROM).replace("-",
							"/")
					+ "-"
					+ mInfo.getString(ConfigInfo.KEY_EXPIRED_DATE_TO).replace(
							"-", "/"));
		}*/
		return view;
	}

	@Override
	protected View onCreateHeader(LayoutInflater inflater, ViewGroup container) {
		View view = inflater.inflate(R.layout.header_text, container, false);

		TextView title = (TextView) view.findViewById(R.id.title);
		title.setText(R.string.settings);
		return view;
	}

	// ===========================================================
	// Listener
	// ===========================================================

	/*private OnClickListener mOnClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.birth_date:
				XDatePickerDialog dialog = new XDatePickerDialog(getActivity(),
						mDateSetListener, mYear, mMonth, mDay);
				dialog.setPermanentTitle(R.string.birth_date);
				dialog.show();
				break;
			case R.id.save:
				requestSaveUserInfo();
				break;
			case R.id.address: {
				PopupMenu popup = new PopupMenu(getActivity(), v);
				Menu menu = popup.getMenu();
				String[] buildingTypes = getResources().getStringArray(
						R.array.address);
				for (String item : buildingTypes) {
					menu.add(item);
				}
				popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
					public boolean onMenuItemClick(MenuItem item) {
						mAddress.setText(item.getTitle());
						return true;
					}
				});
				popup.show();
				break;
			}
			case R.id.building_type: {
				PopupMenu popup = new PopupMenu(getActivity(), v);
				Menu menu = popup.getMenu();
				String[] buildingTypes = getResources().getStringArray(
						R.array.building_type);
				for (String item : buildingTypes) {
					menu.add(item);
				}
				popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
					public boolean onMenuItemClick(MenuItem item) {
						mBuildingType.setText(item.getTitle());
						return true;
					}
				});
				popup.show();
				break;
			}
			case R.id.protocol: {
				Intent intent = new Intent(getActivity(), TextActivity.class);
				intent.putExtra("type", TextActivity.TYPE_PROTOCOL);
				startActivity(intent);
				break;
			}
			case R.id.privacy_policy: {
				Intent intent = new Intent(getActivity(), TextActivity.class);
				intent.putExtra("type", TextActivity.TYPE_PRIVACY_POLICY);
				startActivity(intent);
				break;
			}
			case R.id.top: {
				Intent intent = new Intent(getActivity(),
						TutorialActivity.class);
				intent.putExtra("from", "top");
				startActivity(intent);
				break;
			}
			default:
				break;
			}
		}
	};*/

	/*private DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {

		public void onDateSet(DatePicker view, int year, int monthOfYear,
				int dayOfMonth) {
			mYear = year;
			mMonth = monthOfYear;
			mDay = dayOfMonth;
			mBirthDate.setText(new StringBuilder().append(mYear).append("-")
			// Month is 0 based so add 1
					.append(mMonth + 1).append("-").append(mDay));
		}
	};*/

	// ===========================================================
	// Methods
	// ===========================================================
	/*private void requestSaveUserInfo() {
		final LoadingDialog dialog = new LoadingDialog(getActivity(),
				getResources().getString(R.string.save_loading));
		dialog.show();

		UrlBuilder builder = new UrlBuilder();
		builder.put("c", "users");
		builder.put("a", "update_user_info");
		builder.put("birthday", mBirthDate.getText().toString());
		mInfo.putString(ConfigInfo.KEY_BIRTHDAY, mBirthDate.getText()
				.toString());
		builder.put("sex", mSex.getCheckedRadioButtonId() == R.id.male ? "1"
				: "0");
		mInfo.putString(ConfigInfo.KEY_SEX,
				mSex.getCheckedRadioButtonId() == R.id.male ? "1" : "0");
		builder.put("address", mAddress.getText().toString());
		mInfo.putString(ConfigInfo.KEY_ADDRESS, mAddress.getText().toString());
		builder.put("building_type", mBuildingType.getText().toString());
		mInfo.putString(ConfigInfo.KEY_BUIDING_TYPE, mBuildingType.getText()
				.toString());
		builder.put(
				"device_id",
				mInfo.getSharedPreferences().getString(
						GcmActivity.PROPERTY_REG_ID, ""));
		builder.put("device_type", "2");
		builder.put("push_flag", mPush.isChecked() ? "0" : "1");
		mInfo.putString(ConfigInfo.KEY_PUSH_FLAG, mPush.isChecked() ? "0" : "1");
		builder.put("rank_id", mLoginCode.getText().toString());

		String url = builder.build();
		KLog.i("url:" + url);

		GsonRequest<LoginInResponse> request = new GsonRequest<LoginInResponse>(
				url, LoginInResponse.class, new Listener<LoginInResponse>() {

					@Override
					public void onResponse(LoginInResponse response) {
						try {
							if (null != dialog && dialog.isShowing()) {
								dialog.dismiss();
							}

							KLog.i("error:" + response.getResult().getError());
							if ("0".equals(response.getResult().getError())) {
								LoginIn info = response.getResult().getList();
								mInfo.putBoolean(ConfigInfo.KEY_LOGIN, true);
								mInfo.putString(ConfigInfo.KEY_ID, info.getId());
								mInfo.putString(ConfigInfo.KEY_RANK_ID,
										info.getRank_id()); // login code
								mInfo.putString(ConfigInfo.KEY_RANK_NAME,
										info.getRank_name());
								SimpleDateFormat df = new SimpleDateFormat(
										"yyyy/MM/dd", Locale.ENGLISH);

								String expired_date_from = info
										.getExpired_date_from();
								if (!TextUtils.isEmpty(expired_date_from)) {
									expired_date_from = expired_date_from
											+ "000";
									Date date = new Date(Long
											.parseLong(expired_date_from));
									expired_date_from = df.format(date);
								}

								String expired_date_to = info
										.getExpired_date_to();
								if (!TextUtils.isEmpty(expired_date_to)) {
									expired_date_to = expired_date_to + "000";
									Date date = new Date(Long
											.parseLong(expired_date_to));
									expired_date_to = df.format(date);
								}

								mInfo.putString(
										ConfigInfo.KEY_EXPIRED_DATE_FROM,
										expired_date_from);
								mInfo.putString(ConfigInfo.KEY_EXPIRED_DATE_TO,
										expired_date_to);
								notifyDataSetChanged();
								Toast.makeText(getActivity(),
										R.string.request_succeed,
										Toast.LENGTH_LONG).show();
							} else {
								Toast.makeText(getActivity(),
										R.string.request_error,
										Toast.LENGTH_LONG).show();
							}
						} catch (Exception e) {
							e.printStackTrace();
							Toast.makeText(getActivity(),
									R.string.request_failed, Toast.LENGTH_LONG)
									.show();
						}
					}
				}, new ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						if (null != dialog && dialog.isShowing()) {
							dialog.dismiss();
						}
						Toast.makeText(getActivity(), R.string.request_failed,
								Toast.LENGTH_LONG).show();
					}
				});
		request.setTag("save_info");
		addRequestTag("save_info");
		mVolleyRequest.getRequestQueue().cancelAll("save_info");
		mVolleyRequest.addToRequestQueue(request);
	}

	@Override
	public void notifyDataSetChanged() {
		mMembershipCategory.setText(mInfo.getString(ConfigInfo.KEY_RANK_NAME));
		mLoginCode.setText(mInfo.getString(ConfigInfo.KEY_RANK_ID));
		mValidTerm.setText(mInfo.getString(ConfigInfo.KEY_EXPIRED_DATE_FROM)
				.replace("-", "/")
				+ "-"
				+ mInfo.getString(ConfigInfo.KEY_EXPIRED_DATE_TO).replace("-",
						"/"));
	}*/
	// ===========================================================
	// Getter & Setter
	// ===========================================================
	// ===========================================================
	// Methods for/from SuperClass/Interfaces
	// ===========================================================
	// ===========================================================
	// Inner and Anonymous Classes
	// ===========================================================

	class MyAdapter extends BaseAdapter{

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return items.length;
		}

		@Override
		public Object getItem(int arg0) {
			// TODO Auto-generated method stub
			return items[arg0];
		}

		@Override
		public long getItemId(int arg0) {
			// TODO Auto-generated method stub
			return arg0;
		}

		@Override
		public View getView(int arg0, View arg1, ViewGroup arg2) {
			// TODO Auto-generated method stub
			HoldView holdView;
			if(arg1 != null){
				holdView = (HoldView) arg1.getTag();
			}else{
				arg1 = mInflater.inflate(R.layout.settings_list_item, arg2,false);
				holdView = new HoldView();
				holdView.content = (TextView) arg1.findViewById(R.id.content);
				arg1.setTag(holdView);
			}
			String text = (String) getItem(arg0);
			holdView.content.setText(text);
			return arg1;
		}
	}
	
	class HoldView{
		TextView content;
	}
}
