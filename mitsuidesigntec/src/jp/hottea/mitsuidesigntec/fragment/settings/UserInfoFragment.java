package jp.hottea.mitsuidesigntec.fragment.settings;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.GsonRequest;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.PopupMenu;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import jp.hottea.mitsuidesigntec.android.R;
import jp.hottea.mitsuidesigntec.entity.LoginIn;
import jp.hottea.mitsuidesigntec.entity.LoginInResponse;
import jp.hottea.mitsuidesigntec.fragment.BaseFragment;
import jp.hottea.mitsuidesigntec.gcm.GcmActivity;
import jp.hottea.mitsuidesigntec.log.KLog;
import jp.hottea.mitsuidesigntec.util.ConfigInfo;
import jp.hottea.mitsuidesigntec.volley.UrlBuilder;
import jp.hottea.mitsuidesigntec.volley.VolleyRequest;
import jp.hottea.mitsuidesigntec.widget.LoadingDialog;
import jp.hottea.mitsuidesigntec.widget.XDatePickerDialog;

public class UserInfoFragment extends BaseFragment{

	private ConfigInfo mInfo;
	private VolleyRequest mVolleyRequest;
	
	private EditText mBirthDate;
	private RadioGroup mSex;
	private EditText mAddress;
	private EditText mBuildingType;
	
	private Button mSave;
	
	// birth day
		private int mYear;
		private int mMonth;
		private int mDay;
	
	@Override
	public View onCreateContent(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.settings_userinfo, container,
				false);
		view.setOnClickListener(mOnClickListener);
		mBirthDate = (EditText) view.findViewById(R.id.birth_date);
		mBirthDate.setOnClickListener(mOnClickListener);
		mSex = (RadioGroup) view.findViewById(R.id.sex);
		mAddress = (EditText) view.findViewById(R.id.address);
		mAddress.setOnClickListener(mOnClickListener);
		mBuildingType = (EditText) view.findViewById(R.id.building_type);
		mBuildingType.setOnClickListener(mOnClickListener);
		mBirthDate.setText(mInfo.getString(ConfigInfo.KEY_BIRTHDAY));
		mSex.check("1".equals(mInfo.getString(ConfigInfo.KEY_SEX)) ? R.id.male
				: R.id.female);
		mAddress.setText(mInfo.getString(ConfigInfo.KEY_ADDRESS));
		mBuildingType.setText(mInfo.getString(ConfigInfo.KEY_BUIDING_TYPE));
		
		mSave = (Button) view.findViewById(R.id.save);
		mSave.setOnClickListener(mOnClickListener);
		return view;
	}

	@Override
	protected View onCreateHeader(LayoutInflater inflater, ViewGroup container) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.header_text, container, false);

		TextView title = (TextView) view.findViewById(R.id.title);
		title.setText(R.string.user_info);
		return view;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		mInfo = getConfigInfo();
		mVolleyRequest = getVolleyRequest();
	}

	private OnClickListener mOnClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.birth_date:
				XDatePickerDialog dialog = new XDatePickerDialog(getActivity(),
						mDateSetListener, mYear, mMonth, mDay);
				dialog.setPermanentTitle(R.string.birth_date);
				dialog.show();
				break;
			case R.id.save:
				requestSaveUserInfo();
				break;
			case R.id.address: {
				PopupMenu popup = new PopupMenu(getActivity(), v);
				Menu menu = popup.getMenu();
				String[] buildingTypes = getResources().getStringArray(
						R.array.address);
				for (String item : buildingTypes) {
					menu.add(item);
				}
				popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
					public boolean onMenuItemClick(MenuItem item) {
						mAddress.setText(item.getTitle());
						return true;
					}
				});
				popup.show();
				break;
			}
			case R.id.building_type: {
				PopupMenu popup = new PopupMenu(getActivity(), v);
				Menu menu = popup.getMenu();
				String[] buildingTypes = getResources().getStringArray(
						R.array.building_type);
				for (String item : buildingTypes) {
					menu.add(item);
				}
				popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
					public boolean onMenuItemClick(MenuItem item) {
						mBuildingType.setText(item.getTitle());
						return true;
					}
				});
				popup.show();
				break;
			}
			default:
				break;
			}
		}
	};
	
	private DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {

		public void onDateSet(DatePicker view, int year, int monthOfYear,
				int dayOfMonth) {
			mYear = year;
			mMonth = monthOfYear;
			mDay = dayOfMonth;
			mBirthDate.setText(new StringBuilder().append(mYear).append("-")
			// Month is 0 based so add 1
					.append(mMonth + 1).append("-").append(mDay));
		}
	};
	
	private void requestSaveUserInfo() {
		final LoadingDialog dialog = new LoadingDialog(getActivity(),
				getResources().getString(R.string.save_loading));
		dialog.show();

		UrlBuilder builder = new UrlBuilder();
		builder.put("c", "users");
		builder.put("a", "update_user_info");
		builder.put("user_id", mInfo.getString(ConfigInfo.KEY_USER_ID));
		builder.put("birthday", mBirthDate.getText().toString());
		mInfo.putString(ConfigInfo.KEY_BIRTHDAY, mBirthDate.getText()
				.toString());
		builder.put("sex", mSex.getCheckedRadioButtonId() == R.id.male ? "1"
				: "0");
		mInfo.putString(ConfigInfo.KEY_SEX,
				mSex.getCheckedRadioButtonId() == R.id.male ? "1" : "0");
		builder.put("address", mAddress.getText().toString());
		mInfo.putString(ConfigInfo.KEY_ADDRESS, mAddress.getText().toString());
		builder.put("building_type", mBuildingType.getText().toString());
		mInfo.putString(ConfigInfo.KEY_BUIDING_TYPE, mBuildingType.getText()
				.toString());
		String device_id = mInfo.getSharedPreferences().getString(
				GcmActivity.PROPERTY_REG_ID, "");
		if(!device_id.equals("")){
		builder.put(
				"device_id",device_id);
		}
		builder.put("device_type", "2");

		builder.put("up_flg", "2");
		builder.put("rank_id", mInfo.getString(ConfigInfo.KEY_RANK_ID));
		
		String url = builder.build();
		KLog.i("url:" + url);

		GsonRequest<LoginInResponse> request = new GsonRequest<LoginInResponse>(
				url, LoginInResponse.class, new Listener<LoginInResponse>() {

					@Override
					public void onResponse(LoginInResponse response) {
						try {
							if (null != dialog && dialog.isShowing()) {
								dialog.dismiss();
							}

							KLog.i("error:" + response.getResult().getError());
							if ("0".equals(response.getResult().getError())) {
								LoginIn info = response.getResult().getList();
								mInfo.putBoolean(ConfigInfo.KEY_LOGIN, true);
								mInfo.putString(ConfigInfo.KEY_ID, info.getId());
								mInfo.putString(ConfigInfo.KEY_RANK_ID,
										info.getRank_id()); // login code
								mInfo.putString(ConfigInfo.KEY_RANK_NAME,
										info.getRank_name());
								SimpleDateFormat df = new SimpleDateFormat(
										"yyyy/MM/dd", Locale.ENGLISH);

								String expired_date_from = info
										.getExpired_date_from();
								if (!TextUtils.isEmpty(expired_date_from)) {
									expired_date_from = expired_date_from
											+ "000";
									Date date = new Date(Long
											.parseLong(expired_date_from));
									expired_date_from = df.format(date);
								}

								String expired_date_to = info
										.getExpired_date_to();
								if (!TextUtils.isEmpty(expired_date_to)) {
									expired_date_to = expired_date_to + "000";
									Date date = new Date(Long
											.parseLong(expired_date_to));
									expired_date_to = df.format(date);
								}

								mInfo.putString(
										ConfigInfo.KEY_EXPIRED_DATE_FROM,
										expired_date_from);
								mInfo.putString(ConfigInfo.KEY_EXPIRED_DATE_TO,
										expired_date_to);
								notifyDataSetChanged();
								Toast.makeText(getActivity(),
										R.string.request_succeed,
										Toast.LENGTH_LONG).show();
							} else {
								Toast.makeText(getActivity(),
										R.string.request_error,
										Toast.LENGTH_LONG).show();
							}
						} catch (Exception e) {
							e.printStackTrace();
							Toast.makeText(getActivity(),
									R.string.request_failed, Toast.LENGTH_LONG)
									.show();
						}
					}
				}, new ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						if (null != dialog && dialog.isShowing()) {
							dialog.dismiss();
						}
						Toast.makeText(getActivity(), R.string.request_failed,
								Toast.LENGTH_LONG).show();
					}
				});
		request.setTag("save_info");
		addRequestTag("save_info");
		mVolleyRequest.getRequestQueue().cancelAll("save_info");
		mVolleyRequest.addToRequestQueue(request);
	}

	
}
