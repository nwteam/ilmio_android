package jp.hottea.mitsuidesigntec.fragment;

import java.util.ArrayList;
import java.util.List;

import jp.hottea.mitsuidesigntec.android.R;
import jp.hottea.mitsuidesigntec.util.ConfigInfo;
import jp.hottea.mitsuidesigntec.volley.VolleyRequest;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.ProgressBar;
import android.widget.TextView;

public class BaseFragment extends Fragment {
	// ===========================================================
	// Constants
	// ===========================================================
	// ===========================================================
	// Fields
	// ===========================================================
	private VolleyRequest mVolleyRequest;
	private ConfigInfo mConfigInfo;
	private Handler mHandler;
	private boolean mLoadingEnable = false;
	private ViewGroup mContentContainer;
	private ViewGroup mLoadingContainer;
	private List<String> mRequestTags;

	// ===========================================================
	// Life cycle
	// ===========================================================
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		mConfigInfo = ConfigInfo.getInstance(activity.getApplicationContext());
		mVolleyRequest = VolleyRequest.getInstance(activity
				.getApplicationContext());
		mHandler = new Handler();
		mRequestTags = new ArrayList<String>();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		/** common header **/
		View view = inflater.inflate(R.layout.fragment_base, container, false);

		/** setup header **/
		ViewGroup headerContainer = (ViewGroup) view
				.findViewById(R.id.header_container);
		View header = onCreateHeader(inflater, headerContainer);
		headerContainer.removeAllViews();
		headerContainer.addView(header);

		/** setup content **/
		mContentContainer = (ViewGroup) view
				.findViewById(R.id.content_container);
		View content = onCreateContent(inflater, mContentContainer,
				savedInstanceState);
		mContentContainer.removeAllViews();
		mContentContainer.addView(content);

		/** setup loading **/
		mLoadingContainer = (ViewGroup) view
				.findViewById(R.id.loading_container);
		return view;
	}

	/** for content **/
	protected View onCreateContent(LayoutInflater inflater,
			ViewGroup container, Bundle savedInstanceState) {
		return null;
	}

	/** for header **/
	protected View onCreateHeader(LayoutInflater inflater, ViewGroup container) {
		return null;
	}

	@Override
	public void onResume() {
		super.onResume();

		// build loading
		if (getLoadingEnable()) {
			setLoadingEnable(false);
			startLoading();/* just once */
			if (isLocalDebug()) {
				finishLoadingDelay();
			}
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		for (String tag : mRequestTags) {
			mVolleyRequest.getRequestQueue().cancelAll(tag);
		}
	}

	// ===========================================================
	// Getter & Setter
	// ===========================================================
	protected ConfigInfo getConfigInfo() {
		return mConfigInfo;
	}

	protected VolleyRequest getVolleyRequest() {
		return mVolleyRequest;
	}

	public boolean isLocalDebug() {
		return mConfigInfo.isLocalDebug();
	}

	protected void setLoadingEnable(boolean enable) {
		mLoadingEnable = enable;
	}

	protected boolean getLoadingEnable() {
		return mLoadingEnable;
	}

	protected void addRequestTag(String tag) {
		mRequestTags.add(tag);
	}

	// ===========================================================
	// Methods for/from SuperClass/Interfaces
	// ===========================================================
	/**
	 * Custom onResult
	 */
	public void onResult(int requestCode, int resultCode, Intent data) {

	}

	public void notifyDataSetChanged() {

	}

	// ===========================================================
	// Methods
	// ===========================================================

	protected void startLoading() {

		try {
			// hide content
			mContentContainer.setVisibility(View.INVISIBLE);

			// add loading
			View loadingView = LayoutInflater.from(getActivity()).inflate(
					R.layout.content_loading_1, mLoadingContainer, false);
			mLoadingContainer.removeAllViews();
			mLoadingContainer.setClickable(true);
			mLoadingContainer.addView(loadingView);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	protected void finishLoading(boolean result) {
		try {
			if (result == true) {
				// show content
				mContentContainer.setVisibility(View.VISIBLE);
				Animation fadeIn = AnimationUtils.loadAnimation(getActivity(),
						android.R.anim.fade_in);
				mContentContainer.clearAnimation();
				mContentContainer.startAnimation(fadeIn);

				// remove loading
				mLoadingContainer.setClickable(false);
				View loadingView = mLoadingContainer.getChildAt(0);
				Animation fadeOut = AnimationUtils.loadAnimation(getActivity(),
						android.R.anim.fade_out);
				fadeOut.setAnimationListener(new AnimationListener() {

					@Override
					public void onAnimationStart(Animation animation) {
					}

					@Override
					public void onAnimationRepeat(Animation animation) {
					}

					@Override
					public void onAnimationEnd(Animation animation) {
						mLoadingContainer.removeAllViews();
					}
				});
				loadingView.clearAnimation();
				loadingView.startAnimation(fadeOut);
			} else {
				ProgressBar progress = (ProgressBar) mLoadingContainer
						.findViewById(R.id.progress);
				progress.setVisibility(View.GONE);
				TextView msg = (TextView) mLoadingContainer
						.findViewById(R.id.msg);
				msg.setText(R.string.request_failed);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	protected void finishLoading() {
		finishLoading(true);
	}

	protected void finishLoadingDelay() {
		finishLoadingDelay(1000);
	}

	protected void finishLoadingDelay(long delayMillis) {
		mHandler.postDelayed(new Runnable() {
			@Override
			public void run() {
				finishLoading();
			}
		}, delayMillis);
	}

	// ===========================================================
	// Inner and Anonymous Classes
	// ===========================================================

}
