package jp.hottea.mitsuidesigntec.fragment.common;

import jp.hottea.mitsuidesigntec.android.R;
import jp.hottea.mitsuidesigntec.fragment.BaseFragment;
import jp.hottea.mitsuidesigntec.log.KLog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class CommonMapFragment extends BaseFragment {
	// ===========================================================
	// Constants
	// ===========================================================
	// ===========================================================
	// Fields
	// ===========================================================
	private GoogleMap mMap;
	private MapView mMapView;
	private LatLng mMyLatLng;

	// ===========================================================
	// Life cycle
	// ===========================================================
	@Override
	public void onCreate(Bundle savedInstanceState) {
		KLog.i("onCreate");
		super.onCreate(savedInstanceState);

		Bundle args = getArguments();
		if (args != null) {
			double latitude = args.getDouble("latitude");
			double longitude = args.getDouble("longitude");
			mMyLatLng = new LatLng(latitude, longitude);
		} else {
			// NO-OP
		}

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		KLog.i("onCreateView");
		View view = inflater.inflate(R.layout.fragment_googlemap, container,
				false);
		MapsInitializer.initialize(getActivity().getApplicationContext());
		mMapView = (MapView) view.findViewById(R.id.map);
		mMapView.onCreate(savedInstanceState);
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		KLog.i("onActivityCreated");
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public void onResume() {
		KLog.i("onResume");
		super.onResume();
		mMapView.onResume();
		setUpMap();
		setUpCamera();
		setUpMyMarker();
	}

	@Override
	public void onPause() {
		KLog.i("onPause");
		super.onPause();
		mMapView.onPause();
	}

	@Override
	public void onDestroy() {
		KLog.i("onDestroy");
		super.onDestroy();
		mMapView.onDestroy();
	}

	@Override
	public void onLowMemory() {
		KLog.i("onLowMemory");
		super.onLowMemory();
		mMapView.onLowMemory();
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		KLog.i("onSaveInstanceState");
		super.onSaveInstanceState(outState);
		mMapView.onSaveInstanceState(outState);
	}

	// ===========================================================
	// Methods for/from SuperClass/Interfaces
	// ===========================================================
	// ===========================================================
	// Listener
	// ===========================================================
	// ===========================================================
	// Getter & Setter
	// ===========================================================
	// ===========================================================
	// Methods
	// ===========================================================
	private void setUpMap() {
		KLog.i("setUpMap");
		mMap = ((MapView) getView().findViewById(R.id.map)).getMap();
		mMap.clear();
		// Hide the zoom controls as the button panel will cover it.
		mMap.getUiSettings().setZoomControlsEnabled(false);
	}

	private void setUpCamera() {
		KLog.i("setUpCamera");
		CameraPosition cameraPosition = new CameraPosition.Builder()
				.target(mMyLatLng).zoom(15.5f).bearing(0).tilt(0).build();
		mMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
	}

	private void setUpMyMarker() {
		KLog.i("setUpMarkers");
		mMap.addMarker(new MarkerOptions().position(mMyLatLng).icon(
				BitmapDescriptorFactory
						.defaultMarker(BitmapDescriptorFactory.HUE_CYAN)));
	}
	// ===========================================================
	// Inner and Anonymous Classes
	// ===========================================================
}
