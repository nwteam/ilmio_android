package jp.hottea.mitsuidesigntec.fragment;

import jp.hottea.mitsuidesigntec.android.R;
import jp.hottea.mitsuidesigntec.constants.FragmentConstants;
import jp.hottea.mitsuidesigntec.constants.TypeConstants;
import jp.hottea.mitsuidesigntec.fragment.coupon.CouponTopFragment;
import jp.hottea.mitsuidesigntec.fragment.news.NewsTopFragment;
import jp.hottea.mitsuidesigntec.fragment.qr.BarcodeTopFragment;
import jp.hottea.mitsuidesigntec.fragment.settings.SettingsTopFragment;
import jp.hottea.mitsuidesigntec.fragment.shopsearch.ShopSearchTopFragment;
import android.os.Bundle;
import android.support.v4.app.FragmentManager.OnBackStackChangedListener;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class TabFragment extends BaseFragment {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		System.out.println("TabFragment onCreateView");
		View view = inflater.inflate(R.layout.fragment_tab, container, false);
		setLoadingEnable(false);
		if (null == savedInstanceState) {
			Bundle data = getArguments();
			int type = 0;
			if (null != data) {
				type = data.getInt("type");
			}

			switch (type) {
			case TypeConstants.TYPE_COUPON: {
				if (null == getChildFragmentManager().findFragmentByTag(
						FragmentConstants.COUPON_TOP)) {
					getChildFragmentManager().addOnBackStackChangedListener(
							new OnBackStackChangedListener() {

								@Override
								public void onBackStackChanged() {

								}
							});

					FragmentTransaction ft = getChildFragmentManager()
							.beginTransaction();
					CouponTopFragment f = new CouponTopFragment();
					ft.add(R.id.container, f, FragmentConstants.COUPON_TOP);
					ft.commit();
				}
				break;
			}
			case TypeConstants.TYPE_SHOP_SEARCH: {
				if (null == getChildFragmentManager().findFragmentByTag(
						FragmentConstants.SHOP_SEARCH_TOP)) {
					FragmentTransaction ft = getChildFragmentManager()
							.beginTransaction();
					ft.add(R.id.container, new ShopSearchTopFragment(),
							FragmentConstants.SHOP_SEARCH_TOP);
					ft.commit();
				}
				break;
			}
			case TypeConstants.TYPE_QR_CODE: {
				if (null == getChildFragmentManager().findFragmentByTag(
						FragmentConstants.QR_TOP)) {
					FragmentTransaction ft = getChildFragmentManager()
							.beginTransaction();
					ft.add(R.id.container, new BarcodeTopFragment(),
							FragmentConstants.QR_TOP);
					ft.commit();
				}
				break;
			}
			case TypeConstants.TYPE_NEWS: {
				if (null == getChildFragmentManager().findFragmentByTag(
						FragmentConstants.NEWS_TOP)) {
					FragmentTransaction ft = getChildFragmentManager()
							.beginTransaction();
					ft.add(R.id.container, new NewsTopFragment(),
							FragmentConstants.NEWS_TOP);
					ft.commit();
				}

				break;
			}
			case TypeConstants.TYPE_SETTINGS: {
				if (null == getChildFragmentManager().findFragmentByTag(
						FragmentConstants.SETTINGS_TOP)) {
					FragmentTransaction ft = getChildFragmentManager()
							.beginTransaction();
					ft.add(R.id.container, new SettingsTopFragment(),
							FragmentConstants.SETTINGS_TOP);
					ft.commit();
				}
				break;
			}
			default:
				break;
			}
		}
		return view;
	}
}
