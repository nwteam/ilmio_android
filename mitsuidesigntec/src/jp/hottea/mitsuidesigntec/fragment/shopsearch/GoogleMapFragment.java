package jp.hottea.mitsuidesigntec.fragment.shopsearch;

import java.util.HashMap;
import java.util.List;

import jp.hottea.mitsuidesigntec.android.R;
import jp.hottea.mitsuidesigntec.entity.NearbyShop;
import jp.hottea.mitsuidesigntec.entity.NearbyShopResponse;
import jp.hottea.mitsuidesigntec.fragment.BaseFragment;
import jp.hottea.mitsuidesigntec.log.KLog;
import jp.hottea.mitsuidesigntec.util.ConfigInfo;
import jp.hottea.mitsuidesigntec.volley.UrlBuilder;
import jp.hottea.mitsuidesigntec.volley.VolleyRequest;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.Toast;

import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.GsonRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class GoogleMapFragment extends BaseFragment {
	// ===========================================================
	// Constants
	// ===========================================================
	// location type
	private static final int LOCATION_TYPE_LAST = 0;
	private static final int LOCATION_TYPE_NETWORK = 1;
	private static final int LOCATION_TYPE_GPS = 2;
	// handler msg
	private static final int MSG_REMOVE_GPS = 1;
	private static final int MSG_SETUP_MAP = 2;
	// debug
	private static final LatLng LATLNG_DEBUG = new LatLng(35.608720, 139.667634);
	// ===========================================================
	// Fields
	// ===========================================================
	private VolleyRequest mVolleyRequest;
	private ConfigInfo mInfo;
	private GoogleMap mMap;
	private MapView mMapView;
	private LocationManager mLocationManager;
	private int mLocationType = LOCATION_TYPE_LAST;
	private LatLng mMyLatLng;
	private List<NearbyShop> mShops;
	private HashMap<Marker, NearbyShop> mHashMap;

	// ===========================================================
	// Life cycle
	// ===========================================================
	@Override
	public void onCreate(Bundle savedInstanceState) {
		KLog.i("onCreate");
		super.onCreate(savedInstanceState);
		// set up info
		mInfo = getConfigInfo();
		mVolleyRequest = getVolleyRequest();
		mHashMap = new HashMap<Marker, NearbyShop>();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		KLog.i("onCreateView");
		View view = inflater.inflate(R.layout.fragment_googlemap, container,
				false);
		MapsInitializer.initialize(getActivity().getApplicationContext());
		mMapView = (MapView) view.findViewById(R.id.map);
		mMapView.onCreate(savedInstanceState);
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		KLog.i("onActivityCreated");
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public void onResume() {
		KLog.i("onResume");
		super.onResume();
		mMapView.onResume();
		setUpMyPosition();
		requestShopPosition();
		setUpMap();
		setUpCamera();
		setUpMyMarker();
	}

	@Override
	public void onPause() {
		KLog.i("onPause");
		super.onPause();
		mMapView.onPause();
	}

	@Override
	public void onDestroy() {
		KLog.i("onDestroy");
		super.onDestroy();
		mMapView.onDestroy();
	}

	@Override
	public void onLowMemory() {
		KLog.i("onLowMemory");
		super.onLowMemory();
		mMapView.onLowMemory();
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		KLog.i("onSaveInstanceState");
		super.onSaveInstanceState(outState);
		mMapView.onSaveInstanceState(outState);
	}

	// ===========================================================
	// Methods
	// ===========================================================
	private void setUpMyPosition() {
		KLog.i("setUpPosition");
		// request my position
		mLocationManager = (LocationManager) getActivity().getSystemService(
				Context.LOCATION_SERVICE);
		mLocationManager.requestLocationUpdates(
				LocationManager.NETWORK_PROVIDER, 0, 0,
				mLocationNetworkListener);
		mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
				0, 0, mLocationGpsListener);
		// get last location
		Location location = mLocationManager
				.getLastKnownLocation(LocationManager.GPS_PROVIDER);
		if (null == location) {
			location = mLocationManager
					.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
		}

		if (null != location) {
			mMyLatLng = new LatLng(location.getLatitude(),
					location.getLongitude());
		} else {
			// set up a default position
			mMyLatLng = new LatLng(33.588566, 130.398571);
		}

		if (mInfo.isMapDebug()) {
			mMyLatLng = LATLNG_DEBUG;
		}

		// remove GPS after 5000 ms
		mHandler.sendEmptyMessageDelayed(MSG_REMOVE_GPS, 3000);
	}

	private void requestShopPosition() {
		// first cancel request
		mVolleyRequest.getRequestQueue().cancelAll("nearshops");

		// http request
		UrlBuilder builder = new UrlBuilder();
		builder.put("c", "shop");
		builder.put("a", "nearby_shop");
		builder.put("longitude", String.valueOf(mMyLatLng.longitude));
		builder.put("latitude", String.valueOf(mMyLatLng.latitude));
		builder.put("rank_id", getConfigInfo().getString(ConfigInfo.KEY_ID));
		String url = builder.build();

		KLog.i("url:" + url);

		GsonRequest<NearbyShopResponse> gsonRequest = new GsonRequest<NearbyShopResponse>(
				url, NearbyShopResponse.class, null,
				new Listener<NearbyShopResponse>() {
					@Override
					public void onResponse(NearbyShopResponse response) {
						KLog.i("onResponse");
						try {
							if (null != response
									&& "0".equals(response.getResult()
											.getError())) {
								mShops = response.getResult().getList();
								if (null == mShops) {
									Toast.makeText(getActivity(),
											R.string.no_nearby_shop,
											Toast.LENGTH_SHORT).show();
								} else if (mShops.isEmpty()) {
									Toast.makeText(getActivity(),
											R.string.no_nearby_shop,
											Toast.LENGTH_SHORT).show();
								} else {
									setUpMap();
									setUpCamera();
									setUpMyMarker();
									setUpShopMarkers();
								}
							} else {
								Toast.makeText(getActivity(),
										R.string.no_nearby_shop,
										Toast.LENGTH_SHORT).show();
							}
						} catch (Exception e) {
							KLog.e("error response at onResponse!");
							Toast.makeText(getActivity(),
									R.string.request_error, Toast.LENGTH_SHORT)
									.show();
						}
					}
				}, new ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						KLog.e("onErrorResponse");
						Toast.makeText(getActivity(), R.string.request_error,
								Toast.LENGTH_SHORT).show();
					}
				});
		gsonRequest.setTag("nearby_shops");
		addRequestTag("nearby_shops");
		mVolleyRequest.getRequestQueue().cancelAll("nearby_shops");
		mVolleyRequest.addToRequestQueue(gsonRequest);
	}

	private void setUpMap() {
		KLog.i("setUpMap");
		mMap = ((MapView) getView().findViewById(R.id.map)).getMap();
		mMap.clear();
		mMap.setOnInfoWindowClickListener(mOnInfoWindowClickListener);
		// Hide the zoom controls as the button panel will cover it.
		mMap.getUiSettings().setZoomControlsEnabled(false);
	}

	private void setUpCamera() {
		KLog.i("setUpCamera");
		CameraPosition cameraPosition = new CameraPosition.Builder()
				.target(mMyLatLng).zoom(15.5f).bearing(0).tilt(0).build();
		mMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
	}

	private void setUpMyMarker() {
		KLog.i("setUpMarkers");
		Marker markder = mMap.addMarker(new MarkerOptions().position(mMyLatLng)
				.icon(BitmapDescriptorFactory
						.defaultMarker(BitmapDescriptorFactory.HUE_CYAN)));
		// mHashMap.put(markder, null);
	}

	private void setUpShopMarkers() {
		KLog.i("setUpMarkers");
		if (null != mShops) {
			mHashMap.clear();
			LatLng latLng = null;
			Marker marker = null;
			for (NearbyShop shop : mShops) {
				latLng = new LatLng(Double.parseDouble(shop.getLatitude()),
						Double.parseDouble(shop.getLongitude()));
				marker = mMap
						.addMarker(new MarkerOptions()
								.position(latLng)
								.title(shop.getShop_name())
								.icon(BitmapDescriptorFactory
										.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
				mHashMap.put(marker, shop);
			}
		}
	}

	// ===========================================================
	// Listener
	// ===========================================================
	private OnInfoWindowClickListener mOnInfoWindowClickListener = new OnInfoWindowClickListener() {

		@Override
		public void onInfoWindowClick(Marker marker) {
			NearbyShop shop = mHashMap.get(marker);
			Fragment f = new ShopDetailFragment3();
			Bundle args = new Bundle();
			args.putSerializable("shop", shop);
			f.setArguments(args);
			getFragmentManager().beginTransaction().add(R.id.container, f)
					.addToBackStack(null).commit();

		}
	};

	private LocationListener mLocationNetworkListener = new LocationListener() {
		@Override
		public void onLocationChanged(Location location) {
			KLog.i("LocationNetworkListener onLocationChanged");

			if (mLocationType == LOCATION_TYPE_LAST) {
				mLocationType = LOCATION_TYPE_NETWORK;
				KLog.i("latitude:" + location.getLatitude());
				KLog.i("longitude:" + location.getLongitude());
				mMyLatLng = new LatLng(location.getLatitude(),
						location.getLongitude());
				if (mInfo.isMapDebug()) {
					mMyLatLng = LATLNG_DEBUG;
				}
				mLocationManager.removeUpdates(mLocationNetworkListener);
				setUpMap();
				setUpCamera();
				setUpMyMarker();
				requestShopPosition();
			}
		}

		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {
			KLog.i("LocationNetworkListener onStatusChanged");

		}

		@Override
		public void onProviderEnabled(String provider) {
			KLog.i("LocationNetworkListener onProviderEnabled");

		}

		@Override
		public void onProviderDisabled(String provider) {
			KLog.i("LocationNetworkListener onProviderDisabled");

		}

	};

	private LocationListener mLocationGpsListener = new LocationListener() {

		@Override
		public void onLocationChanged(Location location) {
			KLog.i("LocationGpsListener onLocationChanged");
			mLocationType = LOCATION_TYPE_GPS;
			KLog.i("latitude:" + location.getLatitude());
			KLog.i("longitude:" + location.getLongitude());
			mMyLatLng = new LatLng(location.getLatitude(),
					location.getLongitude());
			if (mInfo.isMapDebug()) {
				mMyLatLng = LATLNG_DEBUG;
			}
			mLocationManager.removeUpdates(mLocationGpsListener);
			setUpMap();
			setUpCamera();
			setUpMyMarker();
			requestShopPosition();
		}

		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {
			KLog.i("LocationGpsListener onStatusChanged");

		}

		@Override
		public void onProviderEnabled(String provider) {
			KLog.i("LocationGpsListener onProviderEnabled");

		}

		@Override
		public void onProviderDisabled(String provider) {
			KLog.i("LocationGpsListener onProviderDisabled");

		}
	};

	// ===========================================================
	// Getter & Setter
	// ===========================================================
	// ===========================================================
	// Methods for/from SuperClass/Interfaces
	// ===========================================================
	// ===========================================================
	// Inner and Anonymous Classes
	// ===========================================================

	private Handler mHandler = new Handler() {
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case MSG_REMOVE_GPS:
				if (mLocationType != LOCATION_TYPE_GPS) {
					KLog.i("mLocationManager.removeUpdates(mLocationGpsListener);");
					mLocationManager.removeUpdates(mLocationGpsListener);
				}
				break;
			case MSG_SETUP_MAP:
				setUpMap();
				break;
			default:
				break;
			}
		}
	};
}
