package jp.hottea.mitsuidesigntec.fragment.shopsearch;

import jp.hottea.mitsuidesigntec.activity.MainActivity;
import jp.hottea.mitsuidesigntec.android.R;
import jp.hottea.mitsuidesigntec.constants.NetworkConstants;
import jp.hottea.mitsuidesigntec.entity.Shop;
import jp.hottea.mitsuidesigntec.entity.ShopDetailResponse;
import jp.hottea.mitsuidesigntec.fragment.BaseFragment;
import jp.hottea.mitsuidesigntec.fragment.common.CommonMapFragment;
import jp.hottea.mitsuidesigntec.log.KLog;
import jp.hottea.mitsuidesigntec.util.ConfigInfo;
import jp.hottea.mitsuidesigntec.volley.UrlBuilder;
import jp.hottea.mitsuidesigntec.volley.VolleyRequest;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.GsonRequest;
import com.android.volley.toolbox.NetworkImageView;

public class ShopDetailFragment2 extends BaseFragment {
	// ===========================================================
	// Constants
	// ===========================================================
	// ===========================================================
	// Fields
	// ===========================================================
	private ConfigInfo mInfo;
	private VolleyRequest mVolleyRequest;
	private Shop mShop;
	// --
	private NetworkImageView mBrandLogo;
	private TextView mShopName;
	private TextView mTimeFromTo;
	private TextView mRestDay;
	private TextView mAddress;
	private TextView mTel;
	private Button mCoupon;
	private TextView mMap;

	// private Button mQR;

	// ===========================================================
	// Life cycle
	// ===========================================================
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setLoadingEnable(true);
		mInfo = getConfigInfo();
		mVolleyRequest = getVolleyRequest();

		Bundle args = getArguments();
		if (args != null) {
			mShop = (Shop) args.getSerializable("shop");
			requestShopDetail();
		}
	}

	@Override
	public View onCreateContent(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_shop_detail, container,
				false);
		mBrandLogo = (NetworkImageView) view.findViewById(R.id.brand_logo);
		mBrandLogo.setDefaultImageResId(R.drawable.empty_photo);
		mBrandLogo.setErrorImageResId(R.drawable.empty_photo);
		mShopName = (TextView) view.findViewById(R.id.shop_name);
		mTimeFromTo = (TextView) view.findViewById(R.id.business_time);
		mRestDay = (TextView) view.findViewById(R.id.rest_day);
		mAddress = (TextView) view.findViewById(R.id.address);
		mTel = (TextView) view.findViewById(R.id.tel);
		mCoupon = (Button) view.findViewById(R.id.coupon);
		mCoupon.setOnClickListener(mOnClickListener);
		mMap = (TextView) view.findViewById(R.id.map);
		mMap.setOnClickListener(mOnClickListener);
		// mQR = (Button) view.findViewById(R.id.qr);
		// mQR.setOnClickListener(mOnClickListener);

		// int screenWidth = getResources().getDisplayMetrics().widthPixels;
		// int spacing = getResources().getDimensionPixelSize(R.dimen.dp_15);
		// int width = (screenWidth - spacing * 3) / 2;
		// mCoupon.getLayoutParams().width = width;
		// mQR.getLayoutParams().width = width;

		if (mInfo.isLocalDebug()) {
			mBrandLogo.setImageResource(R.drawable.brand_logo);
			mShopName.setText("XXXXXXX");
			mTimeFromTo.setText(String.format(getString(R.string.time_from_to),
					"11:00", "20:00"));
			mRestDay.setText("XXXXXXXX");
			mAddress.setText("XXXXXXXX");
			mTel.setText("03-1234-5678");
		} else {
			if (mShop != null) {
				notifyDataSetChanged();
			}
		}

		return view;
	}

	@Override
	protected View onCreateHeader(LayoutInflater inflater, ViewGroup container) {
		View view = inflater.inflate(R.layout.header_back_text_1, container,
				false);
		view.findViewById(R.id.back).setOnClickListener(mOnClickListener);
		TextView title = (TextView) view.findViewById(R.id.title);
		title.setText(R.string.shop_detail);
		return view;
	}

	// ===========================================================
	// Listener
	// ===========================================================
	private OnClickListener mOnClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.back: {
				getFragmentManager().popBackStack();
				break;
			}
			case R.id.coupon: {
				Fragment f = new BrandDetailFragment2();
				Bundle args = new Bundle();
				args.putString("brand_id", mShop.getBrand_id());
				f.setArguments(args);
				getFragmentManager().beginTransaction().add(R.id.container, f)
						.addToBackStack(null).commit();
				break;
			}
			case R.id.qr: {
				MainActivity activity = (MainActivity) getActivity();
				activity.requestQrcode();
				break;
			}
			case R.id.map: {
				FragmentTransaction ft = getFragmentManager()
						.beginTransaction();
				CommonMapFragment f = new CommonMapFragment();
				Bundle args = new Bundle();
				args.putDouble("latitude",
						Double.parseDouble(mShop.getLatitude()));
				args.putDouble("longitude",
						Double.parseDouble(mShop.getLongitude()));
				f.setArguments(args);
				ft.add(R.id.container, f);
				ft.addToBackStack(null);
				ft.commit();
				break;
			}
			default:
				break;
			}
		}
	};

	// ===========================================================
	// Methods
	// ===========================================================
	private void requestShopDetail() {
		if (mShop == null) {
			KLog.e("mShop = null");
			Toast.makeText(getActivity(), R.string.request_error,
					Toast.LENGTH_LONG).show();
			return;
		}

		UrlBuilder builder = new UrlBuilder();
		builder.put("c", "shop");
		builder.put("a", "shop_info");
		builder.put("shop_id", mShop.getShop_id());
		String url = builder.build();
		KLog.i("url:" + url);

		GsonRequest<ShopDetailResponse> request = new GsonRequest<ShopDetailResponse>(
				url, ShopDetailResponse.class,
				new Listener<ShopDetailResponse>() {

					@Override
					public void onResponse(ShopDetailResponse response) {
						try {
							if ("0".equals(response.getResult().getError())) {
								mShop = response.getResult().getList();
								finishLoading();
								notifyDataSetChanged();
							} else {
								Toast.makeText(getActivity(),
										R.string.request_error,
										Toast.LENGTH_LONG).show();
								finishLoading(false);
							}
						} catch (Exception e) {
							e.printStackTrace();
							Toast.makeText(getActivity(),
									R.string.request_error, Toast.LENGTH_LONG)
									.show();
							finishLoading(false);
						}
					}
				}, new ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						Toast.makeText(getActivity(), R.string.request_failed,
								Toast.LENGTH_LONG).show();
						finishLoading(false);
					}
				});
		request.setTag("shop_detail");
		addRequestTag("shop_detail");
		mVolleyRequest.getRequestQueue().cancelAll("shop_detail");
		mVolleyRequest.addToRequestQueue(request);
	}

	@Override
	public void notifyDataSetChanged() {
		super.notifyDataSetChanged();

		if (mShop == null) {
			KLog.e("mShop = null");
			return;
		}

		mBrandLogo.setImageUrl(
				NetworkConstants.URL_HOST + mShop.getBrand_logo(),
				mVolleyRequest.getImageLoader());
		mShopName.setText(mShop.getShop_name());
		// mTimeFromTo.setText(String.format(getString(R.string.time_from_to),
		// mShop.getOpen_time_from(), mShop.getOpen_time_to()));
		mTimeFromTo.setText(mShop.getOpen_time());
		mRestDay.setText(mShop.getRest_day());
		mAddress.setText(mShop.getAddress());
		mTel.setText(mShop.getTel());
	}
	// ===========================================================
	// Getter & Setter
	// ===========================================================
	// ===========================================================
	// Methods for/from SuperClass/Interfaces
	// ===========================================================
	// ===========================================================
	// Inner and Anonymous Classes
	// ===========================================================
}
