package jp.hottea.mitsuidesigntec.fragment.shopsearch;

import jp.hottea.mitsuidesigntec.android.R;
import jp.hottea.mitsuidesigntec.fragment.BaseFragment;
import android.content.Context;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

public class ShopSearchTopFragment extends BaseFragment {
	// ===========================================================
	// Fields
	// ===========================================================
	private EditText mTextSearch;
	private View mMapSearch;
	private View mBrandListSearch;

	// ===========================================================
	// Life cycle
	// ===========================================================
	@Override
	public View onCreateContent(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_shopsearch_top,
				container, false);

		mTextSearch = (EditText) view.findViewById(R.id.search);
		mTextSearch.setOnEditorActionListener(mOnEditorActionListener);
		mMapSearch = view.findViewById(R.id.search_around);
		mMapSearch.setOnClickListener(mOnClickListener);
		mBrandListSearch = view.findViewById(R.id.search_brand_list);
		mBrandListSearch.setOnClickListener(mOnClickListener);

		return view;
	}

	@Override
	protected View onCreateHeader(LayoutInflater inflater, ViewGroup container) {
		View view = inflater.inflate(R.layout.header_text, container, false);
		TextView title = (TextView) view.findViewById(R.id.title);
		title.setText(R.string.search_jp);

		return view;
	}

	// ===========================================================
	// Methods
	// ===========================================================
	// ===========================================================
	// Listener
	// ===========================================================
	private OnClickListener mOnClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.search_around:
				getFragmentManager().beginTransaction()
						.add(R.id.container, new GoogleMapFragment())
						.addToBackStack(null).commit();
				break;
			case R.id.search_brand_list:
				getFragmentManager().beginTransaction()
						.add(R.id.container, new BrandListFragment())
						.addToBackStack(null).commit();
				break;
			default:
				break;
			}
		}
	};

	private OnEditorActionListener mOnEditorActionListener = new OnEditorActionListener() {

		@Override
		public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
			if (actionId == EditorInfo.IME_ACTION_SEARCH) {
				InputMethodManager imm = (InputMethodManager) getActivity()
						.getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(mTextSearch.getWindowToken(), 0);

				Bundle args = new Bundle();
				args.putString("key", v.getText().toString());
				ShopListFragment2 f = new ShopListFragment2();
				f.setArguments(args);
				getFragmentManager().beginTransaction().add(R.id.container, f)
						.addToBackStack(null).commit();
				return true;
			}
			return false;
		}
	};

	// ===========================================================
	// Getter & Setter
	// ===========================================================
	// ===========================================================
	// Methods for/from SuperClass/Interfaces
	// ===========================================================
	// ===========================================================
	// Inner and Anonymous Classes
	// ===========================================================

}
