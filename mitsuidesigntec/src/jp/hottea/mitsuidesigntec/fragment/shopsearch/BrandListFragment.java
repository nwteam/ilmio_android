package jp.hottea.mitsuidesigntec.fragment.shopsearch;

import java.util.ArrayList;
import java.util.List;

import jp.hottea.mitsuidesigntec.android.R;
import jp.hottea.mitsuidesigntec.constants.NetworkConstants;
import jp.hottea.mitsuidesigntec.entity.Brand;
import jp.hottea.mitsuidesigntec.entity.BrandListResponse;
import jp.hottea.mitsuidesigntec.fragment.BaseFragment;
import jp.hottea.mitsuidesigntec.fragment.coupon.BrandDetailFragment;
import jp.hottea.mitsuidesigntec.log.KLog;
import jp.hottea.mitsuidesigntec.util.ConfigInfo;
import jp.hottea.mitsuidesigntec.volley.UrlBuilder;
import jp.hottea.mitsuidesigntec.volley.VolleyRequest;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.GsonRequest;
import com.android.volley.toolbox.NetworkImageView;

public class BrandListFragment extends BaseFragment {
	// ===========================================================
	// Constants
	// ===========================================================
	// ===========================================================
	// Fields
	// ===========================================================
	private ConfigInfo mInfo;
	private VolleyRequest mVolleyRequest;
	private List<Brand> mBrands;
	private ListView mListView;
	private MyAdapter mAdapter;
	private LayoutInflater mInflater;
	private boolean mFirstInit = true;

	// ===========================================================
	// Life cycle
	// ===========================================================
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setLoadingEnable(true);
		mInfo = getConfigInfo();
		mVolleyRequest = getVolleyRequest();
		mInflater = LayoutInflater.from(getActivity());
		mBrands = new ArrayList<Brand>();
	}

	@Override
	protected View onCreateContent(LayoutInflater inflater,
			ViewGroup container, Bundle savedInstanceState) {
		View contentView = inflater.inflate(R.layout.fragment_brand_list,
				container, false);

		mListView = (ListView) contentView.findViewById(R.id.list);
		mAdapter = new MyAdapter();
		mListView.setAdapter(mAdapter);
		mListView.setOnItemClickListener(mOnItemClickListener);

		if (mFirstInit == true) {
			requestBrandList();
			mFirstInit = false;
		}

		return contentView;
	}

	@Override
	protected View onCreateHeader(LayoutInflater inflater, ViewGroup container) {
		View header = inflater.inflate(R.layout.header_back_text_1, container,
				false);
		TextView title = (TextView) header.findViewById(R.id.title);
		title.setText(R.string.coupon_list);
		return header;
	}

	// ===========================================================
	// Methods for/from SuperClass/Interfaces
	// ===========================================================
	@Override
	public void notifyDataSetChanged() {
		mAdapter.notifyDataSetChanged();
	}

	// ===========================================================
	// Listener
	// ===========================================================
	private OnItemClickListener mOnItemClickListener = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			Brand brand = mAdapter.getItem(position);
			Bundle args = new Bundle();
			args.putSerializable("brand", brand);
			FragmentTransaction ft = getFragmentManager().beginTransaction();
			BrandDetailFragment f = new BrandDetailFragment();
			f.setArguments(args);
			ft.add(R.id.container, f);
			ft.addToBackStack(null);
			ft.commit();
		}
	};

	// ===========================================================
	// Getter & Setter
	// ===========================================================
	// ===========================================================
	// Methods
	// ===========================================================
	private void requestBrandList() {

		UrlBuilder builder = new UrlBuilder();
		builder.put("c", "brand");
		builder.put("a", "search_brand_list");
		builder.put("type", "recommend");
		builder.put("rank_id", getConfigInfo().getString(ConfigInfo.KEY_ID));
		String url = builder.build();
		KLog.i("url:" + url);
		GsonRequest<BrandListResponse> request = new GsonRequest<BrandListResponse>(
				url, BrandListResponse.class,
				new Listener<BrandListResponse>() {
					@Override
					public void onResponse(BrandListResponse response) {
						try {
							if ("0".equals(response.getResult().getError())) {
								mBrands = response.getResult().getList();
								notifyDataSetChanged();
								finishLoading();
							} else {
								Toast.makeText(getActivity(),
										R.string.request_error,
										Toast.LENGTH_LONG).show();
								finishLoading(false);
							}
						} catch (Exception e) {
							e.printStackTrace();
							Toast.makeText(getActivity(),
									R.string.request_error, Toast.LENGTH_LONG)
									.show();
							finishLoading(false);
						}
					}
				}, new ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						KLog.e("error:" + error.getMessage());
						Toast.makeText(getActivity(), R.string.request_failed,
								Toast.LENGTH_LONG).show();
						finishLoading(false);
					}
				});
		request.setTag("brand_list");
		addRequestTag("brand_list");
		mVolleyRequest.getRequestQueue().cancelAll("brand_detail");
		mVolleyRequest.addToRequestQueue(request);
	}

	// ===========================================================
	// Inner and Anonymous Classes
	// ===========================================================

	private class MyAdapter extends BaseAdapter {

		@Override
		public int getCount() {
			return mBrands.size();
		}

		@Override
		public Brand getItem(int position) {
			return mBrands.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View view = null;
			ViewHolder holder = null;
			if (convertView == null) {
				view = mInflater.inflate(R.layout.item_brand_1, parent, false);
				holder = new ViewHolder();
				holder.image = (NetworkImageView) view.findViewById(R.id.image);
				holder.image.setDefaultImageResId(R.drawable.empty_photo);
				holder.image.setErrorImageResId(R.drawable.empty_photo);
				holder.brand = (TextView) view.findViewById(R.id.brand);
				holder.category = (TextView) view.findViewById(R.id.category);
				holder.content = (TextView) view.findViewById(R.id.content);
				holder.discount_rate = (TextView) view
						.findViewById(R.id.discount_rate);
				view.setTag(holder);
			} else {
				view = convertView;
				holder = (ViewHolder) view.getTag();
			}

			Brand brand = getItem(position);
			holder.image.setImageUrl(
					NetworkConstants.URL_HOST + brand.getBrand_image(),
					mVolleyRequest.getImageLoader());
			holder.brand.setText(brand.getBrand_name());
			holder.category.setText(brand.getCat_name());
			holder.content.setText(brand.getDiscount_out_contents());
			holder.discount_rate.setText(brand.getDiscount_rate());
			return view;
		}
	}

	private static class ViewHolder {
		private NetworkImageView image;
		private TextView brand;
		private TextView category;
		private TextView content;
		private TextView discount_rate;
	}
}
