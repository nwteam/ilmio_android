package jp.hottea.mitsuidesigntec.log;

import android.util.Log;

public class KLog {

	private static final String TAG = "mitsuidesigntec";
	private static final boolean sIsDebug = true;

	public static void i(String msg) {

		if (!sIsDebug) {
			return;
		}

		StackTraceElement se;

		StackTraceElement[] stackElements = Thread.currentThread()
				.getStackTrace();
		
		if (null != stackElements && stackElements.length >= 4) {
			se = stackElements[3];
			StringBuilder sb = new StringBuilder("[");
			sb.append(se.getClassName());
			sb.append("#");
			sb.append(se.getMethodName());
			sb.append("()");
			sb.append("#line:");
			sb.append(se.getLineNumber());
			sb.append("]");
			sb.append(msg);
			Log.i(TAG, sb.toString());
		}
	}

	public static void w(String msg) {

		if (!sIsDebug) {
			return;
		}

		int index;
		StackTraceElement se;

		StackTraceElement[] stackElements = Thread.currentThread()
				.getStackTrace();
		if (null != stackElements && stackElements.length > 1) {
			index = stackElements.length >= 2 ? 2 : stackElements.length - 1;
			se = stackElements[index];
			StringBuilder sb = new StringBuilder("[");
			sb.append(se.getClassName());
			sb.append("#");
			sb.append(se.getMethodName());
			sb.append("()");
			sb.append("#line:");
			sb.append(se.getLineNumber());
			sb.append("]");
			sb.append(msg);
			Log.w(TAG, sb.toString());
		}
	}

	public static void e(String msg) {

		if (!sIsDebug) {
			return;
		}

		int index;
		StackTraceElement se;

		StackTraceElement[] stackElements = Thread.currentThread()
				.getStackTrace();
		if (null != stackElements && stackElements.length > 1) {
			index = stackElements.length >= 2 ? 2 : stackElements.length - 1;
			se = stackElements[index];
			StringBuilder sb = new StringBuilder("[");
			sb.append(se.getClassName());
			sb.append("#");
			sb.append(se.getMethodName());
			sb.append("()");
			sb.append("#line:");
			sb.append(se.getLineNumber());
			sb.append("]");
			sb.append(msg);
			Log.e(TAG, sb.toString());
		}
	}

}
