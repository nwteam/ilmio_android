package jp.hottea.mitsuidesigntec.constants;

public class ActivityConstants {

	public static final int REQUEST_CATEGORY = 1000;
	public static final int REQUEST_QR_CODE = 1001;
}
