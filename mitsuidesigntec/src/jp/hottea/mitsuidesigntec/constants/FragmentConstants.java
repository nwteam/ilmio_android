package jp.hottea.mitsuidesigntec.constants;

public class FragmentConstants {

	public static final String TAB_COUPON = "tab_coupon";
	public static final String TAB_SHOP_SEARCH = "tab_shop_search";
	
	public static final String TAB_QR = "tab_qr";
	
	public static final String TAG_NEWS = "tab_news";
	public static final String TAG_SETTINGS = "tab_settings";

	public static final String COUPON_TOP = "coupon_top";
	public static final String SHOP_SEARCH_TOP = "shop_search_top";
	
	public static final String QR_TOP = "qr_top";
	
	public static final String NEWS_TOP = "news_top";
	public static final String SETTINGS_TOP = "settings_top";
}
