package jp.hottea.mitsuidesigntec.constants;

public class NetworkConstants {

//	 public static final String URL_HOST = "http://153.121.37.86";
	public static final String URL_HOST = "http://www1099gk.sakura.ne.jp";
	public static final String URL_HOST_INDEX = URL_HOST + "/index.php";
	public static final String URL_SECRET_KEY = "thisisajapanesekey";
	public static final String URL_REGISTER_NO = URL_HOST_INDEX
			+ "?c=users&a=check_user_rank&rank_id=%1$s";
	public static final String URL_REGISTER_INFO = URL_HOST_INDEX
			+ "?c=users&a=register&birthday=%1$s&sex=1&address=%2$s";
	
	public static final String URL_QA =  URL_HOST+"/odm/qa.html";
}
