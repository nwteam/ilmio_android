package jp.hottea.mitsuidesigntec.constants;

public class TypeConstants {

    /** クーポン */
    public static final int TYPE_COUPON = 1;
    /** 店舗検索 */
    public static final int TYPE_SHOP_SEARCH = 2;
    /** QRコード */
    public static final int TYPE_QR_CODE = 3;
    /** ニュース */
    public static final int TYPE_NEWS = 4;
    /** 設定 */
    public static final int TYPE_SETTINGS = 5;
}
