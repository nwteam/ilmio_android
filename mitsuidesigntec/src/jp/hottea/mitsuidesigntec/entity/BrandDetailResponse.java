package jp.hottea.mitsuidesigntec.entity;

public class BrandDetailResponse {
	private BrandDetailResult result;

	public BrandDetailResult getResult() {
		return result;
	}

	public void setResult(BrandDetailResult result) {
		this.result = result;
	}
}
