package jp.hottea.mitsuidesigntec.entity;

public class NewsDetailResult {

	private String error;
	private News list;

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public News getList() {
		return list;
	}

	public void setList(News list) {
		this.list = list;
	}

}
