package jp.hottea.mitsuidesigntec.entity;

import java.io.Serializable;

public class CategoryListResponse implements Serializable {
	private static final long serialVersionUID = -5492039359363162793L;
	private CategoryListResult result;

	public CategoryListResult getResult() {
		return result;
	}

	public void setResult(CategoryListResult result) {
		this.result = result;
	}

}
