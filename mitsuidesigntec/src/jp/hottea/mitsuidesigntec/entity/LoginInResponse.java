package jp.hottea.mitsuidesigntec.entity;

public class LoginInResponse {

	private LoginInResult result;

	public LoginInResult getResult() {
		return result;
	}

	public void setResult(LoginInResult result) {
		this.result = result;
	}

}
