package jp.hottea.mitsuidesigntec.entity;

public class Coupon {
	private String shop_id;
	private String brand_id;
	private String brand_logo;
	private String brand_image;
	private String discount_rate;
	private String address;
	private String barcode;

	public String getShop_id() {
		return shop_id;
	}

	public void setShop_id(String shop_id) {
		this.shop_id = shop_id;
	}

	public String getBrand_id() {
		return brand_id;
	}

	public void setBrand_id(String brand_id) {
		this.brand_id = brand_id;
	}

	public String getBrand_logo() {
		return brand_logo;
	}

	public void setBrand_logo(String brand_logo) {
		this.brand_logo = brand_logo;
	}

	public String getBrand_image() {
		return brand_image;
	}

	public void setBrand_image(String brand_image) {
		this.brand_image = brand_image;
	}

	public String getDiscount_rate() {
		return discount_rate;
	}

	public void setDiscount_rate(String discount_rate) {
		this.discount_rate = discount_rate;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getBarcode() {
		return barcode;
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

}
