package jp.hottea.mitsuidesigntec.entity;

import java.io.Serializable;

public class News implements Serializable {
	private static final long serialVersionUID = -6964042620098243203L;
	private String id;
	private String title;
	private String contents;
	private String insert_time;
	private String sender;
	private String supply_id;
	private String supply_time_from;
	private String img_path1;
	private String img_path2;
	private String img_path3;
	private boolean isFirstItem = false;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContents() {
		return contents;
	}

	public void setContents(String contents) {
		this.contents = contents;
	}

	public String getInsert_time() {
		return insert_time;
	}

	public void setInsert_time(String insert_time) {
		this.insert_time = insert_time;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public String getSupply_id() {
		return supply_id;
	}

	public void setSupply_id(String supply_id) {
		this.supply_id = supply_id;
	}

	public String getSupply_time_from() {
		return supply_time_from;
	}

	public void setSupply_time_from(String supply_time_from) {
		this.supply_time_from = supply_time_from;
	}

	public String getImg_path1() {
		return img_path1;
	}

	public void setImg_path1(String img_path1) {
		this.img_path1 = img_path1;
	}

	public String getImg_path2() {
		return img_path2;
	}

	public void setImg_path2(String img_path2) {
		this.img_path2 = img_path2;
	}

	public String getImg_path3() {
		return img_path3;
	}

	public void setImg_path3(String img_path3) {
		this.img_path3 = img_path3;
	}

	public boolean isFirstItem() {
		return isFirstItem;
	}

	public void setFirstItem(boolean isFirstItem) {
		this.isFirstItem = isFirstItem;
	}
}
