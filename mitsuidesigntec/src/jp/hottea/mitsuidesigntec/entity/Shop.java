package jp.hottea.mitsuidesigntec.entity;

import java.io.Serializable;

public class Shop implements Serializable {
	private static final long serialVersionUID = 3174925819851254032L;
	private String shop_id;
	private String brand_id;
	private String shop_name;
	private String brand_logo;
	private String address;
	private String tel;
	private String open_time_from;
	private String open_time_to;
	private String open_time;
	private String rest_day;
	private String latitude;
	private String longitude;

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getOpen_time() {
		return open_time;
	}

	public void setOpen_time(String open_time) {
		this.open_time = open_time;
	}

	public String getRest_day() {
		return rest_day;
	}

	public void setRest_day(String rest_day) {
		this.rest_day = rest_day;
	}

	public String getShop_id() {
		return shop_id;
	}

	public void setShop_id(String shop_id) {
		this.shop_id = shop_id;
	}

	public String getBrand_id() {
		return brand_id;
	}

	public void setBrand_id(String brand_id) {
		this.brand_id = brand_id;
	}

	public String getShop_name() {
		return shop_name;
	}

	public void setShop_name(String shop_name) {
		this.shop_name = shop_name;
	}

	public String getBrand_logo() {
		return brand_logo;
	}

	public void setBrand_logo(String brand_logo) {
		this.brand_logo = brand_logo;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public String getOpen_time_from() {
		return open_time_from;
	}

	public void setOpen_time_from(String open_time_from) {
		this.open_time_from = open_time_from;
	}

	public String getOpen_time_to() {
		return open_time_to;
	}

	public void setOpen_time_to(String open_time_to) {
		this.open_time_to = open_time_to;
	}
}
