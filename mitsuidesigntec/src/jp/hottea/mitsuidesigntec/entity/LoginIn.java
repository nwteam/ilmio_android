package jp.hottea.mitsuidesigntec.entity;

public class LoginIn {
	private String id;
	private String rank_id;
	private String rank_name;
	private String expired_date_from;
	private String expired_date_to;
	private String msg;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getRank_id() {
		return rank_id;
	}

	public void setRank_id(String rank_id) {
		this.rank_id = rank_id;
	}

	public String getRank_name() {
		return rank_name;
	}

	public void setRank_name(String rank_name) {
		this.rank_name = rank_name;
	}

	public String getExpired_date_from() {
		return expired_date_from;
	}

	public void setExpired_date_from(String expired_date_from) {
		this.expired_date_from = expired_date_from;
	}

	public String getExpired_date_to() {
		return expired_date_to;
	}

	public void setExpired_date_to(String expired_date_to) {
		this.expired_date_to = expired_date_to;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}
}
