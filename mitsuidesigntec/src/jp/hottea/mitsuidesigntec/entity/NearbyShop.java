package jp.hottea.mitsuidesigntec.entity;

import java.io.Serializable;

public class NearbyShop implements Serializable {
	private static final long serialVersionUID = 3389594579157079744L;
	private String shop_id;
	private String shop_name;
	private String shop_note;
	private String brand_id;
	private String longitude;
	private String latitude;

	public String getShop_id() {
		return shop_id;
	}

	public void setShop_id(String shop_id) {
		this.shop_id = shop_id;
	}

	public String getShop_name() {
		return shop_name;
	}

	public void setShop_name(String shop_name) {
		this.shop_name = shop_name;
	}

	public String getShop_note() {
		return shop_note;
	}

	public void setShop_note(String shop_note) {
		this.shop_note = shop_note;
	}

	public String getBrand_id() {
		return brand_id;
	}

	public void setBrand_id(String brand_id) {
		this.brand_id = brand_id;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

}
