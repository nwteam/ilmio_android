package jp.hottea.mitsuidesigntec.entity;

public class ShopDetailResult {

	private String error;
	private Shop list;

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public Shop getList() {
		return list;
	}

	public void setList(Shop list) {
		this.list = list;
	}

}
