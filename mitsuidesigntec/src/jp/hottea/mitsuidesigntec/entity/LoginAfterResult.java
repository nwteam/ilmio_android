package jp.hottea.mitsuidesigntec.entity;

public class LoginAfterResult {

	private String error;
	private String user_id;

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

}
