package jp.hottea.mitsuidesigntec.entity;

public class NewsListResponse {

	private NewsListResult result;

	public NewsListResult getResult() {
		return result;
	}

	public void setResult(NewsListResult result) {
		this.result = result;
	}

}
