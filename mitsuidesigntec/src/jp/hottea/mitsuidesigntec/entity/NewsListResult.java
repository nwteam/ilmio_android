package jp.hottea.mitsuidesigntec.entity;

public class NewsListResult {

	private String error;
	private NewsList list;

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public NewsList getList() {
		return list;
	}

	public void setList(NewsList list) {
		this.list = list;
	}

}
