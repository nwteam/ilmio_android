package jp.hottea.mitsuidesigntec.entity;

import java.io.Serializable;

public class Brand implements Serializable {

	private static final long serialVersionUID = -4661130963185733905L;
	private String brand_id;
	private String brand_name;
	private String brand_logo;
	private String brand_image;
	private String brand_image2;
	private String brand_image3;
	private String brand_desc;
	private String discount_out_contents;
	private String cat_name;
	private String discount_rate;
	private int qr_flg;
	private int resource_id;
	private String ec_url;
	private String kp_code;
	private String bl_code;
	
	private int id;
	private String title;
	private String contents;

	public int getQr_flg() {
		return qr_flg;
	}

	public void setQr_flg(int qr_flg) {
		this.qr_flg = qr_flg;
	}

	public String getBrand_id() {
		return brand_id;
	}

	public void setBrand_id(String brand_id) {
		this.brand_id = brand_id;
	}

	public String getBrand_name() {
		return brand_name;
	}

	public void setBrand_name(String brand_name) {
		this.brand_name = brand_name;
	}

	public String getBrand_image() {
		return brand_image;
	}

	public void setBrand_image(String brand_image) {
		this.brand_image = brand_image;
	}

	public String getBrand_image2() {
		return brand_image2;
	}

	public void setBrand_image2(String brand_image2) {
		this.brand_image2 = brand_image2;
	}

	public String getBrand_image3() {
		return brand_image3;
	}

	public void setBrand_image3(String brand_image3) {
		this.brand_image3 = brand_image3;
	}

	public String getCat_name() {
		return cat_name;
	}

	public void setCat_name(String cat_name) {
		this.cat_name = cat_name;
	}

	public int getResourceId() {
		return resource_id;
	}

	public void setResourceId(int resource_id) {
		this.resource_id = resource_id;
	}

	public String getBrand_logo() {
		return brand_logo;
	}

	public void setBrand_logo(String brand_logo) {
		this.brand_logo = brand_logo;
	}

	public String getBrand_desc() {
		return brand_desc;
	}

	public void setBrand_desc(String brand_desc) {
		this.brand_desc = brand_desc;
	}

	public String getDiscount_out_contents() {
		return discount_out_contents;
	}

	public void setDiscount_out_contents(String discount_out_contents) {
		this.discount_out_contents = discount_out_contents;
	}

	public String getDiscount_rate() {
		return discount_rate;
	}

	public void setDiscount_rate(String discount_rate) {
		this.discount_rate = discount_rate;
	}

	public int getResource_id() {
		return resource_id;
	}

	public void setResource_id(int resource_id) {
		this.resource_id = resource_id;
	}

	public String getEc_url() {
		return ec_url;
	}

	public void setEc_url(String ec_url) {
		this.ec_url = ec_url;
	}

	public String getKp_code() {
		return kp_code;
	}

	public void setKp_code(String kp_code) {
		this.kp_code = kp_code;
	}

	public String getBl_code() {
		return bl_code;
	}

	public void setBl_code(String bl_code) {
		this.bl_code = bl_code;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContents() {
		return contents;
	}

	public void setContents(String contents) {
		this.contents = contents;
	}
}
