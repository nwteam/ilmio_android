package jp.hottea.mitsuidesigntec.entity;

import java.util.ArrayList;

public class NearbyShopResult {
	private String error;
	private ArrayList<NearbyShop> list;

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public ArrayList<NearbyShop> getList() {
		return list;
	}

	public void setList(ArrayList<NearbyShop> list) {
		this.list = list;
	}
}
