package jp.hottea.mitsuidesigntec.entity;


public class NearbyShopResponse {

	private NearbyShopResult result;

	public NearbyShopResult getResult() {
		return result;
	}

	public void setResult(NearbyShopResult result) {
		this.result = result;
	}

}
