package jp.hottea.mitsuidesigntec.entity;

public class CouponResponse {

	private CouponResult result;

	public CouponResult getResult() {
		return result;
	}

	public void setResult(CouponResult result) {
		this.result = result;
	}

}
