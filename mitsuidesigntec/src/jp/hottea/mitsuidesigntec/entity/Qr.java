package jp.hottea.mitsuidesigntec.entity;

public class Qr {

	private String Shop_id;
	private String Brand_id;

	public String getShop_id() {
		return Shop_id;
	}

	public void setShop_id(String shop_id) {
		Shop_id = shop_id;
	}

	public String getBrand_id() {
		return Brand_id;
	}

	public void setBrand_id(String brand_id) {
		Brand_id = brand_id;
	}
}
