package jp.hottea.mitsuidesigntec.entity;

import java.io.Serializable;
import java.util.ArrayList;

public class CategoryListResult implements Serializable {
	private static final long serialVersionUID = -5022048751729731559L;
	private String error;
	private ArrayList<Category> list;

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public ArrayList<Category> getList() {
		return list;
	}

	public void setList(ArrayList<Category> list) {
		this.list = list;
	}
}
