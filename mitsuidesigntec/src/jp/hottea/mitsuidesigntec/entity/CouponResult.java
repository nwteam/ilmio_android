package jp.hottea.mitsuidesigntec.entity;

public class CouponResult {

	private String error;
	private Coupon list;

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public Coupon getList() {
		return list;
	}

	public void setList(Coupon list) {
		this.list = list;
	}

}
