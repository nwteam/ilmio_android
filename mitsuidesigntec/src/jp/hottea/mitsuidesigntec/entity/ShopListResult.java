package jp.hottea.mitsuidesigntec.entity;

import java.util.ArrayList;

public class ShopListResult {

	private String error;
	private ArrayList<Shop> list;

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public ArrayList<Shop> getList() {
		return list;
	}

	public void setList(ArrayList<Shop> list) {
		this.list = list;
	}

}
