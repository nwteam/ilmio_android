package jp.hottea.mitsuidesigntec.entity;

import java.util.ArrayList;

public class NewsList {

	private ArrayList<News> news_list;

	public ArrayList<News> getNews_list() {
		return news_list;
	}

	public void setNews_list(ArrayList<News> news_list) {
		this.news_list = news_list;
	}

}
