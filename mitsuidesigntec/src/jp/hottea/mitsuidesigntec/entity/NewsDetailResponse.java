package jp.hottea.mitsuidesigntec.entity;

public class NewsDetailResponse {

	private NewsDetailResult result;

	public NewsDetailResult getResult() {
		return result;
	}

	public void setResult(NewsDetailResult result) {
		this.result = result;
	}

}
