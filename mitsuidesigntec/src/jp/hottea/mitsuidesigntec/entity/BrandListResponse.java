package jp.hottea.mitsuidesigntec.entity;

public class BrandListResponse {

	private BrandListResult result;

	public BrandListResult getResult() {
		return result;
	}

	public void setResult(BrandListResult result) {
		this.result = result;
	}
}
