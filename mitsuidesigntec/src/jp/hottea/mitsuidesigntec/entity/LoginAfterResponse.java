package jp.hottea.mitsuidesigntec.entity;

public class LoginAfterResponse {

	private LoginAfterResult result;

	public LoginAfterResult getResult() {
		return result;
	}

	public void setResult(LoginAfterResult result) {
		this.result = result;
	}

}
