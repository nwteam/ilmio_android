package jp.hottea.mitsuidesigntec.entity;


public class BrandDetailResult {

	private String error;
	private Brand list;

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public Brand getList() {
		return list;
	}

	public void setList(Brand list) {
		this.list = list;
	}

}
