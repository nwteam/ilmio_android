package jp.hottea.mitsuidesigntec.entity;

public class ShopDetailResponse {

	private ShopDetailResult result;

	public ShopDetailResult getResult() {
		return result;
	}

	public void setResult(ShopDetailResult result) {
		this.result = result;
	}

}
