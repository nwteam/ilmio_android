package jp.hottea.mitsuidesigntec.entity;

public class ShopListResponse {

	private ShopListResult result;

	public ShopListResult getResult() {
		return result;
	}

	public void setResult(ShopListResult result) {
		this.result = result;
	}

}
