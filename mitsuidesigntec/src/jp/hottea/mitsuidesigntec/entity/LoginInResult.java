package jp.hottea.mitsuidesigntec.entity;

public class LoginInResult {

	private String error;
	private LoginIn list;

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public LoginIn getList() {
		return list;
	}

	public void setList(LoginIn list) {
		this.list = list;
	}
}
