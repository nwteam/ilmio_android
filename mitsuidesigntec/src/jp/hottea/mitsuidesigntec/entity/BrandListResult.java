package jp.hottea.mitsuidesigntec.entity;

import java.util.ArrayList;

public class BrandListResult {
	private String error;
	private ArrayList<Brand> list;

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public ArrayList<Brand> getList() {
		return list;
	}

	public void setList(ArrayList<Brand> list) {
		this.list = list;
	}

}
