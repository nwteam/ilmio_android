package jp.hottea.mitsuidesigntec.volley;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;

import jp.hottea.mitsuidesigntec.constants.NetworkConstants;
import jp.hottea.mitsuidesigntec.util.DigestUtils;

public class UrlBuilder {

	public Map<String, String> mMap;

	public UrlBuilder() {
		mMap = new LinkedHashMap<String, String>();
	}

	public void put(String key, String value) {
		mMap.put(key, value);
	}

	public String build() {
		StringBuilder urlBuilder = new StringBuilder(
				NetworkConstants.URL_HOST_INDEX);
		urlBuilder.append("?");
		StringBuilder signBuilder = new StringBuilder("");

		Object[] keys = mMap.keySet().toArray();
		Arrays.sort(keys);

		// build url
		try {
			String value = null;
			String valueUTF8 = null;
			for (Object key : keys) {
				value = mMap.get(key);
				valueUTF8 = value == null ? URLEncoder.encode("", "UTF-8")
						: URLEncoder.encode(value, "UTF-8");
				urlBuilder.append(key).append("=").append(valueUTF8)
						.append("&");

				if (!key.equals("a") && !key.equals("c")) {
					signBuilder.append(key).append("=").append(value)
							.append("&");
				}
			}
			if (signBuilder.length() > 0) {
				signBuilder.deleteCharAt(signBuilder.length() - 1);
			}
			signBuilder.append(NetworkConstants.URL_SECRET_KEY);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		// build md5
		String sign = DigestUtils.md5(signBuilder.toString());
		urlBuilder.append("sign").append("=").append(sign);

		return urlBuilder.toString();
	}

}
