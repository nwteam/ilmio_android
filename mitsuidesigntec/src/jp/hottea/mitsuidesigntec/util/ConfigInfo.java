package jp.hottea.mitsuidesigntec.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ConfigInfo {

	// ===========================================================
	// Constants
	// ===========================================================
	/** user info */
	public static final String KEY_BIRTHDAY = "key_birthday";
	public static final String KEY_PUSH_FLAG = "key_push_flag";
	public static final String KEY_SEX = "key_sex";
	public static final String KEY_ADDRESS = "key_address";
	public static final String KEY_BUIDING_TYPE = "key_buiding_type";
	public static final String KEY_EXPIRED_DATE_FROM = "key_expired_date_from";
	public static final String KEY_EXPIRED_DATE_TO = "key_expired_date_to";
	public static final String KEY_RANK_NAME = "key_rank_name";

	/** tutorial flag **/
	public static final String KEY_TUTORIAL = "key_tutorial";
	/** login flag **/
	public static final String KEY_LOGIN = "key_login";
	/** login after settings **/
	public static final String KEY_SETTINGS = "key_settings";
	
	public static final String KEY_USER_ID = "key_user_id";
	
	
	public static final String KEY_ID = "key_id";
	/** rank id **/
	public static final String KEY_RANK_ID = "key_rank_id"; // login code
	/** for debug **/
	public static final int MODE_LOCAL_DEBUG = 0; /* get data from local */
	public static final int MODE_RELEASE = 1; /* get data from network */
	public static final int MODE_GOOGLE_MAP = 2;
	private static final int MODE_DEFAULT = MODE_RELEASE;
	// ===========================================================
	// Fields
	// ===========================================================
	// ===========================================================
	// Life cycle
	// ===========================================================
	// ===========================================================
	// Methods
	// ===========================================================
	// ===========================================================
	// Listener
	// ===========================================================
	// ===========================================================
	// Getter & Setter
	// ===========================================================
	// ===========================================================
	// Methods for/from SuperClass/Interfaces
	// ===========================================================
	// ===========================================================
	// Inner and Anonymous Classes
	// ===========================================================
	private SharedPreferences mSharedPreferences;

	private static ConfigInfo sConfigInfo;

	private ConfigInfo(Context context) {
		mSharedPreferences = context.getSharedPreferences("config_info",
				Context.MODE_PRIVATE);
	}

	public static ConfigInfo getInstance(Context context) {
		if (sConfigInfo == null) {
			sConfigInfo = new ConfigInfo(context);
		}
		return sConfigInfo;
	}

	public SharedPreferences getSharedPreferences() {
		return mSharedPreferences;
	}

	public boolean isLocalDebug() {
		int mode = mSharedPreferences.getInt("mode", MODE_DEFAULT);
		return mode == MODE_LOCAL_DEBUG;
	}

	public boolean isMapDebug() {
		int mode = mSharedPreferences.getInt("mode", MODE_DEFAULT);
		return mode == MODE_GOOGLE_MAP;
	}

	public boolean isvalid(){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		try {
			String dateStr = getString(ConfigInfo.KEY_EXPIRED_DATE_TO);
			if(dateStr == null){
				return true;
			}
			Date d1 = sdf.parse(dateStr);
			Date d2 = new Date(System.currentTimeMillis());
			if(d1.getTime() > d2.getTime()){
				return true;
			}else{
				return false;
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return true;
	}

	public void setLocalDebug(boolean flag) {
		Editor editor = mSharedPreferences.edit();
		if (flag) {
			editor.putInt("mode", MODE_LOCAL_DEBUG);
		} else {
			editor.putInt("mode", MODE_RELEASE);
		}
		editor.commit();
	}

	public boolean getBoolean(String key) {
		return mSharedPreferences.getBoolean(key, false);
	}

	public void putBoolean(String key, boolean flag) {
		Editor editor = mSharedPreferences.edit();
		editor.putBoolean(key, flag);
		editor.commit();
	}

	public String getString(String key) {
		return mSharedPreferences.getString(key, null);
	}

	public void putString(String key, String value) {
		Editor editor = mSharedPreferences.edit();
		editor.putString(key, value);
		editor.commit();
	}
}
