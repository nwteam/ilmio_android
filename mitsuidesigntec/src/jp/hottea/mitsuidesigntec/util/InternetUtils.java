package jp.hottea.mitsuidesigntec.util;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import jp.hottea.mitsuidesigntec.constants.NetworkConstants;

public class InternetUtils {
	private static final String UTF_8 = "UTF-8";

	public static String buildUrl(String url, String... params) {
		StringBuilder sb = new StringBuilder(NetworkConstants.URL_HOST_INDEX);

		try {
			if (null != url) {
				if (null != params) {
					Object[] objs = new Object[params.length];
					for (int i = 0; i < params.length; i++) {
						if (null == params[i]) {
							objs[i] = URLEncoder.encode("", UTF_8);
						} else {
							objs[i] = URLEncoder.encode(params[i], UTF_8);
						}
					}
					url = String.format(url, objs);
				}
				sb.append(url);
			}
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return sb.toString();
	}
}
