package jp.hottea.mitsuidesigntec.util;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

public class DigestUtils {
	

	public static String md5(String msg) {

		String digest = null;

		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[] hash = md.digest(msg.getBytes("UTF-8"));

			// converting byte array to Hexadecimal String
			StringBuilder sb = new StringBuilder(hash.length * 2);
			for (byte b : hash) {
				sb.append(String.format("%02x", b & 0xff));
			}
			digest = sb.toString();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

		return digest;
	}

	public String getHmacMD5(String key, String data) {
		StringBuilder sb = new StringBuilder();
		try {
			SecretKey sk = new SecretKeySpec(key.getBytes("UTF-8"), "HmacMD5");
			Mac mac = Mac.getInstance("HmacMD5");
			mac.init(sk);
			byte[] result = mac.doFinal(data.getBytes("UTF-8"));
			for (byte b : result) {
				if (Integer.toHexString(0xFF & b).length() == 1)
					sb.append("0" + Integer.toHexString(0xFF & b));
				else
					sb.append(Integer.toHexString(0xFF & b));
			}
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (InvalidKeyException e) {
			e.printStackTrace();
		}
		return sb.toString();
	}
}
