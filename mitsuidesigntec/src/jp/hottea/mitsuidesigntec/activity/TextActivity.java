package jp.hottea.mitsuidesigntec.activity;

import jp.hottea.mitsuidesigntec.android.R;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.TextView;

public class TextActivity extends BaseActivity {
	public static final int TYPE_PROTOCOL = 1;
	public static final int TYPE_PRIVACY_POLICY = 2;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.activity_text);

		Intent intent = getIntent();
		int type = intent.getIntExtra("type", 1);

		LayoutInflater inflater = LayoutInflater.from(this);
		FrameLayout headerContainer = (FrameLayout) this
				.findViewById(R.id.header_container);
		View header = inflater.inflate(R.layout.header_back_text_1,
				headerContainer, false);
		headerContainer.addView(header);
		View title = header.findViewById(R.id.title);
		title.setVisibility(View.GONE);

		View back = header.findViewById(R.id.back);
		back.setOnClickListener(mOnClickListener);

		TextView subTitle = (TextView) this.findViewById(R.id.sub_title);
		TextView text = (TextView) this.findViewById(R.id.text);
		switch (type) {
		case TYPE_PROTOCOL:
			subTitle.setText(R.string.protocol);
			text.setText(R.string.protocol_text);
			break;
		case TYPE_PRIVACY_POLICY:
			subTitle.setText(R.string.privacy_policy);
			text.setText(R.string.privacy_policy_text);
			break;
		default:
			break;
		}
	}

	private OnClickListener mOnClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.back:
				finish();
				break;
			default:
				break;
			}
		}
	};
}
