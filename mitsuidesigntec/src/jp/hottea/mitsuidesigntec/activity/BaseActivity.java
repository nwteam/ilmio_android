package jp.hottea.mitsuidesigntec.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

public class BaseActivity extends FragmentActivity {

	private static final String TAG = "BaseActivity";
	private Intent mFromData;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mFromData = getIntent();
	}

	@Override
	public void startActivity(Intent intent) {
		if (mFromData != null) {
			Bundle data = mFromData.getExtras();
			if (data != null) {
				intent.putExtras(data);
			}
		}
		super.startActivity(intent);
	}
}
