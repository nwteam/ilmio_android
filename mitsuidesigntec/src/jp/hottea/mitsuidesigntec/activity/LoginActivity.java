package jp.hottea.mitsuidesigntec.activity;

import java.text.SimpleDateFormat;
import java.util.Locale;

import jp.hottea.mitsuidesigntec.android.R;
import jp.hottea.mitsuidesigntec.entity.LoginIn;
import jp.hottea.mitsuidesigntec.entity.LoginInResponse;
import jp.hottea.mitsuidesigntec.gcm.GcmActivity;
import jp.hottea.mitsuidesigntec.log.KLog;
import jp.hottea.mitsuidesigntec.util.ConfigInfo;
import jp.hottea.mitsuidesigntec.volley.UrlBuilder;
import jp.hottea.mitsuidesigntec.volley.VolleyRequest;
import jp.hottea.mitsuidesigntec.widget.LoadingDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.GsonRequest;

public class LoginActivity extends GcmActivity {
	// ===========================================================
	// Constants
	// ===========================================================
	// ===========================================================
	// Fields
	// ===========================================================
	private ConfigInfo mInfo;
	private VolleyRequest mVolleyRequest;
	private EditText mLoginCode;
	private Button mLogin;
	private TextView mContactUsHere;

	// ===========================================================
	// Life cycle
	// ===========================================================
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
//		this.setContentView(R.layout.activity_login);	 //Use old Login UI
		this.setContentView(R.layout.activity_login_new);//Use New LOGIN UI

		mInfo = ConfigInfo.getInstance(getApplicationContext());
		mVolleyRequest = VolleyRequest.getInstance(getApplicationContext());
		getWindow().addFlags(
				WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
						| WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN);

		mLoginCode = (EditText) this.findViewById(R.id.login_code);
		mLogin = (Button) this.findViewById(R.id.login);
		mLogin.setOnClickListener(mOnClickListener);
		mContactUsHere = (TextView) this.findViewById(R.id.contact_us_here);
		mContactUsHere.setMovementMethod(LinkMovementMethod.getInstance());
		mContactUsHere.setOnClickListener(mOnClickListener);
	}

	// ===========================================================
	// Methods
	// ===========================================================
	private void startLoginAfterActivity() {
		Intent intent = new Intent();
		intent.setClass(getApplication(), LoginAfterActivity.class);
		startActivity(intent);
		finish();
	}

	private void loginHttp() {
		KLog.i("loginHttp");
		final LoadingDialog dialog = new LoadingDialog(LoginActivity.this);
		dialog.show();

		UrlBuilder builder = new UrlBuilder();
		builder.put("c", "users");
		builder.put("a", "check_user_rank");
		builder.put("rank_id", mLoginCode.getText().toString());
		String url = builder.build();
		KLog.i("url:" + url);
		GsonRequest<LoginInResponse> request = new GsonRequest<LoginInResponse>(
				url, LoginInResponse.class, null,
				new Listener<LoginInResponse>() {
					@Override
					public void onResponse(LoginInResponse response) {
						if (null != dialog && dialog.isShowing()) {
							dialog.dismiss();
						}
						KLog.i("error:" + response.getResult().getError());
						try {
							if ("0".equals(response.getResult().getError())) {
								LoginIn info = response.getResult().getList();
								mInfo.putBoolean(ConfigInfo.KEY_LOGIN, true);
								mInfo.putString(ConfigInfo.KEY_ID, info.getId());
								mInfo.putString(ConfigInfo.KEY_RANK_ID,
										info.getRank_id()); // login code
								mInfo.putString(ConfigInfo.KEY_RANK_NAME,
										info.getRank_name());
								mInfo.putString(
										ConfigInfo.KEY_EXPIRED_DATE_FROM,
										info.getExpired_date_from());
								mInfo.putString(ConfigInfo.KEY_EXPIRED_DATE_TO,
										info.getExpired_date_to());
								startLoginAfterActivity();
							} else {
								Toast.makeText(getApplicationContext(),
										R.string.request_error,
										Toast.LENGTH_LONG).show();
							}
						} catch (Exception e) {
							e.printStackTrace();
							Toast.makeText(getApplicationContext(),
									R.string.request_error, Toast.LENGTH_LONG)
									.show();
						}
					}
				}, new ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						KLog.i("onErrorResponse, error:" + error.getMessage());
						if (null != dialog && dialog.isShowing()) {
							dialog.dismiss();
						}
						Toast.makeText(getApplicationContext(),
								R.string.request_failed, Toast.LENGTH_LONG)
								.show();
					}
				});
		request.setTag("login");
		mVolleyRequest.getRequestQueue().cancelAll("login");
		mVolleyRequest.addToRequestQueue(request);
	}

	// ===========================================================
	// Listener
	// ===========================================================
	private OnClickListener mOnClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.login:
				if (mInfo.isLocalDebug()) {
					startLoginAfterActivity();
				} else {
					loginHttp();
				}
				break;
			case R.id.contact_us_here:
				sendEmail();
				break;
			default:
				break;
			}
		}
	};

	// ===========================================================
	// Getter & Setter
	// ===========================================================
	// ===========================================================
	// Methods for/from SuperClass/Interfaces
	// ===========================================================
	private void sendEmail() {
		Intent intent = new Intent(Intent.ACTION_SENDTO);
		intent.setType("plain/text");
		// intent.putExtra(Intent.EXTRA_EMAIL,
		// new String[] { getString(R.string.email_address_to) });
		intent.setData(Uri.parse("mailto:"
				+ getString(R.string.email_address_to)));
		intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.email_subject));
		intent.putExtra(Intent.EXTRA_TEXT, getString(R.string.email_text));
		if (intent.resolveActivity(getPackageManager()) != null) {
			startActivity(intent);
		}
	}

	@Override
	protected void sendRegistrationIdToBackend() {

	}
	// ===========================================================
	// Inner and Anonymous Classes
	// ===========================================================

}
