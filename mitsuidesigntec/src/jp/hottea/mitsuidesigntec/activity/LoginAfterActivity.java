package jp.hottea.mitsuidesigntec.activity;

import java.util.Calendar;

import jp.hottea.mitsuidesigntec.android.R;
import jp.hottea.mitsuidesigntec.entity.LoginAfterResponse;
import jp.hottea.mitsuidesigntec.gcm.GcmActivity;
import jp.hottea.mitsuidesigntec.log.KLog;
import jp.hottea.mitsuidesigntec.util.ConfigInfo;
import jp.hottea.mitsuidesigntec.volley.UrlBuilder;
import jp.hottea.mitsuidesigntec.volley.VolleyRequest;
import jp.hottea.mitsuidesigntec.widget.LoadingDialog;
import jp.hottea.mitsuidesigntec.widget.XDatePickerDialog;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.PopupMenu;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.GsonRequest;

public class LoginAfterActivity extends BaseActivity {
	// ===========================================================
	// Constants
	// ===========================================================
	// ===========================================================
	// Fields
	// ===========================================================
	// birth date
	private EditText mBirthDate;
	private int mYear;
	private int mMonth;
	private int mDay;
	// sex
	private RadioGroup mSex;
	// address
	private EditText mAddress;
	// building type
	private EditText mBuildingType;
	// info
	private ConfigInfo mInfo;
	// volley
	private VolleyRequest mVolleyRequest;

	// ===========================================================
	// Life cycle
	// ===========================================================
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
//		setContentView(R.layout.activity_login_after);//Use old login_after UI
		setContentView(R.layout.activity_login_after_new);//Use new login_after UI

		// birth date
		mBirthDate = (EditText) this.findViewById(R.id.birth_date);
		mBirthDate.setOnClickListener(mOnClickListener);

		// sex
		mSex = (RadioGroup) this.findViewById(R.id.sex);

		// address
		mAddress = (EditText) this.findViewById(R.id.address);
		mAddress.setOnClickListener(mOnClickListener);
		// building type
		mBuildingType = (EditText) this.findViewById(R.id.building_type);
		mBuildingType.setOnClickListener(mOnClickListener);

		this.findViewById(R.id.top_redirect).setOnClickListener(
				mOnClickListener);

		// date
		final Calendar c = Calendar.getInstance();
		mYear = c.get(Calendar.YEAR);
		mMonth = c.get(Calendar.MONTH);
		mDay = c.get(Calendar.DAY_OF_MONTH);

		// config info
		mInfo = ConfigInfo.getInstance(getApplicationContext());

		// volley
		mVolleyRequest = VolleyRequest.getInstance(getApplicationContext());
	}

	// ===========================================================
	// Methods
	// ===========================================================
	private void startMainActivityLocal() {
		Intent intent = new Intent();
		intent.setClass(getApplication(), MainActivity.class);
		startActivity(intent);
		finish();

	}

	private void startMainActivityHttp() {
		final LoadingDialog dialog = new LoadingDialog(LoginAfterActivity.this,
				getResources().getString(R.string.save_loading));
		dialog.show();

		// post login info to server
		UrlBuilder builder = new UrlBuilder();
		builder.put("c", "users");
		builder.put("a", "register");
		builder.put("birthday", mBirthDate.getText().toString());
		mInfo.putString(ConfigInfo.KEY_BIRTHDAY, mBirthDate.getText()
				.toString());
		builder.put("sex", mSex.getCheckedRadioButtonId() == R.id.male ? "1"
				: "0");
		mInfo.putString(ConfigInfo.KEY_SEX,
				mSex.getCheckedRadioButtonId() == R.id.male ? "1" : "0");
		builder.put("address", mAddress.getText().toString());
		mInfo.putString(ConfigInfo.KEY_ADDRESS, mAddress.getText().toString());
		builder.put("building_type", mBuildingType.getText().toString());
		mInfo.putString(ConfigInfo.KEY_BUIDING_TYPE, mBuildingType.getText()
				.toString());
		builder.put(
				"device_id",
				mInfo.getSharedPreferences().getString(
						GcmActivity.PROPERTY_REG_ID, ""));
		builder.put("device_type", "2");
		builder.put("push_flg", "0");
		mInfo.putString(ConfigInfo.KEY_PUSH_FLAG, "0");
		builder.put("rank_id", mInfo.getString(ConfigInfo.KEY_RANK_ID));
		String url = builder.build();
		KLog.i("url:" + url);
		GsonRequest<LoginAfterResponse> request = new GsonRequest<LoginAfterResponse>(
				url, LoginAfterResponse.class, null,
				new Listener<LoginAfterResponse>() {
					@Override
					public void onResponse(LoginAfterResponse response) {
						KLog.i("error:" + response.getResult().getError());
						if (null != dialog && dialog.isShowing()) {
							dialog.dismiss();
						}
						try {
							if ("0".equals(response.getResult().getError())) {
								mInfo.putString(ConfigInfo.KEY_USER_ID, response.getResult().getUser_id());
								mInfo.putBoolean(ConfigInfo.KEY_SETTINGS, true);
								Intent intent = new Intent();
								intent.setClass(getApplication(),
										MainActivity.class);
								startActivity(intent);
								finish();
							} else {
								Toast.makeText(getApplicationContext(),
										R.string.request_error,
										Toast.LENGTH_LONG).show();
							}
						} catch (Exception e) {
							e.printStackTrace();
							Toast.makeText(getApplicationContext(),
									R.string.request_error, Toast.LENGTH_LONG)
									.show();
						}
					}
				}, new ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						KLog.i("onErrorResponse, error:" + error.getMessage());
						if (null != dialog && dialog.isShowing()) {
							dialog.dismiss();
						}
						Toast.makeText(getApplicationContext(),
								R.string.request_failed, Toast.LENGTH_LONG)
								.show();
					}
				});
		request.setTag("settings");
		mVolleyRequest.getRequestQueue().cancelAll("settings");
		mVolleyRequest.addToRequestQueue(request);
	}

	// ===========================================================
	// Listener
	// ===========================================================
	private OnClickListener mOnClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.top_redirect: {
				if (mInfo.isLocalDebug()) {
					startMainActivityLocal();
				} else {
					startMainActivityHttp();
				}
				break;
			}
			case R.id.birth_date: {
				XDatePickerDialog dialog = new XDatePickerDialog(
						LoginAfterActivity.this, mDateSetListener, mYear,
						mMonth, mDay);
				dialog.setPermanentTitle(R.string.birth_date);
				dialog.getDatePicker().setMaxDate(System.currentTimeMillis());
				dialog.show();
				break;
			}
			case R.id.address: {
				PopupMenu popup = new PopupMenu(LoginAfterActivity.this, v);
				Menu menu = popup.getMenu();
				String[] buildingTypes = getResources().getStringArray(
						R.array.address);
				for (String item : buildingTypes) {
					menu.add(item);
				}
				popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
					public boolean onMenuItemClick(MenuItem item) {
						mAddress.setText(item.getTitle());
						return true;
					}
				});
				popup.show();
				break;
			}
			case R.id.building_type: {
				PopupMenu popup = new PopupMenu(LoginAfterActivity.this, v);
				Menu menu = popup.getMenu();
				String[] buildingTypes = getResources().getStringArray(
						R.array.building_type);
				for (String item : buildingTypes) {
					menu.add(item);
				}
				popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
					public boolean onMenuItemClick(MenuItem item) {
						mBuildingType.setText(item.getTitle());
						return true;
					}
				});
				popup.show();
				break;
			}
			default:
				break;
			}
		}
	};

	private DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {

		public void onDateSet(DatePicker view, int year, int monthOfYear,
				int dayOfMonth) {
			mYear = year;
			mMonth = monthOfYear;
			mDay = dayOfMonth;
			mBirthDate.setText(new StringBuilder().append(mYear).append("-")
			// Month is 0 based so add 1
					.append(mMonth + 1).append("-").append(mDay));
		}
	};
	// ===========================================================
	// Inner and Anonymous Classes
	// ===========================================================

}
