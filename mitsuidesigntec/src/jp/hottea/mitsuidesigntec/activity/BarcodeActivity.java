package jp.hottea.mitsuidesigntec.activity;

import java.util.Calendar;

import jp.hottea.mitsuidesigntec.android.R;
import jp.hottea.mitsuidesigntec.entity.Coupon;
import jp.hottea.mitsuidesigntec.entity.CouponResponse;
import jp.hottea.mitsuidesigntec.entity.QrResponse;
import jp.hottea.mitsuidesigntec.fragment.qr.BarcodeResultFragment;
import jp.hottea.mitsuidesigntec.log.KLog;
import jp.hottea.mitsuidesigntec.util.ConfigInfo;
import jp.hottea.mitsuidesigntec.volley.UrlBuilder;
import jp.hottea.mitsuidesigntec.volley.VolleyRequest;
import jp.hottea.mitsuidesigntec.widget.LoadingDialog;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.widget.Toast;

import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.GsonRequest;
import com.google.gson.Gson;
import com.google.zxing.Result;
import com.google.zxing.callback.OnCaptureListener;
import com.google.zxing.fragment.ZxingFragment;

public class BarcodeActivity extends FragmentActivity {
	// ===========================================================
	// Constants
	// ===========================================================
	// ===========================================================
	// Fields
	// ===========================================================
	private ConfigInfo mInfo;
	private VolleyRequest mmVolleyRequest;

	// ===========================================================
	// Life cycle
	// ===========================================================
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_barcode);
		mInfo = ConfigInfo.getInstance(getApplicationContext());
		mmVolleyRequest = VolleyRequest.getInstance(getApplicationContext());
		FragmentManager fm = getSupportFragmentManager();

		ZxingFragment zxingFragment = (ZxingFragment) fm
				.findFragmentById(R.id.zxing);
		zxingFragment.setOnCaptureListener(mOnCaptureListener);

		if (null == savedInstanceState) {
			BarcodeResultFragment resultFragment = (BarcodeResultFragment) fm
					.findFragmentById(R.id.result);
			fm.beginTransaction().hide(resultFragment).commit();
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		mmVolleyRequest.getRequestQueue().cancelAll("request_coupon");
	}

	// ===========================================================
	// Listener
	// ===========================================================
	private OnCaptureListener mOnCaptureListener = new OnCaptureListener() {

		@Override
		public void onCapture(Result result, Bitmap barcode, Bitmap background) {
			KLog.i("barcode:" + result.getText());

			Gson gson = new Gson();
			QrResponse response = gson.fromJson(result.getText(),
					QrResponse.class);
			requestCoupon(response.getQr().getShop_id(), response.getQr()
					.getBrand_id());

			// // result
			// BarcodeResultFragment resultFragment = (BarcodeResultFragment)
			// getSupportFragmentManager()
			// .findFragmentById(R.id.result);
			// getSupportFragmentManager().beginTransaction().show(resultFragment)
			// .commit();
		}
	};

	// ===========================================================
	// Methods
	// ===========================================================
	private void requestCoupon(String shop_id, String brand_id) {

		final LoadingDialog dialog = new LoadingDialog(this);
		dialog.show();

		UrlBuilder builder = new UrlBuilder();

		builder.put("c", "users");
		builder.put("a", "history_info");
		builder.put("shop_id", shop_id);
		builder.put("brand_id", brand_id);
		builder.put("date", String.valueOf(System.currentTimeMillis()));
		builder.put("rank_id", mInfo.getString(ConfigInfo.KEY_RANK_ID));
		builder.put("time_sort", "10:00");
		builder.put("commission", "500");
		builder.put("property_id", mInfo.getString(ConfigInfo.KEY_RANK_ID));
		builder.put("period",
				getPeriod(mInfo.getString(ConfigInfo.KEY_BIRTHDAY)));
		builder.put("sex", mInfo.getString(ConfigInfo.KEY_SEX));
		builder.put("area_id",
				getAreaId(mInfo.getString(ConfigInfo.KEY_ADDRESS)));
		builder.put("medium", "アプリ");
		builder.put("address", mInfo.getString(ConfigInfo.KEY_ADDRESS));

		String url = builder.build();
		KLog.i("url:" + url);

		GsonRequest<CouponResponse> request = new GsonRequest<CouponResponse>(
				url, CouponResponse.class, null,
				new Listener<CouponResponse>() {
					@Override
					public void onResponse(CouponResponse response) {
						if (null != dialog && dialog.isShowing()) {
							dialog.dismiss();
						}
						try {
							if ("0".equals(response.getResult().getError())) {
								Coupon coupon = response.getResult().getList();

								BarcodeResultFragment resultFragment = (BarcodeResultFragment) getSupportFragmentManager()
										.findFragmentById(R.id.result);
								resultFragment.setUpData(coupon);
								getSupportFragmentManager().beginTransaction()
										.show(resultFragment).commit();

							} else {
								Toast.makeText(getApplicationContext(),
										R.string.request_error,
										Toast.LENGTH_LONG).show();
							}
						} catch (Exception e) {
							e.printStackTrace();
							Toast.makeText(getApplicationContext(),
									R.string.request_error, Toast.LENGTH_LONG)
									.show();
						}
					}
				}, new ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						KLog.i("onErrorResponse, error:" + error.getMessage());
						if (null != dialog && dialog.isShowing()) {
							dialog.dismiss();
						}
						Toast.makeText(getApplicationContext(),
								R.string.request_failed, Toast.LENGTH_LONG)
								.show();
					}
				});
		request.setTag("request_coupon");
		mmVolleyRequest.getRequestQueue().cancelAll("request_coupon");
		mmVolleyRequest.addToRequestQueue(request);

	}

	// ===========================================================
	// Getter & Setter
	// ===========================================================
	public String getPeriod(String birthday) {
		String yearStr = birthday.substring(0, 3);

		final Calendar c = Calendar.getInstance();

		int yearInt = c.get(Calendar.YEAR);
		int periodInt = yearInt - Integer.parseInt(yearStr);
		String periodStr = String.valueOf(periodInt);
		String result = "";
		for (int i = 0; i < periodStr.length(); i++) {
			if (i == 0) {
				result = periodStr.substring(0, 0);
			} else {
				result = result + "0";
			}
		}
		return result;
	}

	public String getAreaId(String area) {
		String[] areas = getResources().getStringArray(R.array.address);
		String result = "";
		for (int i = 0; i < areas.length; i++) {
			if (areas[i].equals(area)) {
				result = String.valueOf(i + 1);
			}
		}
		return result;
	}
	// ===========================================================
	// Methods for/from SuperClass/Interfaces
	// ===========================================================
	// ===========================================================
	// Inner and Anonymous Classes
	// ===========================================================

}
