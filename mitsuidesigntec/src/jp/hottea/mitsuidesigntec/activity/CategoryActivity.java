package jp.hottea.mitsuidesigntec.activity;

import java.util.ArrayList;

import jp.hottea.mitsuidesigntec.android.R;
import jp.hottea.mitsuidesigntec.entity.Category;
import jp.hottea.mitsuidesigntec.entity.CategoryListResponse;
import jp.hottea.mitsuidesigntec.log.KLog;
import jp.hottea.mitsuidesigntec.util.ConfigInfo;
import jp.hottea.mitsuidesigntec.volley.UrlBuilder;
import jp.hottea.mitsuidesigntec.volley.VolleyRequest;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.GsonRequest;

public class CategoryActivity extends Activity {
	// ===========================================================
	// Constants
	// ===========================================================
	// ===========================================================
	// Fields
	// ===========================================================
	private ConfigInfo mInfo;
	private VolleyRequest mVolleyRequest;
	private ArrayList<Category> mCategorys;
	private ListView mListView;
	private MyAdapter mAdapter;
	private LayoutInflater mInflater;

	// ===========================================================
	// Life cycle
	// ===========================================================
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// 无title
		requestWindowFeature(Window.FEATURE_NO_TITLE);

		setContentView(R.layout.activity_category);

		// build header
		FrameLayout header = (FrameLayout) findViewById(R.id.header);
		LayoutInflater.from(this).inflate(R.layout.header_image, header, true);

		// info
		mInfo = ConfigInfo.getInstance(this);
		mVolleyRequest = VolleyRequest.getInstance(getApplicationContext());
		if (mInfo.isLocalDebug()) {
			mCategorys = getLocalData();
		} else {
			Intent data = getIntent();
			if (data != null) {
				mCategorys = (ArrayList<Category>) data
						.getSerializableExtra("categorys");
			}
			if (mCategorys == null) {
				mCategorys = new ArrayList<Category>();
				UrlBuilder builder = new UrlBuilder();
				builder.put("c", "category");
				builder.put("a", "cat_list");
				String url = builder.build();
				KLog.i("url:" + url);

				GsonRequest<CategoryListResponse> response = new GsonRequest<CategoryListResponse>(
						url, CategoryListResponse.class, null,
						new Listener<CategoryListResponse>() {

							@Override
							public void onResponse(CategoryListResponse response) {
								try {
									if ("0".equals(response.getResult()
											.getError())) {
										mCategorys = response.getResult()
												.getList();
										mAdapter.notifyDataSetChanged();
									} else {
										Toast.makeText(getApplicationContext(),
												R.string.request_error,
												Toast.LENGTH_SHORT).show();
									}
								} catch (Exception e) {
									e.printStackTrace();
									Toast.makeText(getApplicationContext(),
											R.string.request_error,
											Toast.LENGTH_SHORT).show();
								}
							}
						}, new ErrorListener() {
							@Override
							public void onErrorResponse(VolleyError error) {
								Toast.makeText(getApplicationContext(),
										R.string.request_failed,
										Toast.LENGTH_SHORT).show();
							}
						});
				response.setTag("category_list_response");
				mVolleyRequest.getRequestQueue().cancelAll(
						"category_list_response");
				mVolleyRequest.addToRequestQueue(response);
			}
		}
		mInflater = LayoutInflater.from(this);
		mAdapter = new MyAdapter();
		mListView = (ListView) findViewById(R.id.list);
		mListView.setAdapter(mAdapter);
		// mListView.setOnItemClickListener(mOnItemClickListener);
	}

	// ===========================================================
	// Methods
	// ===========================================================
	// ===========================================================
	// Listener
	// ===========================================================
	private OnItemClickListener mOnItemClickListener = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			Intent data = new Intent();
			data.putExtra("category", mCategorys.get(position).getCat_id());
			KLog.i("category:" + mCategorys.get(position).getCat_id());
			setResult(RESULT_OK, data);
			finish();
		}
	};

	// ===========================================================
	// Getter & Setter
	// ===========================================================
	private ArrayList<Category> getLocalData() {
		ArrayList<Category> data = new ArrayList<Category>();
		data.add(new Category("1", "家具"));
		data.add(new Category("2", "ラグ"));
		data.add(new Category("3", "インテリア小物"));
		data.add(new Category("4", "生活雑貨"));
		data.add(new Category("5", "キッチンウェア"));
		data.add(new Category("6", "アンティーク・ビンテージ"));
		data.add(new Category("7", "照明"));
		data.add(new Category("8", "カーテン・ファブリック"));
		data.add(new Category("9", "家具リペア"));
		data.add(new Category("10", "食器"));
		data.add(new Category("11", "キッズ"));
		data.add(new Category("12", "家電"));
		data.add(new Category("13", "デザイン家電"));
		data.add(new Category("14", "リネン"));
		return data;
	}

	// ===========================================================
	// Methods for/from SuperClass/Interfaces
	// ===========================================================
	// ===========================================================
	// Inner and Anonymous Classes
	// ===========================================================
	class MyAdapter extends BaseAdapter {

		@Override
		public int getCount() {
			return mCategorys.size() / 3;
		}

		@Override
		public Category getItem(int position) {
			return null;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			HoldView holdView;
			if (convertView == null) {
				convertView = mInflater.inflate(R.layout.item_category, parent,
						false);
				holdView = new HoldView();
				holdView.name1 = (TextView) convertView
						.findViewById(R.id.name1);
				holdView.name2 = (TextView) convertView
						.findViewById(R.id.name2);
				holdView.name3 = (TextView) convertView
						.findViewById(R.id.name3);
				convertView.setTag(holdView);
			} else {
				holdView = (HoldView) convertView.getTag();
			}
			holdView.name1.setText(mCategorys.get(position * 3).getCat_name());
			final int positionIndex = position;
			holdView.name1.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					textViewListener(positionIndex * 3);
				}
			});
			holdView.name2.setText(mCategorys.get(position * 3 + 1)
					.getCat_name());
			holdView.name2.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					textViewListener(positionIndex * 3 + 1);
				}
			});
			holdView.name3.setText(mCategorys.get(position * 3 + 2)
					.getCat_name());
			holdView.name3.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					textViewListener(positionIndex * 3 + 2);
				}
			});
			return convertView;
		}
	}

	public void textViewListener(int Position) {
		Intent data = new Intent();
		data.putExtra("category", mCategorys.get(Position).getCat_id());
		KLog.i("category:" + mCategorys.get(Position).getCat_id());
		setResult(RESULT_OK, data);
		finish();
	}

	class HoldView {
		TextView name1;
		TextView name2;
		TextView name3;
	}

}
