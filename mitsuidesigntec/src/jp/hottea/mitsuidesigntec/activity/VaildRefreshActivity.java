package jp.hottea.mitsuidesigntec.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.Toast;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.GsonRequest;
import jp.hottea.mitsuidesigntec.android.R;
import jp.hottea.mitsuidesigntec.entity.LoginIn;
import jp.hottea.mitsuidesigntec.entity.LoginInResponse;
import jp.hottea.mitsuidesigntec.log.KLog;
import jp.hottea.mitsuidesigntec.util.ConfigInfo;
import jp.hottea.mitsuidesigntec.volley.UrlBuilder;
import jp.hottea.mitsuidesigntec.volley.VolleyRequest;
import jp.hottea.mitsuidesigntec.widget.LoadingDialog;

/**
 * Created by yangshiqin on 16/1/12.
 */
public class VaildRefreshActivity extends BaseActivity implements View.OnClickListener{
    Button btn;
    private ConfigInfo mInfo;
    private VolleyRequest mVolleyRequest;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vaild_refresh);
        mVolleyRequest = VolleyRequest.getInstance(getApplicationContext());
        mInfo = ConfigInfo.getInstance(getApplicationContext());
        btn = (Button) findViewById(R.id.sumbit);
        btn.setOnClickListener(this);
    }

    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
    }

    @Override
    public void onClick(View view) {
        if(view == btn){
            setBtn();
        }
    }
    public void setBtn(){
        loginHttp();
    }

    private void loginHttp() {
        KLog.i("loginHttp");
        final LoadingDialog dialog = new LoadingDialog(this);
        dialog.show();

        UrlBuilder builder = new UrlBuilder();
        builder.put("c", "users");
        builder.put("a", "check_user_rank");
        builder.put("rank_id", mInfo.getString(ConfigInfo.KEY_RANK_ID));
        String url = builder.build();
        KLog.i("url:" + url);
        GsonRequest<LoginInResponse> request = new GsonRequest<LoginInResponse>(
                url, LoginInResponse.class, null,
                new Response.Listener<LoginInResponse>() {
                    @Override
                    public void onResponse(LoginInResponse response) {
                        if (null != dialog && dialog.isShowing()) {
                            dialog.dismiss();
                        }
                        KLog.i("error:" + response.getResult().getError());
                        try {
                            if ("0".equals(response.getResult().getError())) {
                                LoginIn info = response.getResult().getList();
                                mInfo.putBoolean(ConfigInfo.KEY_LOGIN, true);
                                mInfo.putString(ConfigInfo.KEY_ID, info.getId());
                                mInfo.putString(ConfigInfo.KEY_RANK_ID,
                                        info.getRank_id()); // login code
                                mInfo.putString(ConfigInfo.KEY_RANK_NAME,
                                        info.getRank_name());
                                mInfo.putString(
                                        ConfigInfo.KEY_EXPIRED_DATE_FROM,
                                        info.getExpired_date_from());
                                mInfo.putString(ConfigInfo.KEY_EXPIRED_DATE_TO,
                                        info.getExpired_date_to());
                                startMain();
                            } else {
                                Toast.makeText(getApplicationContext(),
                                        R.string.request_error,
                                        Toast.LENGTH_LONG).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(getApplicationContext(),
                                    R.string.request_error, Toast.LENGTH_LONG)
                                    .show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                KLog.i("onErrorResponse, error:" + error.getMessage());
                if (null != dialog && dialog.isShowing()) {
                    dialog.dismiss();
                }
                Toast.makeText(getApplicationContext(),
                        R.string.request_failed, Toast.LENGTH_LONG)
                        .show();
            }
        });
        request.setTag("vaild");
        mVolleyRequest.getRequestQueue().cancelAll("vaild");
        mVolleyRequest.addToRequestQueue(request);
    }

    public void startMain() {
        Intent intent = new Intent(getApplicationContext(),
                MainActivity.class);
        startActivity(intent);
        overridePendingTransition(android.R.anim.fade_in,
                android.R.anim.fade_out);
        finish();
    }
}
