package jp.hottea.mitsuidesigntec.activity;

import jp.hottea.mitsuidesigntec.android.R;
import jp.hottea.mitsuidesigntec.constants.ActivityConstants;
import jp.hottea.mitsuidesigntec.constants.FragmentConstants;
import jp.hottea.mitsuidesigntec.constants.TypeConstants;
import jp.hottea.mitsuidesigntec.fragment.BaseFragment;
import jp.hottea.mitsuidesigntec.fragment.TabFragment;
import jp.hottea.mitsuidesigntec.log.KLog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTabHost;
import android.view.View;
import android.widget.ImageView;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TextView;

public class MainActivity extends BaseActivity {
	// ===========================================================
	// Constants
	// ===========================================================
	// ===========================================================
	// Fields
	// ===========================================================
	private Handler mHandler;
	private FragmentTabHost mTabHost;
	private boolean mIsResumeFromBarcode = false;
	private boolean mIsResumeFromNotification = false;

	// private HashMap<String, onActivityResultListener>
	// mOnActivityResultListeners;

	// ===========================================================
	// Life cycle
	// ===========================================================
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		mTabHost = (FragmentTabHost) findViewById(android.R.id.tabhost);
		mTabHost.setup(this, getSupportFragmentManager(), R.id.realtabcontent);
		mTabHost.setBackgroundResource(android.R.color.black);
		Bundle data = null;

		/** クーポン */
		data = new Bundle();
		data.putInt("type", TypeConstants.TYPE_COUPON);
		mTabHost.addTab(mTabHost.newTabSpec(FragmentConstants.TAB_COUPON)
				.setIndicator(getIndicator(0)), TabFragment.class, data);

		/** 店舗検索 */
		data = new Bundle();
		data.putInt("type", TypeConstants.TYPE_SHOP_SEARCH);
		mTabHost.addTab(mTabHost.newTabSpec(FragmentConstants.TAB_SHOP_SEARCH)
				.setIndicator(getIndicator(1)), TabFragment.class, data);
		/** QRコード */
		  data = new Bundle(); data.putInt("type", TypeConstants.TYPE_QR_CODE);
		  mTabHost.addTab(mTabHost.newTabSpec(FragmentConstants.TAB_QR)
		  .setIndicator(getIndicator(2)), TabFragment.class, data);
		 
		/** ニュース */
		data = new Bundle();
		data.putInt("type", TypeConstants.TYPE_NEWS);
		mTabHost.addTab(mTabHost.newTabSpec(FragmentConstants.TAG_NEWS)
				.setIndicator(getIndicator(3)), TabFragment.class, data);

		/** 設定 */
		data = new Bundle();
		data.putInt("type", TypeConstants.TYPE_SETTINGS);
		mTabHost.addTab(mTabHost.newTabSpec(FragmentConstants.TAG_SETTINGS)
				.setIndicator(getIndicator(4)), TabFragment.class, data);

		mTabHost.setOnTabChangedListener(mOnTabChangeListener);

		Intent fromIntent = getIntent();
		if (fromIntent != null) {
			String from = fromIntent.getStringExtra("from");
			if ("notification".equals(from)) {
				mTabHost.setCurrentTab(2);
			}
		}

		mHandler = new Handler();
	}

	@Override
	protected void onNewIntent(Intent intent) {

		String from = intent.getStringExtra("from");
		if ("notification".equals(from)) {
			mIsResumeFromNotification = true;
		}
		super.onNewIntent(intent);
	}

	@Override
	public void onBackPressed() {
		boolean result = false;

		FragmentManager fm = getSupportFragmentManager();
		if (FragmentConstants.TAB_COUPON.equals(mTabHost.getCurrentTabTag())) {
			/** クーポン */
			Fragment f = fm.findFragmentByTag(FragmentConstants.TAB_COUPON);
			if (null != f
					&& 0 != f.getChildFragmentManager()
							.getBackStackEntryCount()) {
				f.getChildFragmentManager().popBackStack();
				result = true;
			}
		} else if (FragmentConstants.TAB_SHOP_SEARCH.equals(mTabHost
				.getCurrentTabTag())) {
			/** 店舗検索 */
			Fragment f = fm
					.findFragmentByTag(FragmentConstants.TAB_SHOP_SEARCH);
			if (null != f
					&& 0 != f.getChildFragmentManager()
							.getBackStackEntryCount()) {
				f.getChildFragmentManager().popBackStack();
				result = true;
			}
		} else if (FragmentConstants.TAG_NEWS.equals(mTabHost
				.getCurrentTabTag())) {
			/** ニュース */
			Fragment f = fm.findFragmentByTag(FragmentConstants.TAG_NEWS);
			if (null != f
					&& 0 != f.getChildFragmentManager()
							.getBackStackEntryCount()) {
				f.getChildFragmentManager().popBackStack();
				result = true;
			}
		}else if (FragmentConstants.TAG_SETTINGS.equals(mTabHost
				.getCurrentTabTag())) {
			/** settings */
			Fragment f = fm.findFragmentByTag(FragmentConstants.TAG_SETTINGS);
			if (null != f
					&& 0 != f.getChildFragmentManager()
							.getBackStackEntryCount()) {
				f.getChildFragmentManager().popBackStack();
				result = true;
			}
		}

		if (false == result) {
			super.onBackPressed();
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (requestCode) {
		case ActivityConstants.REQUEST_CATEGORY:
			if (resultCode == RESULT_OK) {
				// get tab
				Fragment f = getSupportFragmentManager().findFragmentByTag(
						FragmentConstants.TAB_COUPON);
				if (null == f) {
					KLog.e("coupon tab could not be found");
					return;
				}
				// get CouponTopFragment
				f = f.getChildFragmentManager().findFragmentByTag(
						FragmentConstants.COUPON_TOP);
				if (null == f) {
					KLog.e("coupon_top_fragment could not be found");
					return;
				}

				BaseFragment bf = (BaseFragment) f;
				bf.onResult(requestCode, resultCode, data);
			} else {
				// NO-OP
			}
			break;
		case ActivityConstants.REQUEST_QR_CODE:
			mIsResumeFromBarcode = true;
			break;
		default:
			break;
		}
	}

	@Override
	protected void onResumeFragments() {
		super.onResumeFragments();
		if (mIsResumeFromBarcode) {
			mIsResumeFromBarcode = false;
			mTabHost.setCurrentTab(0);
		} else if (mIsResumeFromNotification) {
			mIsResumeFromNotification = false;
			mTabHost.setCurrentTab(2);
		}
	}

	// ===========================================================
	// Listener
	// ===========================================================
	private OnTabChangeListener mOnTabChangeListener = new OnTabChangeListener() {

		@Override
		public void onTabChanged(String tabId) {
			/*
			 * if (tabId.equals(FragmentConstants.TAB_QR)) { Intent intent = new
			 * Intent(getApplicationContext(), BarcodeActivity.class);
			 * startActivityForResult(intent,
			 * ActivityConstants.REQUEST_QR_CODE);
			 * overridePendingTransition(android.R.anim.fade_in,
			 * android.R.anim.fade_out); }
			 */
		}
	};

	// ===========================================================
	// Methods for/from SuperClass/Interfaces
	// ===========================================================
	// ===========================================================
	// Methods
	// ===========================================================

	public void requestQrcode() {
		Intent intent = new Intent(getApplicationContext(),
				BarcodeActivity.class);
		startActivityForResult(intent, ActivityConstants.REQUEST_QR_CODE);
		overridePendingTransition(android.R.anim.fade_in,
				android.R.anim.fade_out);
	}

	public View getIndicator(int index) {
		return  getLayoutInflater().inflate(
				R.layout.indicator_1 + index, null);

	}

	public FragmentTabHost getTabHost() {
		return mTabHost;
	}

	// ===========================================================
	// Inner and Anonymous Classes
	// ===========================================================

}
