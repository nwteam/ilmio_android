package jp.hottea.mitsuidesigntec.activity;

import jp.hottea.mitsuidesigntec.android.R;
import jp.hottea.mitsuidesigntec.util.ConfigInfo;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;

public class TutorialActivity extends BaseActivity {
	// ===========================================================
	// Constants
	// ===========================================================
	// ===========================================================
	// Fields
	// ===========================================================
	private ConfigInfo mInfo;
	private ViewPager mViewPager;
	private MyPagerAdapter mPagerAdapter;
	private boolean mCanMoveToStartActivity = true;

	// ===========================================================
	// Life cycle
	// ===========================================================

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.activity_tutorial);

		Intent intent = getIntent();
		if (intent != null && intent.getExtras() != null) {
			Bundle data = intent.getExtras();
			if (data.containsKey("from")) {
				String from = data.getString("from");
				if ("top".equals(from)) {
					View back = this.findViewById(R.id.back);
					back.setVisibility(View.VISIBLE);
					back.setOnClickListener(mOnClickListener);
					mCanMoveToStartActivity = false;
				}
			}
		}

		mInfo = ConfigInfo.getInstance(this);
		mInfo.putBoolean(ConfigInfo.KEY_TUTORIAL, true);

		mViewPager = (ViewPager) this.findViewById(R.id.viewPager);
		mPagerAdapter = new MyPagerAdapter(getSupportFragmentManager());
		mViewPager.setAdapter(mPagerAdapter);
		mViewPager.setOnPageChangeListener(mOnPageChangeListener);
	}

	// ===========================================================
	// Methods
	// ===========================================================
	private void startLoginActivity() {
		Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
		startActivity(intent);
		overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
		finish();
	}

	// ===========================================================
	// Listener
	// ===========================================================
	private OnClickListener mOnClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.back:
				finish();
				break;
			default:
				break;
			}
		}

	};

	private OnPageChangeListener mOnPageChangeListener = new OnPageChangeListener() {

		private boolean mNeedStartActivity = false;
		private int mLastState;

		@Override
		public void onPageScrollStateChanged(int state) {
			// KLog.i("onPageScrollStateChanged, state:" + state);
			switch (state) {
			case ViewPager.SCROLL_STATE_DRAGGING:
				mNeedStartActivity = true;
				break;
			case ViewPager.SCROLL_STATE_IDLE:
				if (mLastState == ViewPager.SCROLL_STATE_DRAGGING
						&& mNeedStartActivity
						&& mViewPager.getCurrentItem() == 2) {
					if (mCanMoveToStartActivity) {
						startLoginActivity();
					} else {
						finish();
					}
				}
				break;
			case ViewPager.SCROLL_STATE_SETTLING:
			default:
				break;
			}
			mLastState = state;
		}

		@Override
		public void onPageScrolled(int position, float positionOffset,
				int positionOffsetPixels) {
			// KLog.i("onPageScrolled, position:" + position +
			// ", positionOffset:"
			// + positionOffset + ", positionOffsetPixels:"
			// + positionOffsetPixels);

			if (position == mViewPager.getAdapter().getCount() - 1
					&& positionOffset != 0) {
				mNeedStartActivity = false;
			}
		}

		@Override
		public void onPageSelected(int position) {
			// KLog.i("onPageSelected, position:" + position);
		}
	};

	// ===========================================================
	// Getter & Setter
	// ===========================================================
	// ===========================================================
	// Methods for/from SuperClass/Interfaces
	// ===========================================================
	// ===========================================================
	// Inner and Anonymous Classes
	// ===========================================================

	private class MyPagerAdapter extends FragmentStatePagerAdapter {

		public MyPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int position) {
			MyFragment fragment = null;
			switch (position) {
			case 0:
				fragment = MyFragment.newInstance(R.drawable.tutorial_0);
				break;
			case 1:
				fragment = MyFragment.newInstance(R.drawable.tutorial_1);
				break;
			case 2:
				fragment = MyFragment.newInstance(R.drawable.tutorial_2);
				break;
			default:
				break;
			}
			return fragment;
		}

		@Override
		public int getCount() {
			return 3;
		}
	}

	private static class MyFragment extends Fragment {

		public static MyFragment newInstance(int resId) {
			MyFragment fragment = new MyFragment();
			Bundle args = new Bundle();
			args.putInt("resId", resId);
			fragment.setArguments(args);
			return fragment;
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			ImageView view = (ImageView) inflater.inflate(R.layout.image2,
					container, false);
			int resId = getArguments().getInt("resId");
			view.setImageResource(resId);
			return view;
		}
	}
}
