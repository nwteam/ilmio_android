package jp.hottea.mitsuidesigntec.activity;

import java.lang.ref.WeakReference;

import android.app.Dialog;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import jp.hottea.mitsuidesigntec.android.R;
import jp.hottea.mitsuidesigntec.util.ConfigInfo;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

public class SplashActivity extends BaseActivity {
	// ===========================================================
	// Constants
	// ===========================================================
	private static final String TAG = "SplashActivity";
	private static final int START_LOGIN_ACTIVITY = 0;
	private static final int START_LOGIN_AFTER_ACTIVITY = 1;
	private static final int START_MAIN_ACTIVITY = 2;
	private static final int START_TUTORIAL_ACTIVITY = 3;
	private static final int START_VAILD_ACTIVITY=4;
	// ===========================================================
	// Fields
	// ===========================================================
	private Handler mHandler;
	private ConfigInfo mInfo;
	private int mMessageWhat;

	// ===========================================================
	// Life cycle
	// ===========================================================

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);

		mHandler = new MyHandler(this);
		mInfo = ConfigInfo.getInstance(getApplicationContext());

	}

	@Override
	protected void onResume() {
		super.onResume();

		if (mInfo.isLocalDebug()) {
			mMessageWhat = START_LOGIN_ACTIVITY;
		} else if(!mInfo.isvalid()){
			mMessageWhat = START_VAILD_ACTIVITY;
		} else if (mInfo.getBoolean(ConfigInfo.KEY_SETTINGS)) {
			// login after success
			mMessageWhat = START_MAIN_ACTIVITY;
		} else if (mInfo.getBoolean(ConfigInfo.KEY_LOGIN)) {
			// login success
			mMessageWhat = START_LOGIN_AFTER_ACTIVITY;
		} else if (mInfo.getBoolean(ConfigInfo.KEY_TUTORIAL)) {
			// tutorial already
			mMessageWhat = START_LOGIN_ACTIVITY;
		} else {
			// need tutorial
			mMessageWhat = START_TUTORIAL_ACTIVITY;
		}

		if (!mHandler.hasMessages(mMessageWhat)) {
			mHandler.sendEmptyMessageDelayed(mMessageWhat, 1000);
		}
	}

	@Override
	protected void onPause() {
		super.onPause();
		// remove message
		if (mHandler.hasMessages(mMessageWhat)) {
			mHandler.removeMessages(mMessageWhat);
		}
	}

	// ===========================================================
	// Methods
	// ===========================================================
	// ===========================================================
	// Listener
	// ===========================================================
	// ===========================================================
	// Getter & Setter
	// ===========================================================
	// ===========================================================
	// Methods for/from SuperClass/Interfaces
	// ===========================================================
	// ===========================================================
	// Inner and Anonymous Classes
	// ===========================================================

	private static class MyHandler extends Handler {
		private WeakReference<Activity> mActivityWeakRef;

		private MyHandler(Activity activity) {
			mActivityWeakRef = new WeakReference<Activity>(activity);
		}

		@Override
		public void handleMessage(Message msg) {
			SplashActivity activity = (SplashActivity) mActivityWeakRef.get();
			if (null == activity) {
				return;
			}

			switch (msg.what) {
			case START_TUTORIAL_ACTIVITY: {
				Intent intent = new Intent(activity.getApplicationContext(),
						TutorialActivity.class);
				activity.startActivity(intent);
				activity.finish();
				break;
			}
			case START_LOGIN_ACTIVITY: {
				Intent intent = new Intent(activity.getApplicationContext(),
						LoginActivity.class);
				activity.startActivity(intent);
				activity.overridePendingTransition(android.R.anim.fade_in,
						android.R.anim.fade_out);
				activity.finish();
				break;
			}
			case START_LOGIN_AFTER_ACTIVITY: {
				Intent intent = new Intent(activity.getApplicationContext(),
						LoginAfterActivity.class);
				activity.startActivity(intent);
				activity.overridePendingTransition(android.R.anim.fade_in,
						android.R.anim.fade_out);
				activity.finish();
				break;
			}
			case START_MAIN_ACTIVITY: {
				Intent intent = new Intent(activity.getApplicationContext(),
						MainActivity.class);
				activity.startActivity(intent);
				activity.overridePendingTransition(android.R.anim.fade_in,
						android.R.anim.fade_out);
				activity.finish();
				break;
			}
				case START_VAILD_ACTIVITY:
				{
					Intent intent = new Intent(activity.getApplicationContext(),
							VaildRefreshActivity.class);
					activity.startActivity(intent);
					activity.overridePendingTransition(android.R.anim.fade_in,
							android.R.anim.fade_out);
					activity.finish();
					break;
				}
			default:
				break;
			}
		}
	}
}
