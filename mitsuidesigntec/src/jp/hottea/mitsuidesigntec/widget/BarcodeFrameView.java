package jp.hottea.mitsuidesigntec.widget;

import jp.hottea.mitsuidesigntec.android.R;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.View;

import com.google.zxing.camera.CameraManager;

public class BarcodeFrameView extends View {

	private CameraManager mCameraManager;
	private Paint mPaint;
	private int mLineLength;
	private int mStrokeWidth;

	public BarcodeFrameView(Context context) {
		super(context);
		mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		mPaint.setColor(Color.GREEN);
		mStrokeWidth = context.getResources().getDimensionPixelSize(
				R.dimen.dp_1_5);
		mPaint.setStrokeWidth(mStrokeWidth);
		mLineLength = context.getResources().getDimensionPixelSize(
				R.dimen.dp_15);
	}

	public void setCameraManager(CameraManager cameraManager) {
		mCameraManager = cameraManager;
	}

	@Override
	protected void onDraw(Canvas canvas) {
		if (mCameraManager == null) {
			return;
		}
		final Paint paint = mPaint;
		final Rect frame = mCameraManager.getFramingRect();
		final int length = mLineLength;
		final int strokeWidth = mStrokeWidth;
		frame.bottom = frame.top + frame.right - frame.left;

		// left-top
		canvas.drawLine(frame.left, frame.top, frame.left + length, frame.top,
				paint); // h
		canvas.drawLine(frame.left, frame.top, frame.left, frame.top + length,
				paint); // v
		// left-bottom
		canvas.drawLine(frame.left, frame.bottom - strokeWidth, frame.left
				+ length, frame.bottom - strokeWidth, paint); // h
		canvas.drawLine(frame.left, frame.bottom, frame.left, frame.bottom
				- length, paint); // v
		// right-top
		canvas.drawLine(frame.right, frame.top, frame.right - length,
				frame.top, paint); // h
		canvas.drawLine(frame.right - strokeWidth, frame.top, frame.right
				- strokeWidth, frame.top + length, paint); // v
		// right-bottom
		canvas.drawLine(frame.right, frame.bottom - strokeWidth, frame.right
				- length, frame.bottom - strokeWidth, paint);// h
		canvas.drawLine(frame.right - strokeWidth, frame.bottom, frame.right
				- strokeWidth, frame.bottom - length, paint);// v
	}
}
