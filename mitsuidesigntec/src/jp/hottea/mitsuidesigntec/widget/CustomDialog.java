package jp.hottea.mitsuidesigntec.widget;

import android.app.Dialog;
import android.content.Context;
import android.view.KeyEvent;

/**
 * Created by WuhanH on 16/3/10.
 */
public class CustomDialog extends Dialog {
    public CustomDialog(Context context, int theme) {
        super(context, theme);
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        switch(event.getKeyCode())
        {
            case KeyEvent.KEYCODE_BACK:
            {
                return false;
            }
        }
        return true;
    }
}
