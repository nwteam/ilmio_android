package jp.hottea.mitsuidesigntec.widget;

import jp.hottea.mitsuidesigntec.android.R;
import android.app.Dialog;
import android.content.Context;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

public class LoadingDialog extends Dialog {

	public LoadingDialog(Context context) {
		this(context, context.getResources().getString(R.string.loading));
	}

	public LoadingDialog(Context context, String text) {
		super(context, android.R.style.Theme_Translucent_NoTitleBar);
		this.setContentView(R.layout.dialog_loading_1);

		WindowManager.LayoutParams params = this.getWindow().getAttributes();
		// params.width = context.getResources().getDisplayMetrics().widthPixels
		// - context.getResources().getDimensionPixelSize(
		// R.dimen.loading_margin);
		params.width = context.getResources().getDimensionPixelSize(
				R.dimen.loading_width);
		params.height = WindowManager.LayoutParams.WRAP_CONTENT;
		params.gravity = Gravity.CENTER;
		this.getWindow().setAttributes(params);

		TextView textView = (TextView) this.findViewById(R.id.text);
		if (TextUtils.isEmpty(text)) {
			textView.setVisibility(View.GONE);
		} else {
			textView.setText(text);
		}
	}
}
