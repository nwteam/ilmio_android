package jp.hottea.mitsuidesigntec.widget;

import android.content.Context;
import android.support.v4.app.FragmentTabHost;
import android.util.AttributeSet;

public class XFragmentTabHost extends FragmentTabHost {

	public XFragmentTabHost(Context context) {
		super(context);
	}

	public XFragmentTabHost(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	public void onTouchModeChanged(boolean isInTouchMode) {
		// delete super method
		// super.onTouchModeChanged(isInTouchMode);
	}
}
