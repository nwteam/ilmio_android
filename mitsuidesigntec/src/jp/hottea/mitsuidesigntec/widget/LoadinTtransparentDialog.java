package jp.hottea.mitsuidesigntec.widget;

import jp.hottea.mitsuidesigntec.android.R;
import android.app.Dialog;
import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

public class LoadinTtransparentDialog extends Dialog {

	public LoadinTtransparentDialog(Context context) {
		this(context, context.getResources().getString(R.string.loading));
	}

	public LoadinTtransparentDialog(Context context, String text) {
		super(context, android.R.style.Theme_Translucent_NoTitleBar);
		this.setContentView(R.layout.content_loading_1);

		TextView textView = (TextView) this.findViewById(R.id.text);
		if (TextUtils.isEmpty(text)) {
			textView.setVisibility(View.GONE);
		} else {
			textView.setText(text);
		}
	}
}
