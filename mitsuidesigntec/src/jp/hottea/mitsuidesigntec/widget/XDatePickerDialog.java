package jp.hottea.mitsuidesigntec.widget;

import android.app.DatePickerDialog;
import android.content.Context;
import android.widget.DatePicker;

public class XDatePickerDialog extends DatePickerDialog {
	private CharSequence title;

	public XDatePickerDialog(Context context, OnDateSetListener callBack,
			int year, int monthOfYear, int dayOfMonth) {
		super(context, callBack, year, monthOfYear, dayOfMonth);
	}

	public void setPermanentTitle(CharSequence title) {
		this.title = title;
		setTitle(title);
	}

	public void setPermanentTitle(int title) {
		this.title = this.getContext().getResources().getString(title);
		setTitle(title);
	}

	@Override
	public void onDateChanged(DatePicker view, int year, int month, int day) {
		super.onDateChanged(view, year, month, day);
		setTitle(title);
	}
}
