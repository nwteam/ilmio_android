package jp.hottea.mitsuidesigntec.callback;

import android.content.Intent;

public interface onResultListener {
	void onResult(int requestCode, int resultCode, Intent data);
}
